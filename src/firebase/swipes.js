import { firebase } from '../Core/firebase/config';
const db = firebase.firestore();

const onCollectionUpdate = (querySnapshot, callback, cityId) => {
  const usersRef = firebase.firestore().collection('main_data')
    .doc(cityId)
    .collection('users');

  const data = [];
  querySnapshot.forEach((doc) => {
    const temp = doc.data();
    temp.id = doc.id;
    data.push(temp);
  });
  return callback(data, usersRef);
};

export const subscribeToInboundSwipes = (userId, callback, cityId) => {
  const swipesRef = firebase.firestore().collection('main_data')
    .doc(cityId)
    .collection('swipes');
  return swipesRef
    .where('swipedProfile', '==', userId) 
    .onSnapshot((querySnapshot) => onCollectionUpdate(querySnapshot, callback, cityId));
};

export const subscribeToOutboundSwipes = (userId, callback, cityId) => {
  const swipesRef = firebase.firestore().collection('main_data')
    .doc(cityId)
    .collection('swipes');
  return swipesRef
    .where('author', '==', userId)
    .onSnapshot((querySnapshot) => onCollectionUpdate(querySnapshot, callback, cityId));
};

export const addSwipe = (fromUserID, toUserID, type, cityId, matesOrDates, callback) => {
  const swipesRef = firebase.firestore().collection('main_data')
    .doc(cityId)
    .collection('swipes');
  swipesRef
    .add({
      author: fromUserID,
      swipedProfile: toUserID,
      type: type, 
      hasBeenSeen: false,
      created_at: firebase.firestore.FieldValue.serverTimestamp(),
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      matesOrDates: matesOrDates
    })
    .then(() => {
      callback({ success: true });
    })
    .catch((error) => {
      callback({ error: error });
    });
};

export const removeSwipe = (swipeProfileId, userID, cityId) => {
  const batch = db.batch();

  const swipesRef = firebase.firestore().collection('main_data')
    .doc(cityId)
    .collection('swipes');

  const query = swipesRef
    .where('swipedProfile', '==', swipeProfileId)
    .where('author', '==', userID);

  query.get().then(async (querySnapshot) => {
    querySnapshot.docs.forEach((doc) => {
      batch.delete(doc.ref);
    });
    batch.commit();
  });
};

export const markSwipeAsSeen = (fromUserID, toUserID, cityId) => {
  const swipesRef = firebase.firestore().collection('main_data')
    .doc(cityId)
    .collection('swipes');

  swipesRef
    .where('author', '==', fromUserID)
    .where('swipedProfile', '==', toUserID)
    .onSnapshot((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        doc.ref.update({
          hasBeenSeen: true,
        });
      });
    });
};

export const getUserSwipeCount = async (userID, cityId) => {
  const swipeCountRef = firebase.firestore().collection('main_data')
    .doc(cityId)
    .collection('swipe_counts');

  try {
    
    const swipeCount = await swipeCountRef.doc(userID).get();

    if (swipeCount.data()) {
      return swipeCount.data();
    }
  } catch (error) {
    console.log(error);
    return;
  }
};

export const updateUserSwipeCount = (userID, count, cityId) => {
  const data = {
    authorID: userID,
    count: count,
  };

  const swipeCountRef = firebase.firestore().collection('main_data')
    .doc(cityId)
    .collection('swipe_counts');
  

  if (count === 1) {
    data.createdAt = firebase.firestore.FieldValue.serverTimestamp();
  }

  try {
    swipeCountRef.doc(userID).set(data, { merge: true });
  } catch (error) {
    console.log(error);
  }
};
