import { StyleSheet } from 'react-native';
import DynamicAppStyles from '../../DynamicAppStyles';

const dynamicStyles = (colorScheme) => {
  return StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:
        DynamicAppStyles.colorSet[colorScheme].secondaryForegroundColor,
      height: '100%',
    },
    safeAreaContainer: {
      flex: 1,
      backgroundColor:
        DynamicAppStyles.colorSet[colorScheme].secondaryForegroundColor,
    },
    uniDatesContainer: {
      width: "100%", 
      alignItems: "center",
      justifyContent: "center"
    },
    optionButton: {
      alignItems: "center",
      justifyContent: "center",
      width: "40%",
      height: 140,
      borderRadius: 8,
      borderWidth: 2,
      marginTop: 10,
      borderColor: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
      ...DynamicAppStyles.shadows.buttonShadow
    },
    title: {
      fontWeight: "500",
      color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
      textAlign: "center",
      fontSize: 22,
      marginTop: 40
    },
    gradientBackground: {
      marginTop: 0,
      width: "100%",
      height: 280,
      // paddingTop: imageSize * 0.25,
      alignItems: "center",
      justifyContent: "center",
    },
    buttonText: {
      fontWeight: '500',
      fontSize: 20,
      color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
      textAlign: "center",
    },
    likesContainer: {
      marginTop: 10,
      width: "100%", 
      alignItems: "center", 
      justifyContent: "center", 
      // flexDirection: "row"
    },
    likesText: {
      fontSize: 20,
      color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
      fontWeight: '500',
    },
    selectorContainer: {
      width: "100%", 
      height: 60, 
      alignItems: "center", 
      marginTop: 20, 
      flexDirection: "row", 
      justifyContent: "center"
    },
    selectorButton: {
      height: 40,
      marginHorizontal: 20,
      borderRadius:12,
      width: 130,
      alignItems: "center",
      justifyContent: "center",
      // borderColor: "#ef8484",
      // borderRadius: 8,
      // borderWidth: 1
    },
    selectorText: {
      fontSize: 16,
      fontWeight: "500",
      marginVertical: 5,
      marginHorizontal: 10
    }
  });
};

export default dynamicStyles;
