import { StyleSheet, Platform, Dimensions } from 'react-native';
import DynamicAppStyles from '../../DynamicAppStyles';
import { DEVICE_HEIGHT } from '../../helpers/statics';

const width = Dimensions.get('window').width;

const dynamicStyles = (colorScheme) => {
  return StyleSheet.create({
    dealsFeed: {
        marginTop: 20,
        flex: 1,
        borderRadius: 24,
        width: "100%",
        height: "100%",
        backgroundColor: "white",
        justifyContent: "center",
    },
    deal: {
        height: 250,
        margin: 10,
        width: 250,
        borderColor: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
        // borderWidth: 2,
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center", 
        flex: 1,
    },
    dealTitleText: {
        fontSize: 20,
        fontWeight: "500",
        // textAlign: "center",
        // color: "white"
    },
    typeTitleText: {
        fontSize: 14,
        opacity: 0.9,
        fontWeight: "500",
        // color: "white"
    },  
    titleHolder: {
        backgroundColor: "#fafafa",
        // margin: 8,
        // borderRadius: 8,
        opacity: 0.8
    },
    getDeal: {
        marginTop: 140,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "white",
        opacity: 0.6,
        height: 30,
    },
    backView: {
        position: 'absolute',
        top: DEVICE_HEIGHT * 0.46,
        right: 12,
        width: 55,
        height: 55,
        borderRadius: 27.5,
        backgroundColor: '#db6470',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1
      },
      backIcon: {
        width: 30,
        height: 30,
        resizeMode: 'contain',
        tintColor: 'white',
        zIndex: 1
      },
      nameText: {
        fontSize: 28,
        fontWeight: '500',
        // marginRight: 10,
        alignSelf: "flex-start",
        marginTop: 34,
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
      },
      dealsPopup: {
        flex: 1, 
        // marginTop: 40, 
        // alignItems: "center", 
        backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeBackgroundColor
      },
      dealsPopup2: {
        backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeBackgroundColor
      },
      dealsPopupText: {
        fontSize: 22,
        fontWeight: '500',
        letterSpacing: 2,
        color: "white"
      },
      dealsPopupText2: {
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 2,
        opacity: 0.8,
        color: "white"
      },
      dealsPopupText3: {
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 1.5,
        opacity: 0.8,
        color: "white"
      },
      scrollView: {
        paddingBottom: 40,
        alignItems: "center",
        zIndex: 0,
        marginHorizontal: 6,
      },
      bodyText: {
        // paddingLeft: size(10),
        marginVertical: 12,
        fontSize: 16,
        alignSelf: "flex-start",
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
        backgroundColor: 'transparent',
      },
      code: {
          alignSelf: "center",
          marginTop: 20,
          height: 60,
          borderRadius: 10,
          backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeForegroundColor,
          alignItems: "center",
          justifyContent: "center",
          
      },
      codeText: {
          fontSize: 32,
          fontWeight: "500",
          paddingHorizontal: 8
      },
      expiresText: {
          color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
          marginTop: 12,
          fontSize: 12,
      },
      link: {
        height: 50,
        paddingHorizontal: 10,
        borderRadius: 10,
        backgroundColor: "white",
        alignItems: "center",
        justifyContent: "center",
      },
      featuredText: {
        marginTop: 20,
        marginBottom: 10,
        fontSize: 20,
        fontWeight: "500",
        textAlign: "center"
      },
      dateBox: {
        width: 60,
        height: 60,
        borderRadius: 10,
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center"
      },
      dateTextSmall: {
        fontSize: 14, 
        fontWeight: "500",
        opacity: 0.9,
        letterSpacing: 2
      },
      dateTextBig: {
        fontSize: 24, 
        fontWeight: "500"
      },
      categories: {
        flexDirection: "row",
        marginTop: 20,
        marginLeft: 5,
        justifyContent: "space-between"
      },
      location: {
        marginHorizontal: 10
      },
      fixedBottom: {
        height: 130,
        width: "100%",
        borderRadius: 10,
        backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeForegroundColor,
        position: "absolute",
        bottom: 0,
        justifyContent: "center",
        alignItems: "center"
      }
  });
};

export default dynamicStyles;
