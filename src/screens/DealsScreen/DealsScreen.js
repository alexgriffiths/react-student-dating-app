import React, { useCallback, useRef, useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  View,
  Text,
  FlatList,
  ImageBackground,
  TouchableOpacity,
  ActivityIndicator,
  Modal,
  Image,
  ScrollView,
  Linking,
} from 'react-native';
import dynamicStyles from './styles';
import { useColorScheme } from 'react-native-appearance';
import { exp } from 'react-native-reanimated'; 
import { DEVICE_HEIGHT } from '../../helpers/statics';
import AppStyles from '../../AppStyles';
import { firebase } from '../../Core/firebase/config';
import moment from 'moment';
import LinearGradient from 'react-native-linear-gradient';
import DynamicAppStyles from '../../DynamicAppStyles';

const DealsScreen = (props) => {
  const currentUser = useSelector((state) => state.auth.user);
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);
  const appConfig = props?.appConfig
  ? props?.appConfig
  : props.route?.params?.appConfig;

  const [imagesLoaded, setImagesLoaded] = useState(false);
  const [showDeal, setShowDeal] = useState(false);

  const [deals, setDeals] = useState([]);
  const [events, setEvents] = useState([]);

  const [curDealId, setCurDealId] = useState("");
  const [curDealTitle, setCurDealTitle] = useState("");
  const [curDealImage, setCurDealImage] = useState("");
  const [curDealBody, setCurDealBody] = useState("");
  const [curDealCode, setCurDealCode] = useState("");
  const [curDealLink, setCurDealLink] = useState("");
  const [curDealStarts, setCurDealStarts] = useState("");
  const [curDealExpires, setCurDealExpires] = useState("");
  const [curDealType, setCurDealType] = useState("");

//   const dealsRef = firebase.firestore().collection("deals");
  const dealsRef = firebase.firestore().collection('main_data')
    .doc(currentUser.cityId)
    .collection("deals");
  const eventsRef = firebase.firestore().collection('main_data')
    .doc(currentUser.cityId)
    .collection("events");


    //   useEffect(() => {
        
    //     let timer = setTimeout(() => setImagesLoaded(true), 5000);
    //     return () => {
    //         clearTimeout(timer);
    //     };
    // }, []);

    useEffect(() => {
        const newDeals = [];
        let index = 0; 
        try {
            dealsRef.get().then((dealsSnapshot) => {
                dealsSnapshot.forEach((docSnapshot) => {
                    if (docSnapshot !== undefined) {
                        newDeals.push({...docSnapshot.data(), id: ++index});
                    }
                });
            setDeals(newDeals);
            });
        } catch (error) {
            console.log(error);
        }

        const newEvents = [];
        index = 0; 
        try {
            eventsRef.get().then((eventsSnapshot) => {
                eventsSnapshot.forEach((docSnapshot) => {
                    if (docSnapshot !== undefined) {
                        newEvents.push({...docSnapshot.data(), id: ++index});
                    }
                });
            setEvents(newEvents);
            });
        } catch (error) {
            console.log(error);
        }
    }, []);


  const handleDealPress = (id, title, image, body, code, link, expires, starts, type) => {
    setCurDealId(id);
    setCurDealTitle(title);
    setCurDealImage(image);
    setCurDealBody(body);
    setCurDealCode(code);
    setCurDealLink(link);
    setCurDealExpires(expires);
    setCurDealStarts(starts);
    setCurDealType(type);
    setShowDeal(!showDeal);
  };

  const Deal = ({id, title, image, body, code, link, expires, starts, type}) => (
    <View style={styles.deal}>
         <TouchableOpacity 
            onPress={() => handleDealPress(id, title, image, body, code, link, expires, starts, type)} 
            style={{height:"100%", width: "100%"}}>

            <ImageBackground 
                source={{uri: image}} 
                style={{height: "100%", width: "100%", justifyContent: "space-between"}}
                imageStyle={{borderRadius: 10}}
                resizeMode="cover"    
            >
                <View style={{alignItems: "flex-end", margin: 10}}>
                    {expires && <View style={styles.dateBox}>
                        <Text style={styles.dateTextSmall}>{moment(expires).format("MMM")}</Text>
                        <Text style={styles.dateTextBig}>{moment(expires).format("DD")}</Text>
                    </View>}
                </View>

                
                <View style={styles.titleHolder}>
                    <View style={{margin: 8}}>
                        <Text style={styles.typeTitleText}>
                            {type}
                        </Text>
                        <Text style={styles.dealTitleText}>
                            {title}
                        </Text>
                    </View>
                </View>

            </ImageBackground>
        </TouchableOpacity>    
    </View>
  );

  const renderItem = ({ item }) => (
    <Deal id={item.id} title={item.title} image={item.image} body={item.body} code={item.code} link={item.link} expires={item.expires} starts={item.starts} type={item.type} />
  );

  return (
      <View style={styles.dealsFeed}>
          <ScrollView>
            <Modal
                visible={showDeal}
                coverScreen={false}
                animationType="slide"
                transparent={true}
                >   

                    <ScrollView style={styles.dealsPopup2} contentContainerStyle={styles.dealsPopup}>
                        <ImageBackground
                            resizeMode="cover"
                            source={{uri: curDealImage}} 
                            style={{width: "100%", height: 400}}
                        >
                            <View>
                                {curDealExpires && <View style={{...styles.dateBox, alignSelf: "flex-end", top: 50, right: 20}}>
                                    <Text style={styles.dateTextSmall}>{moment(curDealExpires).format("MMM")}</Text>
                                    <Text style={styles.dateTextBig}>{moment(curDealExpires).format("DD")}</Text>
                                </View>}
                            </View>

                            <TouchableOpacity
                                style={styles.backView}
                                onPress={() => setShowDeal(!showDeal)}>
                                <Image
                                    style={styles.backIcon}
                                    source={AppStyles.iconSet.arrowdownIcon}
                                />
                            </TouchableOpacity>

                            <View style={{flex: 1, justifyContent: "flex-end", zIndex: 0}}>

                                <LinearGradient
                                    colors={ ['transparent', DynamicAppStyles.colorSet[colorScheme].mainThemeBackgroundColor]}
                                    start={{x:0, y:0}}
                                    end={{x:0, y:1}}
                                    style={{
                                        width: "100%",
                                        height: 100,
                                        justifyContent: "flex-end"
                                    }}
                                >
                                    <View style={{flexDirection: "row", alignItems: "center", margin: 10}}>
                                        <View style={{marginBottom: 20}}>
                                            <Text style={styles.dealsPopupText2}>{curDealType}</Text>
                                            <Text style={styles.dealsPopupText}>{curDealTitle}</Text>
                                            {curDealStarts && moment.isDate(curDealStarts) && <Text style={styles.dealsPopupText3}>
                                                STARTS {moment(curDealStarts).format('hh:mm A')}
                                            </Text>}
                                        </View>
                                    </View>
                                </LinearGradient>
                            </View>
                        </ImageBackground>

                        <View style={styles.categories}>
                            <TouchableOpacity
                                style={{
                                    width: 76, 
                                    height: 32, 
                                    borderRadius: 10, 
                                    justifyContent: "center", 
                                    alignItems: "center", 
                                    backgroundColor: "white"
                                }}
                            >
                                <Text style={{fontWeight: "bold", fontSize: 16}}>About</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{marginHorizontal: 10, marginTop: 20}}>
                            <Text style={styles.bodyText}>
                                {curDealBody}
                            </Text>
                        </View>

                        {/* Location Section
                        <View style={styles.location}>
                            <Text style={styles.dealsPopupText}>Location</Text>
                            <View style={{height: 250}}>
                                <MapView 
                                    // provider={"google"}
                                    // style={{height: 250}}
                                    // minZoomLevel={15}
                                    initialRegion={{
                                        latitude: 37.78825,
                                        longitude: -122.4324,
                                        latitudeDelta: 0.0922,
                                        longitudeDelta: 0.0421,
                                      }}
                                />
                            </View>
                        </View> */}
                        {curDealCode && <View style={styles.code}>
                            <Text style={styles.codeText}>
                                {curDealCode}
                            </Text>
                        </View>}

                    </ScrollView>

                    <View style={styles.fixedBottom}>
                        {curDealLink && <TouchableOpacity
                            onPress={() => Linking.openURL(curDealLink)}  style={styles.link}>
                            <Text style={{paddingHorizontal: 8, fontWeight: "bold", fontSize: 18, opacity: 0.8}}>
                                Visit Website
                            </Text>
                        </TouchableOpacity>}
                        <Text style={{...styles.expiresText, marginTop: 10}}>
                            Terms and conditions may apply
                        </Text>
                    </View>
                    {/* <View style={styles.dealsPopup}>
                        <ImageBackground
                            source={{uri: curDealImage}} 
                            style={{height: DEVICE_HEIGHT * 0.5, width: "100%", zIndex: 2}}
                            imageStyle={{borderRadius: 10}}>
                            
                            <TouchableOpacity
                                style={styles.backView}
                                onPress={() => setShowDeal(!showDeal)}>
                                <Image
                                    style={styles.backIcon}
                                    source={AppStyles.iconSet.arrowdownIcon}
                                />
                            </TouchableOpacity>
                        </ImageBackground>
                        <ScrollView contentContainerStyle={styles.scrollView}>
                            <Text style={styles.nameText}>
                                {curDealTitle}
                            </Text>
                            <Text style={styles.bodyText}>
                                {curDealBody}
                            </Text>
                            {curDealCode && <View style={styles.code}>
                                <Text style={styles.codeText}>
                                    {curDealCode}
                                </Text>
                            </View>}
                            {curDealExpires && <Text style={styles.expiresText}>
                                Expires: {curDealExpires}
                            </Text>}
                            {curDealLink && <TouchableOpacity
                                onPress={() => Linking.openURL(curDealLink)}  style={styles.link}>
                                <Text style={{paddingHorizontal: 8, textDecorationLine: "underline"}}>
                                    Visit Website
                                </Text>
                            </TouchableOpacity>}
                            <Text style={{...styles.expiresText, marginTop: 50}}>
                                Terms and conditions may apply
                            </Text>
                        </ScrollView>
                    </View> */}

            </Modal>

            <View>   
                <Text style={styles.featuredText}>Events</Text>
                {deals.length >= 1 ?
                        <FlatList
                            horizontal
                            style={{height: 270}}
                            data={events}
                            showsHorizontalScrollIndicator={false}
                            // numColumns={2}
                            keyExtractor={(item, index) => item.id }
                            renderItem={renderItem}
                        />
                    :
                        <View style={{alignItems: "center", justifyContent: "center"}}>
                            {/* <ActivityIndicator/> */}
                            <Text>
                                Exclusive Events Coming Soon...
                            </Text>
                        </View>
                    }
                <Text style={styles.featuredText}>Deals</Text>
                {deals.length >= 1 ?
                        <FlatList
                            horizontal
                            style={{height: 270}}
                            data={deals}
                            showsHorizontalScrollIndicator={false}
                            // numColumns={2}
                            keyExtractor={(item, index) => item.id }
                            renderItem={renderItem}
                        />
                    :
                        <View style={{alignItems: "center", justifyContent: "center"}}>
                            {/* <ActivityIndicator/> */}
                            <Text>
                                Exclusive Deals Coming Soon...
                            </Text>
                        </View>
                    }
            </View>

            {/* <View>
                <Text style={styles.featuredText}>Featured</Text>
            </View> */}
          </ScrollView>

      </View>
  );

}




export default DealsScreen;
