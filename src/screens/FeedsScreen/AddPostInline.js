import React, { useRef, useState, useEffect } from 'react';
import {
  View,
  TouchableOpacity,
  Image,
} from 'react-native';
import { AutoGrowingTextInput } from 'react-native-autogrow-textinput';
import { KeyboardAccessoryView } from 'react-native-ui-lib/keyboard';
import dynamicStyles from './styles';
import { useColorScheme } from 'react-native-appearance';

const assets = {
  cameraFilled: require('../../Core/chat/assets/camera-filled.png'),
  send: require('../../Core/chat/assets/send.png'),
  mic: require('../../Core/chat/assets/microphone.png'),
  close: require('../../Core/chat/assets/close-x-icon.png'),
}; 

function AddPostInline(props) {
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);

  const textInputRef = useRef(null);
  const [customKeyboard, setCustomKeyboard] = useState({
    component: undefined,
    initialProps: undefined,
  });

  const [value, setValue] = useState('');

  useEffect(() => {
    // textInputRef.current?.focus();
  }, []);


  const onKeyboardResigned = () => {
    resetKeyboardView();
  };

  const resetKeyboardView = () => {
    setCustomKeyboard({});
  };
  const isDisabled = () => {
    if (/\S/.test(value)) {
      return false;
    } else {
      return true;
    }
  };


  const onCustomKeyboardItemSelected = (keyboardId, params) => {
    onAudioRecordDone(params);
  };

  const renderBottomInput = () => {
    return (
      <View style={styles.bottomContentContainer}>
        <View style={styles.inputBar}>
          <TouchableOpacity
            // onPress={onAddMediaPress}
            style={styles.inputIconContainer}>
            <Image style={styles.inputIcon} source={assets.cameraFilled} />
          </TouchableOpacity>
          <View style={styles.inputContainer}>
            <AutoGrowingTextInput
              maxHeight={100}
              style={styles.input}
              ref={textInputRef}
              value={value}
              multiline={true}
              placeholder={'Start typing...'}
              // placeholderTextColor={
              //   appStyles.colorSet[colorScheme].mainSubtextColor
              // }
              underlineColorAndroid="transparent"
              onChangeText={(text) => onChangeText(text)}
              onFocus={resetKeyboardView}
            />
          </View>
          <TouchableOpacity
            disabled={isDisabled()}
            // onPress={onSend}
            style={[
              styles.inputIconContainer,
              isDisabled() ? { opacity: 0.2 } : { opacity: 1 },
            ]}>
            <Image style={styles.inputIcon} source={assets.send} />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  return (
    <KeyboardAccessoryView
      renderContent={renderBottomInput}
      trackInteractive={true}
      kbInputRef={textInputRef}
      kbComponent={customKeyboard.component}
      kbInitialProps={customKeyboard.initialProps}
      onItemSelected={onCustomKeyboardItemSelected}
      onKeyboardResigned={onKeyboardResigned}
    />
  );
}

export default AddPostInline;
