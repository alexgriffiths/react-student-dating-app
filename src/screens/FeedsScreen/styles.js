import { StyleSheet, Platform, Dimensions } from 'react-native';
import DynamicAppStyles from '../../DynamicAppStyles';
import { ifIphoneX } from 'react-native-iphone-x-helper';

const width = Dimensions.get('window').width;

const dynamicStyles = (colorScheme) => {
  return StyleSheet.create({
    MainContainer: {
      flex: 1,
      backgroundColor:
        DynamicAppStyles.colorSet[colorScheme].mainThemeForegroundColor,
    },
    feed: {
        flex: 1,
        borderRadius: 24,
        // borderWidth: 4,
        // borderColor: "transparent",
        // borderColor: DynamicAppStyles.colorSet[colorScheme].mainThemeForegroundColor,
        width: "100%",
        height: "100%",
        backgroundColor: "white",
        // alignItems: "center",
        // justifyContent: "center",
    },
    AddPost: {
        marginTop: 30,
        // marginBottom: 10,
        // width: "96%",
        // height: "8%",
        alignSelf: "center",
        borderRadius: 24,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        
    },
    noPostsText: {
      marginTop: 20,
      alignSelf: "center",
      color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
      fontSize: 18, 
      fontWeight: "500",
    },
    titleText: {
        fontSize: 21,
        fontWeight: 'bold',
        // marginRight: 10,
        color: "black",
        padding: 10,
    },
    postTitle: {
        // marginLeft: 10,
        fontSize: 18, 
        fontWeight: "500",
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
        textAlign: "left"

    },
    authorAndDate: {
        marginLeft: 10,
        // alignItems: "flex-start"
    },
    postTitleComment: {
        // marginLeft: 10,
        fontSize: 14, 
        fontWeight: "500",
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
    },
    postTitleAddComment: {
        marginLeft: 10,
        fontSize: 14, 
        fontWeight: "500",
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
    },
    postBody: {
        fontSize: 14, 
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
        paddingVertical: 10,
    },
    postBodyComment: {
        fontSize: 14, 
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
        marginTop: 10,
        // marginLeft: 6,
    },
    postDate: {
        fontSize: 14, 
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
        opacity: 0.6,
        textAlign: "left",
        marginTop: 1
    },    
    postImage: {
        // width: undefined,
        height: 150,
        borderRadius: 5,
        marginVertical: 16,
    },
    commentDate: {
        fontSize: 12, 
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
        opacity: 0.6,
        textAlign: "left",
        marginTop: 1

    },
    annonText: {
        fontSize: 28,
        color: "white",
        opacity: 0.8
    }, 
    annonTextComment: {
        fontSize: 16,
        color: "white",
        opacity: 0.8
    },
    post: {
        width: "100%",
        // height: 400,
        marginTop: 0,
        // flex: 1,
        paddingHorizontal: 14,
        alignSelf: "center",
        justifyContent: "space-between",
        paddingVertical: 10,
        borderBottomColor: "#b8b8b8",
        borderBottomWidth: 0.5,
        minHeight: 150
    },
    comment: {
        width: "100%",
        height: 140,
        paddingHorizontal: 14,
        alignSelf: "center",
        // justifyContent: "space-between",
        paddingVertical: 12,
    },
    addPost: {
        marginTop: 20,
        width: "100%",
        // height: 370,
        // minHeight: 370,

        borderRadius: 10,
        borderColor: "black",
        backgroundColor: "white",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    uploadPhoto: {
        height: 70,
        width: 70,
        borderRadius: 5,
        marginVertical: 16,
    },
    photoButton: {
        alignItems: "flex-end",
        marginHorizontal: 20
    },
    postBtn: {
        marginTop: 40,
        backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeForegroundColor,
        width: "100%",
        height: 60,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 2,
        ...DynamicAppStyles.shadows.buttonShadow
    },
    replyBtn: {
        // marginTop: 40,
        backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeForegroundColor,
        width: "100%",
        height: 60,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 2,
        alignSelf: "center",
        ...DynamicAppStyles.shadows.buttonShadow,
        position: 'absolute',
        left:     0,
        bottom:      140,

      },
    postBtnText: {
        fontSize: 20,
        fontWeight: "500",
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor
    },
    addPostInput: {
        marginVertical: 20,
        minHeight: 80,
        marginHorizontal: 20,
        borderColor: "black",
        borderRadius: 4
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: -200,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: '#F5FCFF88',
    },

    bottomContentContainer: {
        backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
        ...ifIphoneX(
          {
            paddingBottom: 19,
          },
          {
            paddingBottom: 5,
          },
        ),
      },
      inputBar: {
        justifyContent: 'center',
        alignItems: 'center',
        borderTopWidth: 1,
        borderTopColor: DynamicAppStyles.colorSet[colorScheme].hairlineColor,
        backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
        flexDirection: 'row',
      },
      inputIconContainer: {
        margin: 10,
        flex: 0.5,
      },
      inputIcon: {
        tintColor: DynamicAppStyles.colorSet[colorScheme].mainThemeForegroundColor,
        width: 25,
        height: 25,
      },
      inputContainer: {
        flex: 8,
        borderRadius: 20,
        backgroundColor: DynamicAppStyles.colorSet[colorScheme].whiteSmoke,
        flexDirection: 'row',
        alignItems: 'center',
        overflow: 'hidden',
      },
      input: {
        alignSelf: 'center',
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 3,
        paddingRight: 20,
        width: '93%',
        fontSize: 16,
        lineHeight: 22,
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
      },
      inputBar: {
        justifyContent: 'center',
        alignItems: 'center',
        borderTopWidth: 1,
        borderTopColor: DynamicAppStyles.colorSet[colorScheme].hairlineColor,
        backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
        flexDirection: 'row',
      },
      inputContainer: {
        // flex: 4,
        borderRadius: 20,
        backgroundColor: DynamicAppStyles.colorSet[colorScheme].whiteSmoke,
        flexDirection: 'row',
        alignItems: 'center',
        overflow: 'hidden',
      },
      profilePictureStyle: {
        width: 40,
        height: 40,
        borderRadius: 40/2,
        alignItems: "center",
        justifyContent: "center",
        // backgroundColor: "blue"
      },
      campusText: {
        marginTop: 10,
        fontWeight: '500',
        fontSize: 22,
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
        textAlign: "center",
      },
    
  });
};

export default dynamicStyles;
