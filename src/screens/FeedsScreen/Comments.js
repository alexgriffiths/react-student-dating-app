import React, {useRef, useEffect, useCallback, useState} from 'react';
import { 
    SafeAreaView, 
    Text, 
    View, 
    FlatList, 
    TouchableOpacity, 
    RefreshControl,
    ScrollView,
    Dimensions,
    Alert
} from 'react-native';
import dynamicStyles from './styles';
import { useColorScheme } from 'react-native-appearance';
import { firebase } from '../../Core/firebase/config';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ActionSheet from 'react-native-actionsheet';
import DynamicAppStyles from '../../DynamicAppStyles';
import { useSelector, useDispatch } from 'react-redux';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import FastImage from 'react-native-fast-image';

import { AutoGrowingTextInput } from 'react-native-autogrow-textinput';

const Comments = (props) => {
    const colorScheme = useColorScheme();
    const styles = dynamicStyles(colorScheme);
    const [comments, setComments] = useState(new Map());
    const [refreshing, setRefreshing] = useState(false);
    const currentUser = useSelector((state) => state.auth.user);
    const windowHeight = Dimensions.get('window');

    const curPost = props.postId;
    var commentsRef = firebase.firestore().collection('main_data')
        .doc(currentUser.cityId)
        .collection("posts")
        .doc(curPost)
        .collection('comments')
        .orderBy("timestamp", "desc");

    useEffect(() => {
        onRefresh();
      }, []);

    const getTimeFromNow = (time) => {
        const timeNow = firebase.firestore.Timestamp.now().seconds;
        const difference = timeNow - time;
        const inMins = Math.floor(difference / 60);
    
        if (inMins <= 1) {
            return "Just now"
        } 
    
        if (inMins < 60) {
            return inMins.toString() + " mins ago";
        } else {
            const inHours = Math.floor(inMins / 60);
            if (inHours >= 24) {
                const inDays = Math.floor(inHours / 24);
                if (inDays > 1) {
                    return inDays.toString() + " days ago";
                } else {
                return inDays.toString() + " day ago";
            }
            } else {
                return inHours.toString() + " hours ago";
            }
        }
    }

    const getRandomColor = (id) => {
        // var index = Math.floor(Math.random() * 12);
    
        var index = 0;
        if (id > 12) {
            index = id;
        } else {
            index = id % 12;
        }
    
        var colors = ["#f07167", "#ffd166", "#06d6a0", "#118ab2", "#073b4c", "#003566", "#ef233c", "#adb5bd", "#ba181b", "#4ecdc4", "#f3722c", "#4d194d"];
        return colors[index];
        // return 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')';
    }

    const onRefresh = useCallback(async () => {
        setRefreshing(true);
    
        try {
            const commentsNew = new Map();

            commentsRef.get().then((commentsSnapshot) => {
                let index = 0;
                commentsSnapshot.forEach((docSnapshot) => {
                    if (docSnapshot !== undefined) {
                        commentsNew.set(docSnapshot.id, 
                            {...docSnapshot.data(), id: ++index, commentId: docSnapshot.id} 
                        );
                    }

                });
                comments.forEach((value,key) => {
                    commentsNew.set(key, value);
                })

                setComments(commentsNew);
                setRefreshing(false);   
            });
        } catch (error) {
            console.error(error);
        }
     
      }, [refreshing]); 

      const deletePost = (commentId) => {
        if (commentId) {
            firebase.firestore().collection('main_data')
                .doc(currentUser.cityId)
                .collection('posts')
                .doc(props.postId)
                .collection('comments')
                .doc(commentId)
                .delete()
                .then(() => {
                    firebase.firestore().collection('main_data')
                    .doc(currentUser.cityId)
                    .collection('posts')
                    .doc(props.postId)
                    .set({likes: firebase.firestore.FieldValue.increment(-1)}, {merge: true})
                    .then(() => {
                        const commentsNew = new Map();

                        comments.forEach((value, key) => {
                            if (key !== commentId) {
                                commentsNew.set(key, value)
                            }
                        })  
                        setComments(commentsNew);
                    })
                });
        }
    };
    
    const showPostOptions = (body, timestamp, postId, images, authorId, commentId) => {
        if (authorId === currentUser.id) {
            Alert.alert(
                'Post',
                  "Your post options",
                [
                  {
                    text: "Delete Post",
                    onPress: () => deletePost(commentId)
                  },  
                  { 
                    text: 'Cancel',
                  }
                ],
                {
                  cancelable: false,
                },
              );
        } else {
            Alert.alert(
                'Post',
                  "Is this post inappropriate?",
                [
                  {
                    text: "Report Post",
                    onPress: () => reportPost(postId, authorId, body, timestamp, images)
                  },  
                  { 
                    text: 'Cancel',
                  }
                ],
                {
                  cancelable: false,
                },
              );
        }
    }

    const commentOptionsPress = (body, timestamp, authorId, commentId) => {
        showPostOptions(body, timestamp, null, null, authorId, commentId);
    }

    const Comment = ({id, body, timestamp, author, profilePicture, authorId, commentId}) => (
        <View style={styles.comment}>
            <View style={{flexDirection: "row", justifyContent: "space-between", alignItems: "center"}}>
                <View style={{flexDirection: "row", alignItems: "center"}}>

                    <TouchableOpacity
                        style={styles.profilePictureStyle}
                    >
                        {profilePicture == undefined || profilePicture == null || author == "Anonymous" ?
                            <View style={circle(getRandomColor(id))}>
                                <Text style={styles.annonText}>{author.substring(0,1).toUpperCase()}</Text>
                            </View>
                            :
                            <FastImage
                                source={{uri: profilePicture}}
                                style={styles.profilePictureStyle}
                                resizeMode="cover"
                                on
                            />
                        }
                    </TouchableOpacity>
    

                    {/* <View style={circle("blue")}>
                        <Text style={styles.annonTextComment}>{author.substring(0,1).toUpperCase()}</Text>
                    </View> */}
                    <View style={styles.authorAndDate}>
                        <Text  style={styles.postTitleComment}>{author}</Text>
                        <Text style={styles.commentDate}>{getTimeFromNow(timestamp.seconds)}</Text>
                    </View>
                </View>
                <TouchableOpacity onPress={() => commentOptionsPress(body, timestamp, authorId, commentId)}> 
                        <Icon style={{height: 30, width: 30}} name="dots-vertical" size={24} color={DynamicAppStyles.colorSet[colorScheme].mainTextColor}/> 
                </TouchableOpacity>
            </View>
            <Text numberOfLines={3} style={styles.postBodyComment}>{body}</Text>
        </View>
    );

    const renderItem = ({ item }) => (
        <Comment id={item.id} body={item.body} timestamp={item.timestamp} author={item.author} profilePicture={item.profilePicture} authorId={item.authorId} commentId={item.commentId}/>
      );

  return (
    <SafeAreaView 
        contentContainerStyle={{width: "100%", minHeight: "70%"}}
        showsVerticalScrollIndicator={false}
        behavior={Platform.OS == 'ios' ? 'padding' : 'height'}>
        {Array.from( comments.values()).length > 0 ?
            <View style={{width: "100%", height: "70%"}}>
                <FlatList
                    data={Array.from( comments.values() )}
                    renderItem={renderItem}
                    keyExtractor={item => item.id}
                    refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
                />
            </View> 
            :
            <View style={{...styles.comment, alignItems: "center", height: "50%"}}>
                <Text>
                    Be the first to comment
                </Text>
            </View>

        }

        <TouchableOpacity onPress={() => props.setAddComment(true)} style={styles.replyBtn}>
            <Text style={styles.postBtnText}>
                Reply
            </Text> 
        </TouchableOpacity>

    </SafeAreaView>
  );
};

circle = function(color) {
    return {
        width: 28,
        height: 28,
        borderRadius: 28/2,
        backgroundColor: color,
        alignItems: "center",
        justifyContent: "center"
    }
}

export default Comments;
