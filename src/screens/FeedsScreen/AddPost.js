import React from 'react';
import { useState } from 'react';
import { 
    SafeAreaView, 
    Text, 
    View, 
    FlatList, 
    TouchableOpacity, 
    TextInput, 
    Keyboard, 
    TouchableWithoutFeedback,
    Alert,
    Image,
    Modal,
    ActivityIndicator
} from 'react-native';
import dynamicStyles from './styles';
import { useColorScheme } from 'react-native-appearance';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import SwitchSelector from "react-native-switch-selector";
import { useSelector, useDispatch } from 'react-redux';
import DynamicAppStyles from '../../DynamicAppStyles';
import { firebase } from '../../Core/firebase/config';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import { IMLocalized } from '../../Core/localization/IMLocalization';
import { firebaseStorage } from './../../Core/firebase/storage';
import { AutoGrowingTextInput } from 'react-native-autogrow-textinput';
import FastImage from 'react-native-fast-image';

const AddPost = (props) => {
    const colorScheme = useColorScheme();
    const styles = dynamicStyles(colorScheme);
    
    // const currentUser = useSelector((state) => state.auth.user);
    // const { firstName, lastName, profilePictureURL } = currentUser;
    const currentUser = props.currentUser;

    const userLastName = currentUser && currentUser.lastName ? currentUser.lastName : ' ';
    const userfirstName = currentUser && currentUser.firstName ? currentUser.firstName : ' ';

    const [postTxt, setPostTxt] = useState("");
    const [postAuthor, setPostAuthor] = useState( userfirstName + " " + userLastName);

    const [uploadImage, setUploadImage] = useState(null);
    const [uploadImages, setUploadImages] = useState([]);
    const [loading, setLoading] = useState(false);

    const options = [
        { label: userfirstName + " " + userLastName, value: userfirstName + " " + userLastName },
        { label: "Anonymous", value: "Anonymous" },
      ];

    var curUser = firebase.auth().currentUser;
    var postsRef = firebase.firestore().collection('main_data')
        .doc(currentUser.cityId)
        .collection("posts");

  const uploadOneImage = (image) => {
    return firebaseStorage.uploadFile(image, currentUser.id)
        .then((response) => response.downloadURL);
  }
  
  const uploadToFirestore = async (uploadImages) => {
    return await Promise.all(
        uploadImages.map(async imageItem => {
            return uploadOneImage(imageItem.image);
        })
    )

    // return Promise.all(uploadImages.map(imageItem => {
    //     return Promise.all(uploadOneImage(imageItem.image));
    //     }
    // )).then((urls) => {
    //     console.log(urls);
    // })

    // firebaseStorage.uploadFile(uploadImage).then((response) => {
    //     return response.downloadURL;
    // });
  }

  const onPostBtnPress = async() => {
    if (postTxt === "") {
        Alert.alert(
            "Post Empty",
            "Please write something before posting:)"
        );
        return;
    }
    if (uploadImages !== null && uploadImages !== undefined) {
        setLoading(true);

        uploadToFirestore(uploadImages).then((downloadLinks) => {
            let postData = {
                body: postTxt,
                author: postAuthor,
                authorId: curUser.uid,
                likes: 0,
                commentsCount: 0,
                authorProfilePicture: currentUser.profilePictureURL,
                images: downloadLinks,
        
                timestamp: firebase.firestore.Timestamp.now(),
            };
    
            if (postTxt !== "" && postTxt !== undefined && curUser !== undefined && curUser !== null) {
                postsRef.add(postData)
                .catch((error) => {
                    console.error("Error adding document: ", error);
                });
                setLoading(false);
                props.animatePostButton();
                props.refresh();
            }
        });
        
        // let postData = {
        //     body: postTxt,
        //     author: postAuthor,
        //     authorId: curUser.uid,
        //     likes: 0,
        //     commentsCount: 0,
    
        //     images: downloadLinks,
    
        //     timestamp: firebase.firestore.Timestamp.now(),
        // };

        // if (postTxt !== "" && postTxt !== undefined && curUser !== undefined && curUser !== null) {
        //     console.log(downloadLinks);
        //     postsRef.add(postData)
        //     .catch((error) => {
        //         console.error("Error adding document: ", error);
        //     });
        //     setLoading(false);
        //     props.animatePostButton();
        //     props.refresh();
        // }

        // firebaseStorage.uploadFile(uploadImage).then((response) => {
        //     let postData = {
        //         body: postTxt,
        //         author: postAuthor,
        //         authorId: curUser.uid,
        //         likes: 0,
        //         commentsCount: 0,
        
        //         imageUrl: response.downloadURL,
        
        //         // comments: {},
        //         timestamp: firebase.firestore.Timestamp.now(),
        //     };

        //     if (postTxt !== "" && postTxt !== undefined && curUser !== undefined && curUser !== null) {
        //         postsRef.add(postData)
        //         .catch((error) => {
        //             console.error("Error adding document: ", error);
        //         });
        //         setLoading(false);
        //         props.animatePostButton();
        //         props.refresh();
                
        //     }
        // });
    } else {
        let postData = {
            body: postTxt,
            author: postAuthor,
            authorId: curUser.uid,
            likes: 0,
            commentsCount: 0,
        
            // comments: {},
            timestamp: firebase.firestore.Timestamp.now(),
        };    
    
        if (postTxt !== "" && postTxt !== undefined && curUser !== undefined && curUser !== null) {
            postsRef.add(postData)
            .catch((error) => {
                console.error("Error adding document: ", error);
            });
            props.animatePostButton();
            props.refresh();
        }
    }

  }


  const getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        Alert.alert(
          '',
          IMLocalized(
            'Sorry, we need camera roll permissions to make this work!',
          ),
          [{ text: IMLocalized('OK') }],
          {
            cancelable: false,
          },
        );
      }
    }
  };

  const removeUploadedImage = (id) => {
      var oldImages = [...uploadImages];
      var newImages = [];

      oldImages.splice(id, 1);
    //   console.log(oldImages);
      for (let imageElem of oldImages) {
          if (imageElem.id > id) {
            newImages.push({iamge: imageElem.image, imageUrl: imageElem.imageUrl, id: imageElem.id - 1})
          } else {
            newImages.push(imageElem);
          }
      }
    //   console.log(newImages);
      setUploadImages(newImages);
  }

  const onPressAddPhotoBtn = async () => {
    if (uploadImages.length >= 3) {
        Alert.alert(
            "Maximum Number Of Images",
            "Please remove an image before adding a new one"
        );
        return;
    }

    const options = {
      title: IMLocalized('Select photo'),
      cancelButtonTitle: IMLocalized('Cancel'),
      takePhotoButtonTitle: IMLocalized('Take Photo'),
      chooseFromLibraryButtonTitle: IMLocalized('Choose from Library'),
      maxWidth: 2000,
      maxHeight: 2000,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    await getPermissionAsync();

    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
    //   allowsEditing: true,
      // aspect: [4, 3],
      // quality: 1,
    });

    if (!result.cancelled) {
        var newImages = [...uploadImages];
        newImages.push({image: result, imageUrl: result.uri, id: newImages.length});
        setUploadImages(newImages);
    //   setUploadImage(result);
    }
  }

  const renderImage = ({ item }) => {
      if (item == undefined) {
          return;
      }

      return (
        <View style={{marginRight: 10, alignSelf: "flex-start"}}>
            <TouchableOpacity
                style={{alignItems: "center", justifyContent: "center", zIndex: 2, height: 24, width: 24, position: "absolute", left: 56, top: 8, backgroundColor: "white", borderRadius: 12}}
                // onPress={() => setUploadImage(null)}
                onPress={() => removeUploadedImage(item.id)}
            >
                <Icon style={{height: 22, width: 22, position: "absolute"}} name="close-circle" size={22} color={DynamicAppStyles.colorSet[colorScheme].mainTextColor}/>
            </TouchableOpacity>
            <Image source={{uri: item.imageUrl}} style={styles.uploadPhoto} resizeMode="cover"/>
        </View>
      );
  }


  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style={{height: "100%", alignItems: "center"}}>
            
            <View style={{flexDirection: "row", alignItems: "flex-end", alignSelf: "flex-end", marginRight: 10}}>
                <TouchableOpacity onPress={() => props.animatePostButton()}>
                    <Icon style={{height: 30, width: 30}} name="close" size={30} color={DynamicAppStyles.colorSet[colorScheme].mainTextColor}/>
                </TouchableOpacity>
            </View>

            <View style={styles.addPost}>
                <View style={{flexDirection: "row", alignItems: "center", marginLeft: 14, marginTop: 20}}>
                    {postAuthor == "Anonymous" ? 
                        <View style={circle("blue")}>
                            <Text style={styles.annonTextComment}>{postAuthor.substring(0,1).toUpperCase()}</Text>
                        </View>
                    :
                        <View style={styles.profilePictureStyle}>
                            <FastImage
                                source={{uri: currentUser.profilePictureURL}}
                                style={styles.profilePictureStyle}
                                resizeMode="cover"
                                on
                            />
                        </View>
                    }
                    <Text  style={styles.postTitleAddComment}>{postAuthor}</Text>
                </View>
                
                <AutoGrowingTextInput 
                    multiline
                    maxHeight={150}
                    maxLength={250}
                    style={styles.addPostInput}
                    placeholder={"What's on your mind?"}
                    placeholderTextColor={DynamicAppStyles.colorSet[colorScheme].mainTextColor}
                    value={postTxt}
                    onChangeText={(text) => setPostTxt(text)}
                />
                <View style={{flexDirection: "row", justifyContent: "flex-end", alignItems: "center"}}>  
                    {/* {uploadImage && 
                        <View>
                            <TouchableOpacity
                                style={{zIndex: 2, height: 24, width: 24, position: "absolute", left: 56, top: 8,}}
                                onPress={() => setUploadImage(null)}
                            >
                                <Icon style={{height: 22, width: 22, position: "absolute"}} name="close-circle" size={22} color={DynamicAppStyles.colorSet[colorScheme].mainTextColor}/>
                            </TouchableOpacity>
                            <Image source={{uri: uploadImage.uri}} style={styles.uploadPhoto} resizeMode="cover"/>
                        </View>
                    } */}

                    {uploadImages &&
                        <FlatList 
                            horizontal
                            style={{marginLeft: 15, height: 100, width: "100%", marginHorizontal: 10}}
                            data={uploadImages}
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item, index) => item.id }
                            renderItem={renderImage}
                            scrollEnabled={false}
                        />
                    }
                    
                    <TouchableOpacity
                        style={styles.photoButton}
                        onPress={() => onPressAddPhotoBtn()}
                        >
                        <Icon style={{height: 30, width: 30}} name="camera" size={30} color={DynamicAppStyles.colorSet[colorScheme].mainTextColor}/>
                    </TouchableOpacity>
                </View>


                <Text style={{alignSelf: "center"}}>
                    Post As
                </Text>
                <SwitchSelector
                    style={{marginVertical: 10, width: "80%", alignSelf:"center", height: 60}}
                    options={options}
                    initial={0}
                    borderColor={"black"}
                    buttonColor={DynamicAppStyles.colorSet[colorScheme].mainTextColor}
                    onPress={(value) => setPostAuthor(value)}
                    textColor={DynamicAppStyles.colorSet[colorScheme].mainTextColor}
                />
            </View>

            <TouchableOpacity onPress={() => onPostBtnPress()} style={styles.postBtn}>
                <Text style={styles.postBtnText}>
                    Add Post
                </Text>
            </TouchableOpacity>
            
            <Modal visible={loading} transparent={true}>
                <ActivityIndicator 
                    size="large"
                    style={styles.loading}
                    animating={loading}
                />
            </Modal>

        </View>

    </TouchableWithoutFeedback>
    
  );
};

circle = function(color) {
    return {
        width: 40,
        height: 40,
        borderRadius: 40/2,
        backgroundColor: color,
        alignItems: "center",
        justifyContent: "center"
    }
}


export default AddPost;
