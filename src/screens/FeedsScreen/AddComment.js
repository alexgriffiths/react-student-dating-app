import React from 'react';
import { useState } from 'react';
import { 
    SafeAreaView, 
    Text, 
    View, 
    FlatList, 
    TouchableOpacity, 
    TextInput, 
    Keyboard, 
    TouchableWithoutFeedback,
    Alert
} from 'react-native';
import dynamicStyles from './styles';
import { useColorScheme } from 'react-native-appearance';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import SwitchSelector from "react-native-switch-selector";
import { useSelector, useDispatch } from 'react-redux';
import DynamicAppStyles from '../../DynamicAppStyles';
import { firebase } from '../../Core/firebase/config';
import FastImage from 'react-native-fast-image';

const AddComment = (props) => {
    const colorScheme = useColorScheme();
    const styles = dynamicStyles(colorScheme);
    const currentUser = useSelector((state) => state.auth.user);
    const { firstName, lastName, profilePictureURL } = currentUser;

    const postId = props.postId;

    const userLastName = currentUser && lastName ? lastName : ' ';
    const userfirstName = currentUser && firstName ? firstName : ' ';

    const [postTxt, setPostTxt] = useState("");
    const [postAuthor, setPostAuthor] = useState( userfirstName + " " + userLastName);

    const options = [
        { label: userfirstName + " " + userLastName, value: userfirstName + " " + userLastName },
        { label: "Anonymous", value: "Anonymous" },

      ];
    
    var curUser = firebase.auth().currentUser;

      const onReplyBtnPress = () => {
currentUser.profilePictureURL
        const profilePicture = postAuthor == "Anonymous" ? "" : currentUser.profilePictureURL;

        let commentData = {
            body: postTxt,
            author: postAuthor,
            authorId: curUser.uid,
            likes: 0,
            commentsCount: 0,
            profilePicture: profilePicture,
            timestamp: firebase.firestore.Timestamp.now(),
        };

        if (postTxt === "") {
            Alert.alert(
                "Comment Empty",
                "Please write something before commenting:)"
            );
            return;
        }
    
        if (postTxt !== "" && postTxt !== undefined && curUser !== undefined && curUser !== null) {
            firebase.firestore().collection('main_data')
                .doc(currentUser.cityId)
                .collection("posts")
                .doc(postId)
                .collection('comments')
                .add(commentData)
            .catch((error) => {
                console.error("Error adding document: ", error);
            });
            firebase.firestore().collection('main_data')
                .doc(currentUser.cityId)
                .collection("posts")
                .doc(postId)
                .set({commentsCount: firebase.firestore.FieldValue.increment(1)}, {merge: true})
            .catch((error) => {
                console.error("Error adding document: ", error);
            });
            props.setAddCommentVisibleState(false);
        }
    
      }


  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style={{height: "100%", alignItems: "center"}}>
            
            <View style={{flexDirection: "row", alignItems: "flex-end", alignSelf: "flex-end", marginRight: 10}}>
                <TouchableOpacity onPress={() => props.setAddCommentVisibleState(false)}>
                    <Icon style={{height: 30, width: 30}} name="close" size={30} color="black"/>
                </TouchableOpacity>
            </View>

            <View style={styles.addPost}>
                <View style={{flexDirection: "row", alignItems: "center", marginLeft: 14, marginTop: 20}}>
                    {postAuthor == "Anonymous" ? 
                        <View style={circle("blue")}>
                            <Text style={styles.annonTextComment}>{postAuthor.substring(0,1).toUpperCase()}</Text>
                        </View>
                    :
                        <View style={styles.profilePictureStyle}>
                            <FastImage
                                source={{uri: currentUser.profilePictureURL}}
                                style={styles.profilePictureStyle}
                                resizeMode="cover"
                                on
                            />
                        </View>
                    }


                    <Text  style={styles.postTitleAddComment}>{postAuthor}</Text>
                </View>
                
                <TextInput 
                    multiline
                    maxLength={120} 
                    style={styles.addPostInput}
                    placeholder={"Write a comment..."}
                    placeholderTextColor={"black"}
                    onChangeText={(text) => setPostTxt(text)}
                    value={postTxt}
                />
                <Text style={{alignSelf: "center"}}>
                    Comment As
                </Text>
                <SwitchSelector
                    style={{marginVertical: 10, width: "80%", alignSelf:"center"}}
                    options={options}
                    initial={0}
                    borderColor={"black"}
                    buttonColor={DynamicAppStyles.colorSet[colorScheme].mainTextColor}
                    textColor={DynamicAppStyles.colorSet[colorScheme].mainTextColor}
                    onPress={(value) => setPostAuthor(value)}
                />
            </View>

            <TouchableOpacity onPress={() => onReplyBtnPress()} style={styles.postBtn}>
                <Text style={styles.postBtnText}>
                    Add Comment
                </Text>
            </TouchableOpacity>

        </View>
    </TouchableWithoutFeedback>
    
  );
};

circle = function(color) {
    return {
        width: 28,
        height: 28,
        borderRadius: 28/2,
        backgroundColor: color,
        alignItems: "center",
        justifyContent: "center"
    }
}


export default AddComment;
