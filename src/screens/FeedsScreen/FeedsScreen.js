import React, { useCallback, useRef, useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  View,
  Text,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
  RefreshControl,
  ActivityIndicator,
  Modal as Modal2,
  Alert
} from 'react-native';
import dynamicStyles from './styles';
import { useColorScheme } from 'react-native-appearance';
import { FloatingAction } from "react-native-floating-action";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import DynamicAppStyles from '../../DynamicAppStyles';
import Comments from './Comments';
import AddPost from './AddPost';
import AddComment from './AddComment';
import { firebase } from '../../Core/firebase/config';
import ActionSheet from 'react-native-actionsheet';
import SwitchSelector from "react-native-switch-selector";
import DealsScreen from "./../DealsScreen/DealsScreen.js";
import ImageView from 'react-native-image-view';
import CardDetailsView from '../../components/swipe/CardDetailsView/CardDetailsView';
import Modal from 'react-native-modal';
import FastImage from 'react-native-fast-image';
import { ScrollView } from 'react-native-gesture-handler';
import AddPostInline from './AddPostInline';

const FeedsScreen = (props) => {
  const currentUser = useSelector((state) => state.auth.user);
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);
  const appConfig = props?.appConfig
  ? props?.appConfig
  : props.route?.params?.appConfig;

  let currentTheme = DynamicAppStyles.navThemeConstants[colorScheme]; 
  const [showComments, setShowComments] = useState(false);
  
  const [curCommentId, setCurCommentId] = useState(0);
  const [curCommentAuthor, setCurCommentAuthor] = useState("");
  const [curCommentBody, setCurCommentBody] = useState("");
  const [curCommentDate, setCurCommentDate] = useState("");
  const [curPostId, setCurPostId] = useState("");
  const [curPostLikeCount, setCurPostLikeCount] = useState(0);
  const [refreshing, setRefreshing] = useState(false);
  const [curPostImages, setCurPostImages] = useState("");
  const [curPostProfilePic, setCurPostProfilePic] = useState("");

  const [closed, setClosed] = useState(false);
  const [addPostVisible, setAddPostVisible] = useState(false);
  const [addComment, setAddCommentVisible ] = useState(false);

  const [posts, setPosts] = useState(new Map());
  const [likedPosts, setLikedPosts] = useState([]);

  const [noPosts, setNoPosts] = useState(false);

  const [isImageViewVisible, setIsImageViewVisible] = useState(false);
  const [tappedImage, setTappedImage] = useState([]);

  const [pressedProfile, setPressedProfile] = useState(null);

  var postsRef = firebase.firestore().collection('main_data')
    .doc(currentUser.cityId)
    .collection("posts");

  var userRef = firebase.firestore().collection('main_data')
    .doc(currentUser.cityId)
    .collection('users')
    .doc(currentUser.id);

  var usersRef = firebase.firestore().collection('main_data')
    .doc(currentUser.cityId)
    .collection('users');

  useEffect(() => {
    // inAppMessaging().setMessagesDisplaySuppressed(false);
    onRefresh();
  }, []);

  const hydrateList = useCallback(async () => {
    //   console.log("Hydrating...");
      const postsArr = Array.from(posts.values());
      const lastPost = postsArr[postsArr.length - 1];
  
    try {
        const postsNew = new Map();
        const next5Posts = postsRef.where("timestamp", "<", lastPost.timestamp).orderBy("timestamp", "desc").limit(5)
        next5Posts.get().then((postsSnapshot) => {
            var index = 0;
            posts.forEach((value,key) => {
                index++;
                postsNew.set(key, value);
            })
            postsSnapshot.forEach((docSnapshot) => {
                postsNew.set(docSnapshot.id, 
                    {...docSnapshot.data(), id: ++index} 
                );
            });

            setPosts(postsNew);
            setRefreshing(false);   
        });
    } catch (error) {
        console.error(error);
    }

  })

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    try {
        const postsNew = new Map();
        const postRefNew = postsRef.orderBy("timestamp", "desc").limit(5);
        postRefNew.get().then((postsSnapshot) => {
            let index = 0;

            userRef.get().then((userSnapshot) => {
                var dbLikedPosts = userSnapshot.data().likedPosts;

                if (dbLikedPosts !== undefined) {
                    setLikedPosts(dbLikedPosts);
                }

                postsSnapshot.forEach((docSnapshot) => {
                    var data = docSnapshot.data();
                    postsNew.set(docSnapshot.id, 
                        {...data, id: ++index, postId: docSnapshot.id} 
                    );
                });
    
                posts.forEach((value,key) => {
                    postsNew.set(key, value);
                })
                setPosts(postsNew);
                console.log(Array.from( postsNew.values() ).length);
                if (Array.from( postsNew.values() ).length < 1) {
                    setNoPosts(true);
                }
                setRefreshing(false);   

            });
        });
    } catch (error) {
        console.error(error);
    }

  }, [refreshing]);

  const onProfilePicturePressed = (userId, author) => {
    if (author === "Anonymous") {
        Alert.alert(
            "Can't View Profile",
            "This post was posted anonymously ;)"
        );
    } else {
        usersRef.doc(userId).get().then((doc) => {
            let postUser = doc.data();

            if (postUser !== undefined && postUser !== null) {
                setPressedProfile(postUser);
            } else {
                Alert.alert(
                    "Profile does not exist",
                    "The profile that created this post no longer exists"
                );
            }
        }).catch((error) => {
            console.log("Error getting document:", error);
        });
    }
  };

  const isLiked = (id) => {
    if (likedPosts !== undefined && id !== undefined) {
        return likedPosts.includes(id);
    } else {
        return false;
    }
  };

  const onLikePress = (id) => {

    if (likedPosts.includes(id)) {
        userRef.update({
            likedPosts: firebase.firestore.FieldValue.arrayRemove(id)
        }); 
        firebase.firestore().collection('main_data')
            .doc(currentUser.cityId)
            .collection("posts")
            .doc(id)
            .set({likes: firebase.firestore.FieldValue.increment(-1)}, {merge: true})
        .catch((error) => {
            console.error("Error adding document: ", error);
        });

        var array = [...likedPosts]; // make a separate copy of the array
        var index = array.indexOf(id)
        array.splice(index, 1);
        setLikedPosts(array);
    } else {

        userRef.update({
            likedPosts: firebase.firestore.FieldValue.arrayUnion(id)
        });
        firebase.firestore().collection('main_data')
            .doc(currentUser.cityId)
            .collection("posts")
            .doc(id)
            .set({likes: firebase.firestore.FieldValue.increment(1)}, {merge: true})
        .catch((error) => {
            console.error("Error adding document: ", error);
        });
        setLikedPosts([...likedPosts, id]);
    }

  };

  const postImageTapped = (imageLink) => {
    const image = [
        {
          source: {
            uri: imageLink,
          },
        },
      ];
    setTappedImage(image);
    setIsImageViewVisible(true);
  }

  const actions = [
    {
      text: "Post",
      icon: <Icon style={{height: 25, width: 25}} name="account" size={25} color="blue"/> ,
      name: "add_post",
      position: 2
    },
  ];


const setAddCommentVisibleState = (bool) => {
    setAddCommentVisible(bool);
    setAddPostVisible(bool);
}



const getRandomColor = (id) => {
    // var index = Math.floor(Math.random() * 12);

    var index = 0;
    if (id > 12) {
        index = id;
    } else {
        index = id % 12;
    }

    var colors = ["#f07167", "#ffd166", "#06d6a0", "#118ab2", "#073b4c", "#003566", "#ef233c", "#adb5bd", "#ba181b", "#4ecdc4", "#f3722c", "#4d194d"];
    return colors[index];
    // return 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')';
}

const animatePostButton = () => {
    this.floatingAction.animateButton();
    setAddPostVisible(!addPostVisible);
}

const populateCommentsPage = async (id, body, date, author, postId, likeCount, images, profilePic) => {
    setShowComments(!showComments);
    setCurCommentId(id);
    setCurCommentBody(body);
    setCurCommentDate(date);
    setCurCommentAuthor(author);
    setCurPostId(postId);
    setCurPostLikeCount(likeCount);
    setCurPostImages(images);
    setCurPostProfilePic(profilePic);
}

const getTimeFromNow = (time) => {
    const timeNow = firebase.firestore.Timestamp.now().seconds;
    const difference = timeNow - time;
    const inMins = Math.floor(difference / 60);

    if (inMins <= 1) {
        return "Just now"
    }

    if (inMins < 60) {
        return inMins.toString() + " mins ago";
    } else {
        const inHours = Math.floor(inMins / 60);
        if (inHours >= 24) {
            const inDays = Math.floor(inHours / 24);
            if (inDays > 1) {
                return inDays.toString() + " days ago";
            } else {
            return inDays.toString() + " day ago";
            }
        } else {
            return inHours.toString() + " hours ago";
        }
    }
}

const closeProfileShow = () => {
    setPressedProfile(null);
}

const getReplyString = (count) => {
    var returnVal = "";

    if (count == 1) {
        returnVal = "1 reply";
    } else if (count > 1) {
        returnVal = count.toString() + " replies";
    }
    return returnVal;
}

const PostCommentsView = ({}) => (
    <SafeAreaView style={styles.feed}>
        <Post id={curCommentId} body={curCommentBody} timestamp={curCommentDate} author={curCommentAuthor} postId={curPostId} likeCount={curPostLikeCount} images={curPostImages} profilePicture={curPostProfilePic}/>
        <Comments showPostOptions={showPostOptions} setAddComment={setAddCommentVisibleState} postId={curPostId} />
    </SafeAreaView>
);

const arrayToObject = (array) => {
    if (array == undefined) {
        return undefined;
    }

    var output = [];
    var index = 0;

    for (let elem of array) {
        output.push({id: index, imageUrl: elem});
        index++;
    }
    return output;
}

const checkImageExists = (url, author, id) => {
    
    // console.log("Checking image url");
    fetch(url)
        .then(res => {
            // console.log("Fetched URL");
            if (res.status == 404) {
                console.log("404");
                return (
                    <View style={circle(getRandomColor(id))}>
                        <Text style={styles.annonText}>{author.substring(0,1).toUpperCase()}</Text>
                    </View>
                );
            } else {
                console.log("got image");

                return (
                    <FastImage
                        source={{uri: url}}
                        style={styles.profilePictureStyle}
                        resizeMode="cover"
                        on
                    />
                );
            }
        })
        .catch(error => { 
            console.log("Error");
            return ( 
                <View style={circle(getRandomColor(id))}>
                    <Text style={styles.annonText}>{author.substring(0,1).toUpperCase()}</Text>
                </View>
            );
         });
}

const renderImage = ({ item }) => {
    if (item == undefined) {
        return;
    }

    return (
        <View style={{width: 200, height: 150, marginRight: 20}}>
            <TouchableOpacity
                onPress={() => postImageTapped(item.imageUrl)}
            >
                <FastImage 
                    source={{uri: item.imageUrl}}
                    style={styles.postImage}
                    resizeMode="cover"
                    on
                />
            </TouchableOpacity>
        </View>

    );
}

const showActionSheet = () => {
    this.ActionSheet.show()
}

const reportPost = (postId, authorId, postBody, timestamp, images) => {
    console.log(currentUser.cityId);
    const reportedPostsRef = firebase.firestore().collection('main_data')
        .doc(currentUser.cityId)
        .collection('reported_posts')
        .doc(postId);

    reportedPostsRef.set({
        postId: postId, 
        postAuthorId: authorId,
        postBody: postBody,
        postTimestamp: timestamp,
        postImages: images,        
        timesReported: firebase.firestore.FieldValue.increment(1)
      }, {merge: true});
  }

const deletePost = (postId, commentId) => {
    if (commentId) {
        firebase.firestore().collection('main_data')
            .doc(currentUser.cityId)
            .collection('posts')
            .doc(curPostId)
            .collection('comments')
            .doc(commentId)
            .delete();
    } else {
        firebase.firestore().collection('main_data')
            .doc(currentUser.cityId)
            .collection('posts')
            .doc(postId)
            .delete().then(() => {
                const postsNew = new Map();

                posts.forEach((value, key) => {
                    if (key !== postId) {
                        postsNew.set(key, value)
                    }
                })  
                setPosts(postsNew);
            });
    }
};

const showPostOptions = (body, timestamp, postId, images, authorId, commentId) => {
    if (authorId === currentUser.id) {
        Alert.alert(
            'Post',
              "Your post options",
            [
              {
                text: "Delete Post",
                onPress: () => deletePost(postId, commentId)
              },  
              { 
                text: 'Cancel',
              }
            ],
            {
              cancelable: false,
            },
          );
    } else {
        Alert.alert(
            'Post',
              "Is this post inappropriate?",
            [
              {
                text: "Report Post",
                onPress: () => reportPost(postId, authorId, body, timestamp, images)
              },  
              { 
                text: 'Cancel',
              }
            ],
            {
              cancelable: false,
            },
          );
    }
}

//   const userRef = firebase.firestore().collection('users').doc(currentUser.id);
 
  const Post = ({id, body, timestamp, author, postId, commentCount, likeCount, images, authorId, profilePicture}) => (
    <View style={styles.post}>
        <ScrollView>
            {showComments && <View style={{flex: 1, height: 40, width: "100%", marginTop: 0}}>
                <TouchableOpacity 
                    style={{marginTop: 0, marginLeft: 0, width: "100%", height: 40, flex: 1}}
                    onPress={() => setShowComments(!showComments)}
                >   
                    <Icon style={{height: 30, width: 30}} name="keyboard-backspace" size={30} color={DynamicAppStyles.colorSet[colorScheme].mainTextColor}/> 
                </TouchableOpacity>
            </View>}
            <View style={{flexDirection: "row", justifyContent: "space-between", alignItems: "center"}}>
                <View style={{flexDirection: "row", alignItems: "center"}}>
                    <TouchableOpacity style={styles.profilePictureStyle} onPress={() => onProfilePicturePressed(authorId, author)}>
                        {/* {checkImageExists(profilePicture, author, id)} */}
                        {profilePicture == undefined || profilePicture == null || author == "Anonymous" ?
                            <View style={circle(getRandomColor(id))}>
                                <Text style={styles.annonText}>{author.substring(0,1).toUpperCase()}</Text>
                            </View>
                            :
                            <FastImage
                                source={{uri: profilePicture}}
                                style={styles.profilePictureStyle}
                                resizeMode="cover"
                                on
                            />
                        }
                    </TouchableOpacity>

                    <View style={styles.authorAndDate}>
                        <Text style={styles.postTitle}>{author}</Text>
                        <Text style={styles.postDate}>{getTimeFromNow(timestamp.seconds)}</Text>
                    </View>
                </View>
                    <TouchableOpacity onPress={() => showPostOptions(body, timestamp, postId, images, authorId, null)}>
                        <Icon style={{height: 30, width: 30}} name="dots-vertical" size={24} color={DynamicAppStyles.colorSet[colorScheme].mainTextColor}/> 
                    </TouchableOpacity>
            </View>
            
            <Text numberOfLines={10} style={styles.postBody}>{body}</Text>

            {/* {image !== undefined &&  <TouchableOpacity
                onPress={() => postImageTapped(image)}
            >
                <FastImage 
                    source={{uri: image}}
                    style={styles.postImage}
                    resizeMode="cover"
                    on
                />
            </TouchableOpacity>} */}
            {images !== undefined && images.length > 0 && 
                <FlatList 
                    horizontal
                    style={{height: 150, marginBottom: 20, alignSelf: "flex-start"}}
                    data={images}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={(item, index) => item.id.toString() }
                    renderItem={renderImage}
                />
            }


        </ScrollView>
        

        <View style={{ marginHorizontal: 8, marginBottom: 2, flexDirection: "row", justifyContent: "space-between", alignItems: "center"}}>
            <TouchableOpacity style={{flexDirection: "row"}} onPress={() => onLikePress(postId)}>
                {isLiked(postId) ?
                    <Icon style={{height: 20, width: 20}} name="thumb-up" size={20} color={DynamicAppStyles.colorSet[colorScheme].mainTextColor}/> 
                :    
                    <Icon style={{height: 20, width: 20}} name="thumb-up-outline" size={20} color={DynamicAppStyles.colorSet[colorScheme].mainTextColor}/> 
                }
                <Text style={{color: DynamicAppStyles.colorSet[colorScheme].mainTextColor, fontSize: 12}}>{likeCount == 0 ? "" : likeCount}</Text>
            </TouchableOpacity>
             
            <TouchableOpacity 
                onPress={() => populateCommentsPage(id, body, timestamp, author, postId, likeCount, images, profilePicture)}
            >   
                <View style={{flexDirection: "row"}}>
                    <Text style={{color: DynamicAppStyles.colorSet[colorScheme].mainTextColor, fontSize: 12}}>{getReplyString(commentCount)} </Text>
                    {showComments ?
                        <Icon style={{height: 20, width: 20}} name="comment" size={20} color={DynamicAppStyles.colorSet[colorScheme].mainTextColor}/> 
                    :
                        <Icon style={{height: 20, width: 20}} name="comment-outline" size={20} color={DynamicAppStyles.colorSet[colorScheme].mainTextColor}/> 
                    }
                </View>
    
            </TouchableOpacity>

        </View>

    </View>
  );

  const renderItem = ({ item }) => (
    <Post id={item.id} body={item.body} timestamp={item.timestamp} author={item.author} postId={item.postId} commentCount={item.commentsCount} likeCount={item.likes} images={arrayToObject(item.images)} authorId={item.authorId} profilePicture={item.authorProfilePicture}/>
  );

  const options = [
    { label: "Campus Newsfeed", value: "feeds" },
    { label: "Deals & Events", value: "deals" },
  ];

  return (
    <View style={styles.MainContainer}>
        {/* <View style={styles.AddPost}> */}
            {/* <SwitchSelector
                style={{width: "100%", alignSelf:"center", height: "100%"}}
                borderRadius={14}
                options={options}
                initial={0}
                height={60}
                textStyle={{fontSize: 20, fontWeight: "bold"}}
                selectedTextStyle={{color:"black", fontSize: 20, fontWeight: "bold"}}
                buttonColor={"white"}
                backgroundColor={"transparent"}
                // buttonColor={DynamicAppStyles.colorSet[colorScheme].mainThemeForegroundColor}
                onPress={() => setScreenIndex(!screenIndex)}
            /> */}
            {/* <Text style={styles.titleText}>
                {appConfig.onboardingConfig.uniName} Campus Newsfeed
            </Text> */}
        {/* </View> */}

        <Modal2
            visible={pressedProfile !== null}
            animationType={"slide"}

        >
            {pressedProfile && <CardDetailsView
                profilePictureURL={pressedProfile?.profilePictureURL}
                firstName={pressedProfile?.firstName || ""}
                lastName={pressedProfile?.lastName}
                uniName={pressedProfile?.universityName || ""}
                bio={pressedProfile?.profile?.about}
                year={pressedProfile?.profile?.year}
                course={pressedProfile?.profile?.studying}
                oncampus={pressedProfile?.profile?.on_campus}
                lookingfor = {pressedProfile?.settings?.want}
                instagramPhotos={pressedProfile?.photos} 
                interests={pressedProfile?.interests}
                trafficLight={pressedProfile.settings?.traffic_light}
                setShowMode={closeProfileShow}
                reportUser={pressedProfile?.id}
            />}
        </Modal2>

        <Modal
            animationType="slide"
            isVisible={addPostVisible}
            onRequestClose={() => setAddPostVisible(!addPostVisible)}
            coverScreen={false}
            backdropOpacity={1.0}
            backdropColor="white"
            swipeDirection="down"
            onSwipeComplete={(e) => { setAddPostVisible(false); }}
        >   
            <View style={{marginTop: 24}}>
                {addComment ?
                <AddComment postId={curPostId} setAddCommentVisibleState={setAddCommentVisibleState} />
                :
                <AddPost refresh={onRefresh} animatePostButton={animatePostButton} currentUser={currentUser}/>
                }
                
            </View>
            
        </Modal>
        
        <View style={{flex: 1}}>
            {showComments ?
                <View style={{minHeight: "100%"}}>
                    <PostCommentsView/>
                </View>
            : 
            <SafeAreaView style={styles.feed}>
                {Array.from( posts.values() ).length < 1 && !noPosts  ?
                    <ActivityIndicator />
                    :
                    <View>
                        {/* <Text style={styles.campusText}>Your Campus Newsfeed</Text> */}
                        {!noPosts && <FlatList
                            style={{height: "100%", marginTop: 4}}
                            data={Array.from( posts.values() )}
                            renderItem={renderItem}
                            keyExtractor={item => item.id.toString()}
                            refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
                            onEndReached={() => hydrateList()}
                            onEndReachedThreshold={0.1}
                        />}
                    </View>

                }

                {noPosts && <Text style={styles.noPostsText}>Be the first to make a post!</Text>}
            </SafeAreaView>
            }
        </View>

        {/* <AddPostInline appStyles={styles} /> */}
    

        <ImageView
            images={tappedImage}
            imageIndex={0}
            isVisible={isImageViewVisible}
            onClose={() => setIsImageViewVisible(false)}
        />

        
        {!showComments && <FloatingAction
            ref={ref => { this.floatingAction = ref; }}
            color="#eb5a6d"
            showBackground={false}
            animated={false}
            onPressMain={() => {
                setAddPostVisible(!addPostVisible);
            }}
        /> } 
 
        {/* <ActionSheet
          ref={o => { this.ActionSheet = o }}
          title={'Post Options'}
          options={['Report Post', 'cancel']}
          cancelButtonIndex={1}
          destructiveButtonIndex={0}
          onPress={(index) => console.log(index)}
        /> */}

    </View>
  );
};

// const mapStateToProps = (state) => ({
//   user: state.auth.user,
// });

circle = function(color) {
    return {
        width: 40,
        height: 40,
        borderRadius: 40/2,
        backgroundColor: color,
        alignItems: "center",
        justifyContent: "center"
    }
}

// export default connect(mapStateToProps)(MyProfileScreen);
export default FeedsScreen;
