import { StyleSheet, Platform, Dimensions } from 'react-native';
import DynamicAppStyles from '../../DynamicAppStyles';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import { size } from '../../helpers/devices';



const dynamicStyles = (colorScheme) => {
  return StyleSheet.create({
    MainContainer: {
      flex: 1,
      backgroundColor:
        DynamicAppStyles.colorSet[colorScheme].mainThemeForegroundColor,
    }, 
    bottomInputContainer: {
        backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
        ...ifIphoneX(
            {
              paddingBottom: 25,
            },
            {
              paddingBottom: 5,
            },
          ),
    },
    messageFlatList: {
      flex: 1, 
      ...ifIphoneX(
        {
          marginBottom: 50,
        },
        {
          marginBottom: 30,
        },
      ),

    },
    inputBar: {
        justifyContent: 'center',
        alignItems: 'center',
        borderTopWidth: 1,
        borderTopColor: DynamicAppStyles.colorSet[colorScheme].hairlineColor,
        backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
        flexDirection: 'row',
      },
      input: {
        alignSelf: 'center',
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 3,
        paddingRight: 20,
        width: '86%',
        fontSize: 16,
        marginLeft: 8,
        lineHeight: 22,
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
      },
      inputIconContainer: {
        margin: 10,
        // marginRight: 20,
        flex: 0.5,
      },
      inputIcon: {
        tintColor: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
        width: 25,
        height: 25,
      },
      inputContainer: {
        flex: 8,
        borderRadius: 20,
        backgroundColor: DynamicAppStyles.colorSet[colorScheme].whiteSmoke,
        flexDirection: 'row',
        alignItems: 'center',
        overflow: 'hidden',
      },
      inputIconContainer: {
        margin: 10,
        flex: 0.5,
      },
      messageContentThreadContainer: {
        margin: 6,
      },
      sendItemContainer: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        flexDirection: 'row',
        marginBottom: 10,
      },
      myMessageBubbleContainerView: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        flexDirection: 'column',
        maxWidth: '80%',
      },
      itemContent: {
        padding: 10,
        backgroundColor: DynamicAppStyles.colorSet[colorScheme].hairlineColor,
        borderRadius: 10,
      },
      sendItemContent: {
        marginRight: 9,
        backgroundColor: "#ef8484",
      },
      sendTextMessage: {
        fontSize: 16,
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
      },
      textBoederImgSend: {
        position: 'absolute',
        right: -5,
        bottom: 0,
        width: 20,
        height: 8,
        resizeMode: 'stretch',
        tintColor: "#ef8484",
      },
      textBoederImgReceive: {
        position: 'absolute',
        left: -5,
        bottom: 0,
        width: 20,
        height: 8,
        resizeMode: 'stretch',
        tintColor: DynamicAppStyles.colorSet[colorScheme].hairlineColor,
      },
      theirMessageBubbleContainerView: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexDirection: 'column',
        maxWidth: '80%',
      },
      receiveItemContainer: { 
        justifyContent: 'flex-start',
        alignItems: 'flex-end',
        flexDirection: 'row',
        marginBottom: 10,
      },
      receiveItemContent: {
        marginLeft: 9,
      },
      receiveTextMessage: {
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
        fontSize: 16,
      },
      userIcon: {
        width: 34,
        height: 34,
        borderRadius: 17,
      },
      mediaMessage: {
        width: size(300),
        height: size(250),
        borderRadius: 10,
      },
      senderNameText: {
        marginLeft: 20, 
        opacity: 0.7, 
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor
      },
  });
};

export default dynamicStyles;
