import React, { useCallback, useRef, useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  View,
  Text,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
  RefreshControl,
  ActivityIndicator,
  Image,
  KeyboardAvoidingView,
  Modal,
  Keyboard,
  Alert
} from 'react-native';
import dynamicStyles from './styles';
import { useColorScheme } from 'react-native-appearance';
import { IMIconButton } from '../../Core/truly-native';
import DynamicAppStyles from '../../DynamicAppStyles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import { AutoGrowingTextInput } from 'react-native-autogrow-textinput';
import { KeyboardAccessoryView } from 'react-native-ui-lib/keyboard';
import { firebase } from '../../Core/firebase/config';
import TNActivityIndicator from '../../Core/truly-native/TNActivityIndicator';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';
import CardDetailsView from '../../components/swipe/CardDetailsView/CardDetailsView';
import { KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import { firebaseStorage } from '../../Core/firebase/storage';

const GroupChatScreen = (props) => {
  const currentUser = useSelector((state) => state.auth.user);
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);
  const navigation = props.navigation;
  const textInputRef = useRef(null);
  const [customKeyboard, setCustomKeyboard] = useState({
    component: undefined,
    initialProps: undefined,
  });
  const [disabled, setDisabled] = useState(false);
  const course = currentUser.profile.studying;
  const [messages, setMessages] = useState([]);

  const [loading, setLoading] = useState(false);

  const [newMessage, setNewMessage] = useState("");

  const [showModal, setShowModal] = useState(false);
  const [pressedUser, setPressedUser] = useState(null);

  const [gotMessages, setGotMessages] = useState(false);


  const gotMessagesRef = useRef(false);
  const messagesRef = useRef(messages);
  const initUpdate = useRef(true);

  const groupRef = firebase.firestore().collection('main_data')
    .doc(currentUser.cityId)
    .collection('group_chats')
    .doc(course.replace(/\s/g, "").toLowerCase())
    .collection("messages");

  var userRef = firebase.firestore().collection('main_data')
    .doc(currentUser.cityId)
    .collection('users');

  useEffect(() => {
    configureNavigation();
    onLoad();
    
    const unsubscribe = groupRef.orderBy("timestamp", "desc").limit(1) //.where("timestamp", ">", messages[messages.length - 1].timestamp)
        .onSnapshot(querySnapshot => {
            const messagesNew = [...messagesRef.current];
            let index = messagesNew.length;

            if (gotMessagesRef.current) {
                if (initUpdate.current) {
                    initUpdate.current = false;
                } else {
                    querySnapshot.forEach((messageSnapshot) => {
                        var data = messageSnapshot.data();
                        messagesNew.unshift({...data, id: ++index, messageId: messageSnapshot.id});
                    });
                    setMessages(messagesNew);
                    messagesRef.current = messagesNew;
                }
            }
    });

    return () => unsubscribe();

  }, []);

  const onLoad = () => {
    setLoading(true);
    try {
        const messagesNew = [];
        const messagesRefNew = groupRef.orderBy("timestamp", "desc").limit(20);

        messagesRefNew.get().then((messagesSnapshot) => {
            let index = messages.length;
            messagesSnapshot.forEach((messageSnapshot) => {
                var data = messageSnapshot.data();
                messagesNew.push({...data, id: ++index, messageId: messageSnapshot.id});
            })

            messages.forEach((oldMessage) => {
                messagesNew.push(oldMessage);
            });
            setMessages(messagesNew);

            messagesRef.current = messagesNew;
            setGotMessages(true);
            gotMessagesRef.current = true;
            setLoading(false);
        })

    } catch (error) {
        console.log(error);
        setLoading(false);
    }
  }

  const sendMessage = () => {
    setNewMessage("");
    
    const message = {
        content: newMessage,
        timestamp: firebase.firestore.Timestamp.now(),
        author: currentUser.id,
        authorProfilePicture: currentUser.profilePictureURL,
        authorName: currentUser.firstName,
    }

    try {
        groupRef.add(message).catch((error) => {
            console.log("Error adding message: ", error);
        });
        // Keyboard.dismiss();
    } catch (error) {
        console.log(error);
    }


  };

  const resetKeyboardView = () => {
    setCustomKeyboard({});
  };

  const onKeyboardResigned = () => {
    resetKeyboardView();
  };

  const configureNavigation = () => {
    navigation.setOptions({
        headerTitle: () => (
          <Text style={{color: "#ef8484", fontSize: 20, fontWeight: "500" }}>{currentUser.profile.studying}</Text>
        ),
        headerStyle: {
          backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeForegroundColor,
        },
        headerBackTitleVisible: false,
        headerTintColor: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
        headerRight: () => (
          <View style={{ flexDirection: 'row' }}>
            <IMIconButton
              source={require('../../Core/chat/assets/settings-icon.png')}
              tintColor={DynamicAppStyles.colorSet[colorScheme].mainTextColor}
            //   onPress={onSettingsPress}
              marginRight={15}
              width={20}
              height={20}
            />
          </View>
        ),
      });
  }

  const typingFinished = (text) => {
      if (text == "") {
          setDisabled(true);
          setNewMessage(text);
      } else {
          setDisabled(false);
          setNewMessage(text);
      }
  }

  const renderBottomInput = () => {
      return (
        <View style={styles.bottomInputContainer}>
            <View style={styles.inputBar}>
                <TouchableOpacity
                    onPress={() => sendImage()}
                    style={styles.inputIconContainer}>
                    <Image style={styles.inputIcon} source={require("../../Core/chat/assets/camera-filled.png")} />
                </TouchableOpacity>
                <View style={styles.inputContainer}>
                    <AutoGrowingTextInput
                        maxHeight={100}
                        style={styles.input}
                        multiline={true}
                        ref={textInputRef}
                        value={newMessage}
                        onChangeText={(text) => typingFinished(text)}
                        placeholder={"Start typing..."}
                        placeholderTextColor={
                            DynamicAppStyles.colorSet[colorScheme].mainTextColor
                        }
                        underlineColorAndroid="transparent"
                        onFocus={resetKeyboardView}
                    />
                </View>

                <TouchableOpacity
                    disabled={disabled}
                    onPress={() => sendMessage()}
                    style={[
                        styles.inputIconContainer,
                        disabled ? { opacity: 0.2 } : { opacity: 1 },
                    ]}>
                    <Image style={styles.inputIcon} source={require('../../Core/chat/assets/send.png')} />
                </TouchableOpacity>
            </View>

        </View>
      );
  }

  const onOtherUserPress = (otherUserId) => {
    userRef.doc(otherUserId).get().then((userSnapshot) => {
        const data = userSnapshot.data();
        setPressedUser(data);
        setShowModal(true);
    })
  }

  const allMessagesSwiped = () => {
    console.log("End Reached");

    if (messages.length >= 20) {
        try {
            const messagesNew = [];
            const next10Messages = groupRef.where("timestamp", "<", messages[messages.length - 1].timestamp).orderBy("timestamp", "desc").limit(5);
    
            next10Messages.get().then((messagesSnapshot) => {
                let index = messages.length;
                messages.forEach((oldMessage) => {
                    messagesNew.push(oldMessage);
                });
                messagesSnapshot.forEach((messageSnapshot) => {
                    var data = messageSnapshot.data();
                    messagesNew.push({...data, id: ++index, messageId: messageSnapshot.id});
                })
    
    
                setMessages(messagesNew);
    
                messagesRef.current = messagesNew;
                setGotMessages(true);
                gotMessagesRef.current = true;
                setLoading(false);
            })
    
        } catch (error) {
            console.log(error);
            setLoading(false);
        }
    }
  }

  const renderTextBoederImg = (id) => {
    if (id === currentUser.id) {
      return (
        <Image
          source={require('../../Core/chat/assets/textBorderImg1.png')}
          style={styles.textBoederImgSend}
        />
      );
    }

    if (id !== currentUser.id) {
      return (
        <Image
          source={require('../../Core/chat/assets/textBorderImg2.png')}
          style={styles.textBoederImgReceive}
        />
      );
    }
  };

  const sendImage = async () => {

    await getPermissionAsync();

    let image = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
    });

    if (!image.cancelled) {
        firebaseStorage.uploadFile(image, currentUser.id).then((response) => {
            const url = response.downloadURL;
            const message = {
                timestamp: firebase.firestore.Timestamp.now(),
                author: currentUser.id,
                authorProfilePicture: currentUser.profilePictureURL,
                imageUrl: url,
            }
            groupRef.add(message).catch((error) => {
                console.log("Error adding message: ", error);
            });
        });
    }
  }

  const getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        Alert.alert(
          '',
            'Sorry, we need camera roll permissions to make this work!',
          [{ text: 'Ok' }],
          {
            cancelable: false,
          },
        );
      }
    }
  };

  const renderChatItem = ({item, index}) => {

    const outbound = item.author == currentUser.id;

    return (
        <View

        >
            <View>
                {outbound && 
                    <View style={styles.sendItemContainer}>
                        <View style={[styles.myMessageBubbleContainerView]}>
                            {/* {renderInReplyToIfNeeded(item, true)} */}
                            {!item.imageUrl && <View style={[styles.itemContent, styles.sendItemContent]}>
                                <Text style={styles.sendTextMessage}>{item.content}</Text>
                                {renderTextBoederImg(item.author)}
                            </View>}
                            {item.imageUrl && item.imageUrl != '' && <View style={[styles.itemContent, styles.sendItemContent]}>
                                <FastImage 
                                    style={styles.mediaMessage}
                                    source={{uri: item.imageUrl}}
                                />                                
                                {renderTextBoederImg(item.author)}
                            </View>}
                        </View>
                        <TouchableOpacity>
                            <FastImage
                            style={styles.userIcon}
                            source={{ uri: item.authorProfilePicture }}
                            />
                        </TouchableOpacity>
                    </View>
                    
                }
                {!outbound && 
                    <View style={styles.receiveItemContainer}>
                        <TouchableOpacity
                            onPress={() => onOtherUserPress(item.author)}
                            >
                                <FastImage
                                    style={styles.userIcon}
                                    source={{ uri: item.authorProfilePicture }}
                                />
                        </TouchableOpacity>
                        <View style={[styles.theirMessageBubbleContainerView]}>
                            {/* {renderInReplyToIfNeeded(item, true)} */}
                            <Text style={styles.senderNameText}>
                                {item.authorName}
                            </Text>
                            {!item.imageUrl && <View style={[styles.itemContent, styles.receiveItemContent]}>
                                <Text style={styles.receiveTextMessage}>{item.content}</Text>
                                {renderTextBoederImg(item.author)}
                            </View>}
                            {item.imageUrl && item.imageUrl != '' && <View style={[styles.itemContent, styles.receiveItemContent]}>
                                <FastImage 
                                    style={styles.mediaMessage}
                                    source={{uri: item.imageUrl}}
                                />                                
                                {renderTextBoederImg(item.author)}
                            </View>
                            }
                        </View>
                    </View>
                }
            </View>

        </View>
    );
  }

  return (
    <SafeAreaView style={styles.MainContainer}>
        {loading && 
            <ActivityIndicator style={{marginTop: 200}}/>
        }

        <Modal animationType="slide" visible={showModal}>
            <CardDetailsView  
                profilePictureURL={pressedUser?.profilePictureURL}
                firstName={pressedUser?.firstName}
                lastName={pressedUser?.lastName}
                school={pressedUser?.school}
                distance={pressedUser?.distance}
                trafficLight={pressedUser?.settings?.traffic_light}
                interests={pressedUser?.interests}
                instagramPhotos={pressedUser?.photos}
                uniName={pressedUser?.universityName}
                bio={pressedUser?.profile?.about}
                year={pressedUser?.profile?.year} 
                course={pressedUser?.profile?.studying}
                oncampus={pressedUser?.profile?.on_campus}
                lookingfor = {pressedUser?.settings?.want}
                setShowMode={setShowModal}
                reportUser = {pressedUser?.id}
            />
        </Modal>

            {messages.length > 0 && <KeyboardAwareFlatList
                style={styles.messageFlatList}
                inverted={true}
                vertical={true}
                showsVerticalScrollIndicator={false}
                data={messages}
                renderItem={renderChatItem}
                keyExtractor={(item) => `${item.id}`}
                contentContainerStyle={styles.messageContentThreadContainer}
                removeClippedSubviews={true}
                // ListHeaderComponent={() => renderListHeaderComponent()}
                // keyboardShouldPersistTaps={'never'}
                onEndReached={() => allMessagesSwiped()}
                onEndReachedThreshold={0.1}
            />}
            <KeyboardSpacer topSpacing={-60}/>
        
        <KeyboardAccessoryView 
            scrollB
            renderContent={renderBottomInput}
            // trackInteractive={trackInteractive}
            kbInputRef={textInputRef}
            kbComponent={customKeyboard.component}
            kbInitialProps={customKeyboard.initialProps}
            // onItemSelected={onCustomKeyboardItemSelected}
            onKeyboardResigned={onKeyboardResigned} 
            manageScrollView={false}
        />
        

    </SafeAreaView>
  );
};


export default GroupChatScreen;
