import { StyleSheet } from 'react-native';

const imageContainerWidth = 66;
const imageWidth = imageContainerWidth - 6;

const dynamicStyles = (colorScheme, appStyles) => {
  return StyleSheet.create({
    mainContainer: {
      // flex: 1,
      width: "100%",
      height: "100%",
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    container: {
      // flex: 1,
      width: "100%",
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    userImageContainer: {
      borderWidth: 0,
    },
    chatsChannelContainer: {
      // flex: 1,
      padding: 10,
    },
    content: {
      flexDirection: 'row',
    },
    message: {
      flex: 2,
      color: appStyles.colorSet[colorScheme].mainSubtextColor,
    },
    title: {
      color: appStyles.colorSet[colorScheme].mainTextColor,
      fontSize: 26,
      fontWeight: "500",
      // marginVertical: 10,
      marginLeft: 14,
    },
    underline: {
      marginLeft: 20, 
      height: 4, 
      width: 30, 
      backgroundColor: "#ef8484",
      marginBottom: 5
    },
    expandText: {
      color: appStyles.colorSet[colorScheme].mainTextColor,
      fontSize: 16,
      textDecorationLine: "underline",
    },
    matesDatesBtnContainer: {
      width: "100%", 
      marginVertical: 10,
      alignItems: "center", 
      flexDirection: "row", 
      justifyContent: "center"
    },
    matesDatesBtn: {
      height: 40,
      marginHorizontal: 20,
      borderRadius:12,
      width: 130,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "#ef8484"
    },
    text: {
      fontSize: 12,
      // textAlign: 'center',
      marginLeft: 5,
      color: appStyles.colorSet[colorScheme].mainTextColor,
      paddingTop: 5,
    },
    imageContainer: {
      width: imageContainerWidth,
      height: imageContainerWidth,
      borderRadius: Math.floor(imageContainerWidth / 2),
      borderColor: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
      borderWidth: 2,
      justifyContent: 'center',
      alignItems: 'center',
    },
    image: {
      width: imageWidth,
      height: imageWidth,
      borderRadius: Math.floor(imageWidth / 2),
      borderColor: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
      borderWidth: 1,
      // overflow: 'hidden',
    },
  });
};

export default dynamicStyles;
