import React, {useState, useEffect} from 'react';
import { 
  ScrollView, 
  View, 
  TouchableOpacity, 
  Text, 
  Image } from 'react-native';
import { useColorScheme } from 'react-native-appearance';

import { TNStoriesTray } from '../../Core/truly-native';

import Groups from './Groups/Groups'

import { IMConversationListView } from '../../Core/chat';
import dynamicStyles from './styles';
import { createChannel, joinGroup } from '../../Core/chat/firebase/channel';
import { useSelector } from 'react-redux';
import { firebase } from '../../Core/firebase/config';
import Swiper from 'react-native-swiper';

function ConversationsHomeComponent(props) {
  const {
    matches, 
    mates,
    dates,
    onMatchUserItemPress,
    navigation,
    appStyles,
    emptyStateConfig,
    
  } = props;
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme, appStyles); 
  
  const currentUser = useSelector((state) => state.auth.user);
  const [randomUser, setRandomUsers] = useState({});
  // const [expanded, setExpanded] = useState(false);

  var usersRef = firebase.firestore().collection('main_data')
    .doc(currentUser.cityId)
    .collection('users');

  const getUsersAndCreateGroup = () => {
    // const channelsRef = firebase.firestore().collection(currentUser.universityId)
    //   .doc("channels")
    //   .collection("channels");

    // var randomNewUser = null;
    // usersRef.limit(1).get().then((usersSnapshot) => {
    //   usersSnapshot.forEach((userSnapshot) => {
    //     randomNewUser =  userSnapshot.data();
    //   });
    //   channelsRef
    //     .doc("Computing")
    //     .update({
    //       participants: firebase.firestore.FieldValue.arrayUnion(randomNewUser)
    //     });
    // });
    const randomUsersNew = [];
    usersRef.limit(3).get().then((usersSnapshot) => {
      usersSnapshot.forEach((userSnapshot) => {
        randomUsersNew.push(userSnapshot.data());
      });
      
      joinGroup(currentUser, randomUsersNew);
      // createChannel(currentUser, randomUsersNew, "Computing"); 
    });
  };

  return (
    <View style={styles.mainContainer}>
      <View>
        <Text style={styles.title}>Groups</Text>
        <View style={styles.underline}></View>

        {/* <Groups 
          currentUser={currentUser}
          onStoryItemPress={onMatchUserItemPress}
        /> */}

        <TouchableOpacity
          style={{margin: 8}}
          onPress={() => navigation.navigate(
            'GroupChat',
            {
              appStyles,
            },
          )}
        >
          <View style={styles.imageContainer}>
            <Image
              style={styles.image}
              source={require('../../../assets/images/courseGroup.png')}
            />
          </View>
          <Text style={styles.text}>{currentUser?.profile?.studying}</Text>
        </TouchableOpacity>

        {/* <TNStoriesGroupTray 
          onStoryItemPress={onMatchUserItemPress}
          storyItemContainerStyle={styles.userImageContainer}
          data={matches}
          displayLastName={false}
          appStyles={appStyles}
          showOnlineIndicator={true}
          showGroups={true}
        /> */}

        {/* <TouchableOpacity
          onPress={() => getUsersAndCreateGroup()}
        >
          <Text style={{color: "black"}}>Create group</Text>
        </TouchableOpacity> */}

      </View>

      <Swiper removeClippedSubviews={false} activeDotColor={"#ef8484"} style={{minHeight: "75%"}} showsPagination={true} loop={false}>
        <View style={{minHeight: "75%"}}>
          <Text style={styles.title}>Matches - Dates</Text>
          <View style={styles.underline}></View>

          <ScrollView style={styles.container}>
            <View style={styles.datesContainer}>
              <TNStoriesTray
                onStoryItemPress={onMatchUserItemPress}
                storyItemContainerStyle={styles.userImageContainer}
                data={dates}
                displayLastName={false}
                appStyles={appStyles}
                showOnlineIndicator={true}
                matesOrDates={"dates"}
              />
            </View>

            <View style={styles.chatsChannelContainer}>
              <IMConversationListView 
                navigation={navigation}
                appStyles={appStyles}
                emptyStateConfig={emptyStateConfig}
                mates={false}
                emptyMatches={dates.length == 0}
                showGroups={false}
              />
            </View>

            {/* <TouchableOpacity
              onPress={() => getUsersAndCreateGroup()}
            >
              <Text style={{color: "white"}}>Create group</Text>
            </TouchableOpacity> */}
          </ScrollView>
        </View>

        <View style={{minHeight: "75%"}}>
          <Text style={styles.title}>Matches - Mates</Text>
          <View style={styles.underline}></View>

          <ScrollView style={styles.container}>
            <View style={styles.datesContainer}>
              <TNStoriesTray
                onStoryItemPress={onMatchUserItemPress}
                storyItemContainerStyle={styles.userImageContainer}
                data={mates}
                displayLastName={false}
                appStyles={appStyles}
                showOnlineIndicator={true}
                matesOrDates={"mates"}
              />
            </View>

            <View style={styles.chatsChannelContainer}>
              <IMConversationListView 
                navigation={navigation}
                appStyles={appStyles}
                emptyStateConfig={emptyStateConfig}
                mates={true}
                emptyMatches={mates.length == 0}
                showGroups={false}
              />
            </View>

            {/* <TouchableOpacity
              onPress={() => getUsersAndCreateGroup()}
            >
              <Text style={{color: "black"}}>Create group</Text>
            </TouchableOpacity> */}
          </ScrollView>
        </View>

      </Swiper>


    </View>
  );
}

export default ConversationsHomeComponent;
