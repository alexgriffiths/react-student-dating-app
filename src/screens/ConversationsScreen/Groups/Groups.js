import React, { useCallback, useRef, useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  Image
} from 'react-native';
import dynamicStyles from './styles';
import { useColorScheme } from 'react-native-appearance';

const defaultAvatar =
  'https://www.iosapptemplates.com/wp-content/uploads/2019/06/empty-avatar.jpg';

const groupImage = require("../../../../assets/images/courseGroup.png");

const Groups = (props) => {
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);
  const refs = useRef();
  const channels = useSelector((state) => state.chat.channels);
  const currentUser = props.currentUser;

  console.log(channels);

  const renderItem = ({ item, index }) => {
    return (
        <TouchableOpacity
            style={styles.groupContainer}
            onPress={() => props.onStoryItemPress(item, index, refs, null, true)}
        >
            <View style={{...styles.imageContainer}}>
                <Image
                style={styles.image}
                source={groupImage}
                />
            </View>
            {item.id ? 
            <Text style={styles.text}>{item.id.charAt(0).toUpperCase() + item.id.slice(1)}</Text>
            :
            <Text style={styles.text}>Course Groupchat</Text>
            }
        </TouchableOpacity>
    );
  };


  return (
    <View style={styles.mainContainer}>
        {channels ? 
            <FlatList
                style={styles.storiesContainer}
                data={channels.filter((elem) => elem.isGroup)}
                //   inverted={I18nManager.isRTL}
                renderItem={renderItem}
                keyExtractor={(item, index) => index + 'item'} 
                horizontal={true}
                showsHorizontalScrollIndicator={false}
            />
        :
            <ActivityIndicator/>
        }

    </View>
  );
};


export default Groups;
