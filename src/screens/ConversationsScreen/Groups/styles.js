import { StyleSheet } from 'react-native';
import DynamicAppStyles from '../../../DynamicAppStyles';

const imageContainerWidth = 66;
const imageWidth = imageContainerWidth - 6;

const dynamicStyles = (colorScheme) => {
  return StyleSheet.create({
    mainContainer: {
      width: "100%",
      marginBottom: 5,
      backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    groupContainer: {
        justifyContent: "center",
        alignItems: "center",
        margin: 8,
        // overflow: 'hidden',
    },
    imageContainer: {
        width: imageContainerWidth,
        height: imageContainerWidth,
        borderRadius: Math.floor(imageContainerWidth / 2),
        borderColor: DynamicAppStyles.colorSet[colorScheme].mainThemeForegroundColor,
        borderWidth: 2,
        justifyContent: 'center', 
        alignItems: 'center',
  
        // shadowOffset: {
        //   width: 0,
        //   height: 0,
        // },
        // shadowOpacity: 1,
        // shadowRadius: 3,
  
        // elevation: 40,
      },
      image: {
        width: imageWidth,
        height: imageWidth,
        borderRadius: Math.floor(imageWidth / 2),
        borderColor: DynamicAppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
        borderWidth: 1,
        // overflow: 'hidden',
      },
      text: {
        fontSize: 12,
        textAlign: 'center',
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
        paddingTop: 5,
      },
  });
}; 

export default dynamicStyles;
