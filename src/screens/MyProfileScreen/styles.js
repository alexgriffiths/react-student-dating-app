import { StyleSheet, Platform, Dimensions } from 'react-native';
import DynamicAppStyles from '../../DynamicAppStyles';

const width = Dimensions.get('window').width;
const { height } = Dimensions.get('window');
const imageSize = height * 0.14;

const dynamicStyles = (colorScheme) => {
  return StyleSheet.create({
    MainContainer: {
      flex: 1,
      backgroundColor:
        DynamicAppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    safeAreaContainer: {
      flex: 1,
      backgroundColor:
        DynamicAppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    holder: {
      flex: 1, 
      width: "100%",
      height: "100%",
      alignItems: "center",
      backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeBackgroundColor
    },
    body: {
      width: '100%',
    },
    photoView: {
      top: Platform.OS === 'ios' ? '4%' : '1%',
      width: 146,
      height: 146,
      borderRadius: 73,
      backgroundColor: 'grey',
      overflow: 'hidden',
      alignSelf: 'center',
    },

    InputContainer: {
      height: 42,
      borderWidth: 1,
      borderColor: DynamicAppStyles.colorSet[colorScheme].grey3,
      backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
      paddingLeft: 20,
      color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
      width: '80%',
      alignSelf: 'center',
      marginTop: 20,
      alignItems: 'center',
      borderRadius: DynamicAppStyles.roundness.textInputRadius,
      ...DynamicAppStyles.shadows.buttonShadow
    },

    DeleteReason: {
      height: 100,
      borderWidth: 1,
      borderColor: DynamicAppStyles.colorSet[colorScheme].grey3,
      backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
      paddingLeft: 20,
      color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
      width: '80%',
      alignSelf: 'center',
      marginTop: 20,
      alignItems: 'center',
      borderRadius: DynamicAppStyles.roundness.textInputRadius,
      ...DynamicAppStyles.shadows.buttonShadow
    },

    deleteModalText: {
      fontSize: 20, 
      marginBottom: 40,
      textAlign: "center",
      color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
    },

    loginText: {
      color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
    },

    loginContainer: {
      width: '100%',
      backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeForegroundColor,
      // borderRadius: 25,
      padding: 10,
      marginTop: 40,
      alignSelf: 'center',
      ...DynamicAppStyles.shadows.buttonShadow
    },

    profilePictureContainer: {
      // width: "100%",
      // height: 100,
      minHeight: imageSize + 90,
      paddingTop: imageSize * 0.25,
      alignItems: "center",
      justifyContent: "center",
    },
    nameView: {
      width: '100%',
      marginTop: -20,
      justifyContent: 'center', 
      alignItems: 'center',
    },
    name: {
      fontSize: 24,
      fontWeight: '500',
      letterSpacing: 3,
      // marginRight: 10,
      color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
      padding: 10,
    },
    myphotosView: {
      width: '100%',
      paddingHorizontal: 12,
      marginTop: 10,
      marginBottom: 15,
    },
    itemView: {
      width: '100%',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: 11,
    },
    slide: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    slideActivity: {
      height: '100%',
      width: '90%',
    },
    myphotosItemView: {
      width: Math.floor(width * 0.24),
      height: Math.floor(width * 0.24),
      marginHorizontal: 8,
      marginVertical: 8,
      borderRadius: 15,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'grey',
      overflow: 'hidden',
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.23,
      shadowRadius: 2.62,
      elevation: 4,

    },
    optionView: {
      width: '100%',
      marginVertical: 9,
      paddingHorizontal: 12,
      flexDirection: 'row',
    },
    iconView: {
      flex: 0.2,
      justifyContent: 'center',
      alignItems: 'center',
    },
    textView: {
      flex: 0.8,
      justifyContent: 'center',
      alignItems: 'flex-start',
    },
    textLabel: {
      fontSize: 16,
      color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
    },
    photoTitleLabel: {
      fontWeight: '500',
      fontSize: 17,
      letterSpacing: 3,
      textAlign: "center",
      color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
    },
    logoutView: {
      // width: '100%',
      // marginTop: 60,
      // marginBottom: 50,
      // height: 40,
      // // padding: 10,
      // // borderRadius: 10,
      // // borderWidth: 1,
      // // borderColor: DynamicAppStyles.colorSet[colorScheme].inputBgColor,
      // justifyContent: 'center',
      // alignItems: 'center',
      // ...DynamicAppStyles.shadows.buttonShadow 
      backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeForegroundColor,
      height: 40,
      alignItems: "center",
      justifyContent: "center",
      marginTop: 60,
      marginBottom: 50,
      marginTop: 20,
      width: "100%",
      ...DynamicAppStyles.shadows.buttonShadow
    },
    inactiveDot: {
      backgroundColor: DynamicAppStyles.colorSet[colorScheme].grey6,
      width: 8,
      height: 8,
      borderRadius: 4,
      marginLeft: 3,
      marginRight: 3,
      marginTop: 3,
      marginBottom: 3,
    },
    circleButton: {
      height: 60,
      width: 60,
      borderRadius: 60,
      backgroundColor: "#595959",
      marginHorizontal: 30
    }
  });
};

export default dynamicStyles;
