import React, { useRef, useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  StatusBar,
  SafeAreaView,
  Linking,
  Alert,
  Modal,
  TouchableWithoutFeedback,
  Keyboard, 
  ImageBackground,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Swiper from 'react-native-swiper';
import { connect } from 'react-redux';
import { firebase } from '../../Core/firebase/config';
import { firebaseStorage } from '../../Core/firebase/storage';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet';
import FastImage from 'react-native-fast-image';
import ActivityModal from '../../components/ActivityModal';
import DynamicAppStyles from '../../DynamicAppStyles';
import DatingConfig from '../../DatingConfig';
import { IMLocalized } from '../../Core/localization/IMLocalization';
import { TNTouchableIcon } from '../../Core/truly-native';
import authManager from '../../Core/onboarding/utils/authManager';
import { logout } from '../../Core/onboarding/redux/auth';
import { setUserData } from '../../Core/onboarding/redux/auth';
import { TNProfilePictureSelector } from '../../Core/truly-native';
import dynamicStyles from './styles';
import { useColorScheme } from 'react-native-appearance';
import { useIap } from '../../Core/inAppPurchase/context';
import auth from '@react-native-firebase/auth';
import CardDetailsView from '../../components/swipe/CardDetailsView/CardDetailsView';
import {default as Icon2} from 'react-native-vector-icons/MaterialCommunityIcons';
import IMEditProfileScreen from './../../Core/profile/ui/IMEditProfileScreen/IMEditProfileScreen';
import IMUserSettingsScreen from './../../Core/profile/ui/IMUserSettingsScreen/IMUserSettingsScreen';
import Interests from "./../../Core/profile/interests/interests";
import TrafficLights from "./../../Core/profile/trafficlights/trafficlights";
import Button from 'react-native-button';
import {
  getPurchaseHistory,
  validateReceiptIos
} from 'react-native-iap';
import { setIsPlanActive } from '../../Core/inAppPurchase/redux';

const MyProfileScreen = (props) => {
  const [loading, setLoading] = useState(false);
  const [myphotos, setMyphotos] = useState([]);
  const { setSubscriptionVisible } = useIap();
  const photoDialogActionSheetRef = useRef(null);
  const photoUploadDialogActionSheetRef = useRef(null);
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.auth.user);
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);
  const [modalVisible, setModalVisible] = useState(false);
  let currentTheme = DynamicAppStyles.navThemeConstants[colorScheme]; 

  const [showAccountDetails, setShowAccountDetails] = useState(false);
  const [showInterests, setShowInterests] = useState(false);
  const [showTrafficLights, setShowTrafficLights] = useState(false);
  const [showAccountSettings, setShowAccountSettings] = useState(false);

  const [showDeleteAccount, setShowDeleteAccount] = useState(false);

  const [password, setPassword] = useState("");
  const [deletedReason, setDeletedReason] = useState("");

  var selectedItemIndex = -1;

  const userRef = firebase.firestore().collection('main_data')
    .doc(currentUser.cityId)
    .collection('users')
    .doc(currentUser.id);
  
  const updatePhotos = (photos) => {
    let myUpdatePhotos = [];
    let pphotos = photos ? [...photos] : [];
    let temp = [];

    pphotos.push({ add: true });
    pphotos.map((item, index) => {
      temp.push(item);

      if (index % 6 == 5) {
        myUpdatePhotos.push(temp);
        temp = [];
      } else if (item && item.add) {
        myUpdatePhotos.push(temp);
        temp = [];
      }
    });

    setMyphotos(myUpdatePhotos);
    selectedItemIndex = -1;
  };

  useEffect(() => {
    if (currentUser) {
      updatePhotos(currentUser.photos);
    }
    StatusBar.setHidden(false);
  }, []);

  const detail = () => {
    props.navigation.navigate('AccountDetails', {
      appStyles: DynamicAppStyles,
      form: DatingConfig.editProfileFields,
      screenTitle: IMLocalized('Edit Profile'),
    });
  };

  const onUpgradeAccount = () => {
    setSubscriptionVisible(true);
  };

  const setting = () => {
    props.navigation.navigate('Settings', {
      userId: currentUser.id,
      appStyles: DynamicAppStyles,
      form: DatingConfig.userSettingsFields,
      screenTitle: IMLocalized('Settings'),
    });
  };

  const handleDeleteAccount = () => {
    Alert.alert(
      "Are you sure?",
      "Once you delete your account, all your data will be lost!",
      [
        { text: "Delete Account", onPress: () => { 
            setShowDeleteAccount(true);
          } 
        },
        {
          text: "Need Help?",
          onPress: () => Linking.openURL('mailto:contact@uni-dating.com')
        },
        {
          text: "Cancel",
          style: "cancel"
        },
      ],
      { cancelable: false }
    );
  }

  const contact = () => {
    Linking.openURL('mailto:contact@uni-dating.com');
    // props.navigation.navigate('ContactUs', {
    //   appStyles: DynamicAppStyles,
    //   form: DatingConfig.contactUsFields,
    //   screenTitle: IMLocalized('Contact us'),
    // });
  };

  const restorePurchases = () => {
    setLoading(true);

    getPurchaseHistory().then(purchases => {
      if (purchases == undefined) {
        setLoading(false);
        return;
      }
      // console.log(purchases);
      validateReceiptIos({
        'receipt-data': purchases[purchases.length - 1].transactionReceipt,
        'password': DatingConfig.IAP_SHARED_SECRET
      }).then(receipt => {
        const renewalHistory = receipt.latest_receipt_info;

        if (!renewalHistory) {
          setLoading(false);
          return;
        }
        const expiration = renewalHistory[renewalHistory.length - 1].expires_date_ms;
        
        if (expiration > Date.now()) {
          // Subscription still valid
          dispatch(setIsPlanActive(true))
        } else {
          // Subscription expired
        }

        setLoading(false);
      }).catch(error => {
        setLoading(false);
      });
    });
    setLoading(false);
  };

  const viewWebsite = () => {
    Linking.openURL("https://www.uni-dating.com");
  }

  const onLogout = () => {
    Alert.alert(
      "Logout",
      "Are you sure?",
      [
        { text: "Logout", onPress: () => { 
            // console.log(currentUser);
            // authManager.logout(currentUser);
            firebase.auth().signOut().then(() => {
              // dispatch(logout()); 
              props.navigation.navigate('LoadScreen', {
                appStyles: DynamicAppStyles,
                appConfig: DatingConfig,
              });
            }).catch((error) => {
              console.log(error);
            });
          },
          style: "destructive" 
        },
        {
          text: "Cancel",
          style: "cancel"
        },
      ],
      { cancelable: false }
    );


    
  };

  const onSelectAddPhoto = () => {
    photoUploadDialogActionSheetRef.current.show();
  };

  const onPhotoUploadDialogDone = (index) => {
    if (index == 0) {
      onLaunchCamera();
    }

    if (index == 1) {
      onOpenPhotos();
    }
  };

  const updateUserPhotos = (uri) => {
    const { photos } = currentUser;
    let pphotos = photos ? photos : [];

    pphotos.push(uri);

    const data = {
      photos: pphotos,
    };

    updateUserInfo(data);
    updatePhotos(pphotos);
  };

  const onLaunchCamera = () => {
    ImagePicker.openCamera({
      cropping: false,
    }).then((image) => {
      startUpload(image, updateUserPhotos);
    });
  };

  const onOpenPhotos = () => {
    ImagePicker.openPicker({
      cropping: false,
    })
      .then((image) => {
        startUpload(image, updateUserPhotos);
      })
      .catch((error) => {
        console.log(error);
        setTimeout(() => {
          alert(
            IMLocalized(
              'An errord occurred while loading image. Please try again.',
            ),
          );
        }, 1000);
      });
  };

  const startUpload = (source, updateUserData) => {
    setLoading(true);

    if (!source) {
      updateUserData(null);
      setLoading(false);
      return;
    }

    firebaseStorage
      .uploadFile(source, currentUser.id)
      .then(({ downloadURL }) => {
        if (downloadURL) {
          console.log(downloadURL);
          updateUserData(downloadURL);
          setLoading(false);
        } else {
          // an error occurred
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
        console.log(error);
      });
  };

  const handleDelete = () => {

    if (password === "") {
      console.log("Password null")
      return;
    }

    setLoading(true);


    firebase
      .auth()
      .signInWithEmailAndPassword(currentUser.email, password)
      .then((response) => { 
        userRef.delete().then(() => {

          if (deletedReason !== "") {
            firebase.firestore()
              .collection('deleted_reasons')
              .add({
                name: currentUser.firstName + " " + currentUser.lastName,
                email: currentUser.email,
                university: currentUser.universityName,
                reasonForDelete: deletedReason
              });
          }

          firebase.auth().currentUser.delete().then(() => {
            setLoading(false);
            setShowDeleteAccount(false);
            props.navigation.navigate("LoginStack");
          });
        });
      }).catch(() => {
        setLoading(false);
      });
  }
 
  const renderDeleteAccount = () => {

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style={{flex: 1, alignItems: "center", justifyContent: "center"}}>
            <Text style={styles.deleteModalText}>Please re-enter your password to delete your account</Text>
            <TextInput
              style={styles.DeleteReason}
              multiline={true}
              placeholderTextColor="#aaaaaa"
              placeholder={"Why are you deleting your account?"}
              onChangeText={(text) => setDeletedReason(text)}
              value={deletedReason}
              underlineColorAndroid="transparent"
              autoCapitalize="none"
            />
            
            <TextInput
              style={styles.InputContainer}
              placeholderTextColor="#aaaaaa"
              secureTextEntry
              placeholder={IMLocalized('Password')}
              onChangeText={(text) => setPassword(text)}
              value={password}
              underlineColorAndroid="transparent"
              autoCapitalize="none"
            />

            <Button
              containerStyle={styles.loginContainer}
              style={styles.loginText}
              onPress={() => handleDelete()}>
              Delete Account
            </Button>

            <TouchableOpacity
              style={{justifyContent: "center", marginTop: 80}}
              onPress={() => setShowDeleteAccount(false)}
            >
              <Text>Cancel</Text>
            </TouchableOpacity>
        </View>
      </TouchableWithoutFeedback>

    );
  }

  const updateUserInfo = (data) => {
    const tempUser = currentUser;
    // optimistically update the UI

    dispatch(setUserData({ user: { ...currentUser, ...data } }));
    userRef
      .update(data)
      .then(() => {
        setLoading(false);
      })
      .catch((error) => {
        const { message } = error;
        setLoading(false);
        dispatch(setUserData({ user: { ...tempUser } }));
        console.log('upload error', error);
      });
  };

  const updateProfilePictureURL = (file) => {
    startUpload(file, (downloadURL) => updateUserInfo({ profilePictureURL: downloadURL }));


  };

  const onSelectDelPhoto = (index) => {
    selectedItemIndex = index;
    photoDialogActionSheetRef.current.show();
  };

  const hideModalFunc = (b) => {
    console.log("Hide modal");
    setShowAccountDetails(b);
  };

  const onPhotoDialogDone = (actionSheetActionIndex) => {
    const { photos } = currentUser;

    if (selectedItemIndex == -1 || selectedItemIndex >= photos.length) {
      return;
    }

    if (actionSheetActionIndex == 0) {
      if (photos) {
        photos.splice(selectedItemIndex, 1);
      }

      updateUserInfo({ photos });
      updatePhotos(photos);
    }

    if (actionSheetActionIndex == 2) {
      const photoToUpdate = photos[selectedItemIndex];
      updateUserInfo({ profilePictureURL: photoToUpdate });
    }
  };

  const { firstName, lastName, profilePictureURL } = currentUser;
  const userLastName = currentUser && lastName ? lastName : ' ';
  const userfirstName = currentUser && firstName ? firstName : ' ';
  const userSchool = currentUser && currentUser.school ? currentUser.school : ' ';
  const userDistance = currentUser && currentUser.distance ? currentUser.distance : ' ';
  const userBio = currentUser.profile && currentUser.profile.about ? currentUser.profile.about : ' ';
  const userYear = currentUser.profile && currentUser.profile.year ? currentUser.profile.year : ' ';
  const userCourse = currentUser.profile && currentUser.profile.studying ? currentUser.profile.studying : ' ';
  const userOnCampus = currentUser.profile && currentUser.profile.on_campus ? currentUser.profile.on_campus : ' ';
  const userLookingFor = currentUser.profile && currentUser.settings.want ? currentUser.settings.want : ' ';
  const userPhotos = currentUser.profile && currentUser.photos ? currentUser.photos : null;
  return (
    <View style={styles.MainContainer}>
      <SafeAreaView style={styles.safeAreaContainer}>
        <Modal visible={showDeleteAccount}>
          {renderDeleteAccount()}
        </Modal>

        <Modal visible={modalVisible} animationType={"slide"}>
          {currentUser == undefined || currentUser == null ?
              <CardDetailsView/>
            :
            <CardDetailsView
              profilePictureURL={currentUser.profilePictureURL}
              firstName={currentUser.firstName || ""}
              lastName={currentUser.lastName}
              school={userSchool}
              distance={userDistance}
              uniName={currentUser?.universityName}
              bio={userBio}
              year={userYear}
              course={userCourse}
              oncampus={userOnCampus}
              lookingfor = {userLookingFor}
              instagramPhotos={userPhotos}
              setShowMode={setModalVisible}
              trafficLight={currentUser?.settings?.traffic_light}
              interests={currentUser.interests}
            />
          }
        </Modal>

        <Modal visible={showTrafficLights} animationType={"slide"}>
            <TrafficLights
              appStyles={DynamicAppStyles}
              user={currentUser}
              close={setShowTrafficLights}
            />
        </Modal>

        <Modal visible={showInterests} animationType={"slide"}>
            <Interests
              appStyles={DynamicAppStyles}
              user={currentUser}
              close={setShowInterests}
            />
        </Modal>

        <Modal
          animationType="slide"
          visible={showAccountDetails}>      
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
              <View style={styles.holder}>
                <View style={{marginTop: 60, height: "60%", width: "100%", marginBottom: 90}}>
                  <IMEditProfileScreen 
                        hideModal={() => setShowAccountDetails(false)}
                        appStyles={DynamicAppStyles}
                        form={DatingConfig.editProfileFields}
                        screenTitle={IMLocalized('Edit Profile')}/>
                </View>              
              </View>
            </TouchableWithoutFeedback>
        </Modal>

        <Modal
          animationType="slide"
          visible={showAccountSettings}>      
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
              <View style={styles.holder}>
                <View style={{marginTop: 60, height: "60%", width: "100%", marginBottom: 90}}>
                  < IMUserSettingsScreen 
                        userId={currentUser.id}
                        hideModal={() => setShowAccountSettings(false)}
                        appStyles={DynamicAppStyles}
                        form={DatingConfig.userSettingsFields}
                        screenTitle={IMLocalized('Settings')}/>
                </View>              
              </View>
            </TouchableWithoutFeedback>
        </Modal>


        <View style={styles.MainContainer}>
          <ScrollView style={styles.body}>
            <ImageBackground 
              source={DynamicAppStyles.iconSet.gradient}
              style={styles.profilePictureContainer}
              resizeMode={"contain"}>

                <TNProfilePictureSelector
                  setProfilePictureFile={updateProfilePictureURL}
                  appStyles={DynamicAppStyles}
                  profilePictureURL={profilePictureURL}
                />

            </ImageBackground>
            <TouchableOpacity style={styles.nameView} onPress={() => setModalVisible()}>
              <Text style={styles.name}>
                {userfirstName + ' ' + userLastName}
              </Text>
            </TouchableOpacity>
            <View
              style={[
                styles.myphotosView,
                myphotos[0] && myphotos[0].length <= 3
                  ? { height: 158 }
                  : { height: 268 },
              ]}>
              <View style={styles.itemView}>
                <Text style={styles.photoTitleLabel}>
                  My Photos
                </Text>
              </View>
              <Swiper
                removeClippedSubviews={false}
                showsButtons={false}
                loop={false}
                paginationStyle={{ top: -230, left: null, right: 0 }}
                dot={<View style={styles.inactiveDot} />}
                activeDot={
                  <View
                    style={{
                      backgroundColor: '#db6470',
                      width: 8,
                      height: 8,
                      borderRadius: 4,
                      marginLeft: 3,
                      marginRight: 3,
                      marginTop: 3,
                      marginBottom: 3,
                    }}
                  />
                }>
                {myphotos.map((photos, i) => (
                  <View key={'photos' + i} style={styles.slide}> 
                    <View style={styles.slideActivity}>
                      <FlatList
                        horizontal={false}
                        numColumns={3}
                        data={photos}
                        scrollEnabled={false}
                        renderItem={({ item, index }) =>
                          item.add ? (
                            <TouchableOpacity
                              key={'item' + index}
                              style={[
                                styles.myphotosItemView,
                                {
                                  // borderWidth: 1,
                                  // borderColor: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
                                  backgroundColor:
                                    DynamicAppStyles.colorSet[colorScheme]
                                      .mainThemeForegroundColor,
                                },
                              ]}
                              onPress={onSelectAddPhoto}>
                              <Icon
                                style={styles.icon}
                                name="ios-camera"
                                size={40}
                                color={
                                  "#ef8484"
                                }
                              />
                            </TouchableOpacity>
                          ) : (
                            <TouchableOpacity
                              key={'item' + index}
                              style={styles.myphotosItemView}
                              onPress={() => onSelectDelPhoto(i * 6 + index)}>
                              <FastImage
                                style={{ width: '100%', height: '100%' }}
                                source={{ uri: item }}
                              />
                            </TouchableOpacity>
                          )
                        }
                      />
                    </View>
                  </View>
                ))}
              </Swiper>
            </View>

            <TouchableOpacity style={styles.optionView} onPress={() => onUpgradeAccount()}> 
              <View style={styles.iconView}>
                <Icon2 size={25} color={"#ef8484"} name="check-decagram"/>
              </View>
              <View style={styles.textView}>
                <Text style={styles.textLabel}>
                  Upgrade
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.optionView} onPress={() => setShowAccountDetails(true)}> 
              <View style={styles.iconView}>
                <Icon2 size={25} color={"#ef8484"} name="account-circle"/>
              </View>
              <View style={styles.textView}>
                <Text style={styles.textLabel}>
                  {IMLocalized('Account Details')}
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.optionView} onPress={() => setShowTrafficLights(true)}> 
              <View style={styles.iconView}>
                <Icon2 size={25} color={"#ef8484"} name="traffic-light"/>
              </View>
              <View style={styles.textView}>
                <Text style={styles.textLabel}>
                  Your Light
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.optionView} onPress={() => setShowInterests(true)}> 
              <View style={styles.iconView}>
                <Icon2 size={25} color={"#ef8484"} name="star-outline"/>
              </View>
              <View style={styles.textView}>
                <Text style={styles.textLabel}>
                  {IMLocalized('Interests')}
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.optionView} onPress={() => setShowAccountSettings(true)}>
              <View style={styles.iconView}>
                <Icon2 size={25} color={"#ef8484"} name="cog"/>
              </View>
              <View style={styles.textView}>
                <Text style={styles.textLabel}>{IMLocalized('Settings')}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.optionView} onPress={contact}>
              <View style={styles.iconView}>
                {/* <Image
                  style={{
                    width: 25,
                    height: 25,
                    tintColor: '#88e398',
                    resizeMode: 'cover',
                  }}
                  source={DynamicAppStyles.iconSet.callIcon}
                /> */}
                <Icon2 size={25} color={"#ef8484"} name="email"/>
              </View>
              <View style={styles.textView}>
                <Text style={styles.textLabel}>
                  {IMLocalized('Contact Us')}
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.optionView} onPress={restorePurchases}>
              <View style={styles.iconView}>
                <Icon2 size={25} color={"#ef8484"} name="receipt"/>
              </View>
              <View style={styles.textView}>
                <Text style={styles.textLabel}>
                  {IMLocalized('Restore Purchases')}
                </Text>
              </View>
            </TouchableOpacity>

            {/* <TouchableOpacity style={styles.optionView} onPress={viewWebsite}>
              <View style={styles.iconView}>
                <Image
                  style={{
                    width: 25,
                    height: 25,
                    tintColor: '#5e9af2',
                    resizeMode: 'cover',
                  }}
                  source={DynamicAppStyles.iconSet.starFilled}
                />
              </View>
              <View style={styles.textView}>
                <Text style={styles.textLabel}>
                  {IMLocalized('Visit Our Website')}
                </Text>
              </View>
            </TouchableOpacity> */}
            <TouchableOpacity style={styles.logoutView} onPress={onLogout}>
              <Text style={styles.textLabel}>{IMLocalized('Logout')}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{alignItems: "center"}} onPress={() => handleDeleteAccount()}>
              <Text style={{color: "#7d7d7d", opacity: 0.5}}>{IMLocalized('Delete Account')}</Text>
            </TouchableOpacity>
          </ScrollView>
          <ActionSheet
            ref={photoDialogActionSheetRef}
            title={IMLocalized('Photo Dialog')}
            options={[
              IMLocalized('Remove Photo'),
              IMLocalized('Cancel'),
              IMLocalized('Make Profile Picture'),
            ]}
            cancelButtonIndex={1}
            destructiveButtonIndex={0}
            onPress={onPhotoDialogDone}
          />
          <ActionSheet
            ref={photoUploadDialogActionSheetRef}
            title={IMLocalized('Photo Upload')}
            options={[
              IMLocalized('Launch Camera'),
              IMLocalized('Open Photo Gallery'),
              IMLocalized('Cancel'),
            ]}
            cancelButtonIndex={2}
            onPress={onPhotoUploadDialogDone}
          />
          <ActivityModal
            loading={loading}
            title={IMLocalized('Please wait')}
            size={'large'}
            activityColor={'white'}
            titleColor={'white'}
            activityWrapperStyle={{
              backgroundColor: '#404040',
            }}
          />
        </View>
      </SafeAreaView>
    </View>
  );
};

const mapStateToProps = (state) => ({
  user: state.auth.user,
});

export default connect(mapStateToProps)(MyProfileScreen);
