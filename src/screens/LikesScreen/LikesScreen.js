import React, { useCallback, useRef, useState, useEffect, useContext } from 'react';
import { useSelector, useDispatch, ReactReduxContext, connect } from 'react-redux';
import {
  View,
  Text,
  FlatList,
  ImageBackground,
  TouchableOpacity,
  ActivityIndicator,
  Modal,
  Image,
  ScrollView,
  Linking,
} from 'react-native';
import dynamicStyles from './styles';
import { useColorScheme } from 'react-native-appearance';
import CardDetailsView from '../../components/swipe/CardDetailsView/CardDetailsView';
import { useIap } from '../../Core/inAppPurchase/context';
import SwipeTracker from '../../firebase/tracker';
import { TNEmptyStateView } from '../../Core/truly-native';
import { useNavigation } from '@react-navigation/native';

const LikesScreen = (props) => {
  const currentUser = useSelector((state) => state.auth.user);
  const incomingSwipes = useSelector(state => state.dating.incomingSwipes);
  // const isPlanActive = useSelector((state) => state.inAppPurchase.isPlanActive);
  const { setSubscriptionVisible } = useIap();

  const { store } = useContext(ReactReduxContext);
  const swipeTracker = useRef(new SwipeTracker(store, currentUser.id, currentUser.cityId));

  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);

  const [showModal, setShowModal] = useState(false);
  const [pressedUser, setPressedUser] = useState({});

  const [likes, setLikes] = useState(0);
  const [isProcessing, setIsProcessing] = useState(false);

  const navigation = useNavigation();

  const onSwipedLeft = () => {
    handleSwipe('dislike');
    setShowModal(false);
  };

  const onSwipedRight = () => {
    handleSwipe('like');
    setShowModal(false);
  };

  const handleSwipe = (type) => {
    onSwipe(type, pressedUser);
  };

  const onEmptyStatePress = () => {
    navigation.navigate('Swipe');
  }

  const emptyStateConfig = {
    title: 'No Likes Yet',
    description: 
      "Start swiping on the people you're interested in. When someone likes you, they'll show up here.",
    buttonName: 'Start swiping',
    onPress: onEmptyStatePress
  };

  const onSwipe = (type, swipeItem) => {

    const matesOrDates = currentUser.settings.traffic_light == "green" ? "dates" : "mates";
    if (swipeItem) {
      swipeTracker.current.addSwipe(currentUser, swipeItem, type, matesOrDates, (response) => {});
    }
    setShowModal(false);
  };
  
  const likePressed = (profile) => {
    if (!props.isPlanActive) {
      setSubscriptionVisible(true);
      return;
    }

    setPressedUser(profile);
    setShowModal(true);
  };

  const renderItem = ({ item }) => (
    <View style={styles.likeProfile}>
      <TouchableOpacity
        onPress={() => likePressed(item)}
        style={{height:"100%", width: "100%"}}
      >
        <ImageBackground
          source={{uri: item.profilePictureURL}}
          style={{height: "100%", width: "100%", justifyContent: "space-between"}}
          imageStyle={{borderRadius: 10}}
          resizeMode="cover"
          blurRadius={props.isPlanActive ? 0 : 50}
        >

        </ImageBackground>
      </TouchableOpacity>
    </View>
  );

  const getLikesText = () => {
    const noLikes = incomingSwipes?.length;
    let output = "";
    if (noLikes == 0) {
      output = "";
    } else if (noLikes == 1) {
      output = "1 Like";
    } else if (noLikes > 99) {
      output = "99+ Likes";
    } else {
      output = noLikes + " Likes";
    }

    return output;

  }

  return (
      <ScrollView style={styles.dealsFeed}>
        {/* <View style={styles.circle}>

        </View> */}

        <Modal animationType="slide" visible={showModal}>
            <CardDetailsView  
                profilePictureURL={pressedUser?.profilePictureURL}
                firstName={pressedUser?.firstName}
                lastName={pressedUser?.lastName}
                school={pressedUser?.school}
                distance={pressedUser?.distance}
                trafficLight={pressedUser?.settings?.traffic_light}
                interests={pressedUser?.interests}
                instagramPhotos={pressedUser?.photos}
                uniName={pressedUser?.universityName}
                bio={pressedUser?.profile?.about}
                year={pressedUser?.profile?.year} 
                course={pressedUser?.profile?.studying}
                oncampus={pressedUser?.profile?.on_campus}
                lookingfor = {pressedUser?.settings?.want}
                setShowMode={setShowModal}
                reportUser = {pressedUser?.id}

                onSwipeRight={onSwipedRight}
                onSwipeLeft={onSwipedLeft}
                bottomTabBar = {true}
            />
        </Modal> 

          {!incomingSwipes && <ActivityIndicator style={{alignSelf:"center", marginTop: 100}} size={"large"}/>}

          {incomingSwipes && <Text style={styles.likesText}>{getLikesText()}</Text>}
          {!props.isPlanActive && incomingSwipes && incomingSwipes.length > 0 && <Text style={styles.upgradeText}>Upgrade to Uni Lights PRO to see who already likes you!</Text>}

          {!props.isPlanActive && incomingSwipes && incomingSwipes.length > 0 && <TouchableOpacity
            style={styles.upgradeButton}
            onPress={() => setSubscriptionVisible(true)}
          >
            <Text style={styles.upgradeButtonText}>Upgrade Now</Text>
          </TouchableOpacity>}

          {incomingSwipes && incomingSwipes?.length <= 0 && <TNEmptyStateView
              emptyStateConfig={emptyStateConfig}
              appStyles={props.route.params.appStyles}
            />}

          {incomingSwipes && <FlatList
            nestedScrollEnabled
            numColumns={2}
            style={{flex: 1}}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item, index) => item.id.toString()}
            renderItem={renderItem}
            data={incomingSwipes}
          />}
        
      </ScrollView>
  );

}

const mapStateToProps = ({ inAppPurchase }) => {
  return {
    isPlanActive: inAppPurchase.isPlanActive
  };
}




export default connect(mapStateToProps)(LikesScreen);
