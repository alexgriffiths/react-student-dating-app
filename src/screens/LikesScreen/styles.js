import { StyleSheet, Platform, Dimensions } from 'react-native';
import DynamicAppStyles from '../../DynamicAppStyles';
import { DEVICE_HEIGHT } from '../../helpers/statics';

const width = Dimensions.get('window').width;

const dynamicStyles = (colorScheme) => {
  return StyleSheet.create({
    dealsFeed: {
        flex: 1,
        backgroundColor:
          DynamicAppStyles.colorSet[colorScheme].mainThemeForegroundColor,
    },
    likesText: {
        marginVertical: 8,
        fontSize: 22,
        marginHorizontal: 10,
        fontWeight: '500',
        textAlign: "center",
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
    },
    upgradeText: {
        fontSize: 18,
        // fontWeight: '500',
        marginHorizontal: 10,
        opacity: 0.7,
        textAlign: "center",
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
        marginBottom: 8,
    },
    likeProfile: {
        height: 240,
        margin: 8,
        maxWidth: "46%",
        borderColor: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center", 
        flex: 1,
        // backgroundColor: "blue"
    },
    upgradeButton: {
        // justifyContent: "center",
        // alignItems: "center",
        // height: 40,
        // width: "40%",
        // backgroundColor: "#ef8484",
        // borderRadius: 12,
        // alignSelf: "center",
        // marginBottom: 10,
        // marginTop: 6,
        backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeForegroundColor,
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 60,
        marginBottom: 50,
        marginTop: 20,
        width: "100%",
        ...DynamicAppStyles.shadows.buttonShadow
    },
    upgradeButtonText: {
        color: DynamicAppStyles.colorSet[colorScheme].mainTextColor,
        fontSize: 16,
    },
    circle: {
        position: "absolute",
        top: -width*1.25,
        alignSelf: "center",
        width: width*2,
        height: width*2,
        borderRadius: width,
        backgroundColor: "#ef8484"
    },
  });
};

export default dynamicStyles;
