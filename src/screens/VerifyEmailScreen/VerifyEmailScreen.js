import React, { useCallback, useRef, useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  View,
  Text,
  TouchableOpacity,
  Button,
  Image,
  Linking
} from 'react-native';
import dynamicStyles from './styles.js';
import { useColorScheme } from 'react-native-appearance';
import DynamicAppStyles from '../../DynamicAppStyles';
import { IMLocalized } from '../../Core/localization/IMLocalization';
import TNActivityIndicator from '../../Core/truly-native/TNActivityIndicator';
import { firebase } from '../../Core/firebase/config';

const VerifyEmailScreen = (props) => {
  const appStyles = props.route.params.appStyles;
  const appConfig = props.route.params.appConfig;
  const authManager = props.route.params.authManager;
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(appStyles, colorScheme);

  const currentUsez = useSelector((state) => state.auth.user);

  const [loading, setLoading] = useState(false);

  const handlePress = () => {
    setLoading(true);
    firebase.auth().currentUser.reload().then(() => {
      firebase.firestore().collection('main_data')
        .doc(currentUsez.cityId)
        .collection('users')
        .doc(currentUsez.userID)
        .get()
        .then((userGot) => {
          const userManuallyVerified = userGot.data().manuallyVerified;
          const user = firebase.auth().currentUser;

          const verified = user.emailVerified;

          setLoading(false);
          if (verified || userManuallyVerified) {
            props.navigation.navigate('LoginStack', {
              screen: 'UserSetup', 
              params: {
                  appStyles,
                  appConfig,
                  authManager,
                  user: props.route.params.user
              },
            });
          }
        })
    })
  }


  return (
    <View style={styles.container}>
        <View style={{ flex: 1, width: '100%' }}>
            <TouchableOpacity
            style={{ alignSelf: 'flex-start' }}
            onPress={() => props.navigation.goBack()}>
                <Image
                    style={appStyles.styleSet.backArrowStyle}
                    source={appStyles.iconSet.backArrow}
                />
            </TouchableOpacity>
            <Text style={styles.title}>{IMLocalized('Verify Your Email')}</Text>
            <Text style={styles.bodyText}>Thanks for signing up! You should have received an email with a verification link. Sometimes this can end up in your spam folder, if you have any problems with verifying your email please contact us :)</Text>

            <TouchableOpacity 
                style={styles.refresh}
                onPress={() => handlePress()}
                >
                <Text style={styles.refreshText}>Refresh</Text>
            </TouchableOpacity>

            <TouchableOpacity 
                style={{alignSelf: "center", marginTop: 20, top: 140}}
                onPress={() => Linking.openURL('mailto:contact@uni-dating.com')}
                >
                <Text style={styles.contactUs}>
                Contact Us
                </Text>
            </TouchableOpacity>

            {/* <Button
            containerStyle={styles.loginContainer}
            style={styles.loginText}
            onPress={() => onPressLogin()}>
            {IMLocalized('Log In')}
            </Button> */}
            {loading && <TNActivityIndicator appStyles={appStyles} />}
        </View>
        
    </View>
  );
};

export default VerifyEmailScreen;
