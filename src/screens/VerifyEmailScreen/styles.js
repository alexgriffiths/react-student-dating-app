import { StyleSheet, Platform, Dimensions } from 'react-native';
import DynamicAppStyles from '../../DynamicAppStyles';
import { DEVICE_HEIGHT } from '../../helpers/statics';

const width = Dimensions.get('window').width;

const dynamicStyles = (appStyles, colorScheme) => {
  return StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
      },
      orTextStyle: {
        color: appStyles.colorSet[colorScheme].mainTextColor,
        marginTop: 40, 
        marginBottom: 10,
        alignSelf: 'center',
      },
      title: {
        fontSize: 30,
        fontWeight: appStyles.boldness.bigTitle,
        color: appStyles.colorSet[colorScheme].mainTextColor,
        marginVertical: 30,
        marginTop: 100,
        // marginBottom: 20,
        alignSelf: 'stretch',
        textAlign: 'left',
        // marginLeft: 30,
        alignSelf: "center",
      },
      loginContainer: {
        width: '70%',
        backgroundColor: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
        borderRadius: 25,
        padding: 10,
        marginTop: 40,
        alignSelf: 'center',
      },
      loginText: {
        color: '#ffffff',
      },
      placeholder: {
        color: 'red',
      },
      bodyText: {
        color: appStyles.colorSet[colorScheme].mainTextColor,
        marginTop: 10,
        marginHorizontal: 10,
        marginBottom: 10,
        alignSelf: 'center',
        textAlign: "center",
        fontSize: 18,
      },
      contactUs: {
        fontSize: 16,
        color: appStyles.colorSet[colorScheme].mainTextColor,
        marginTop: 10,
        marginBottom: 30,
        top: "-80%",
        textAlign: 'center',
      },
      refresh: {
        //   width: "30%",
          paddingHorizontal: 40,
          height: 50,
          borderRadius: 12,
          backgroundColor: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
          justifyContent: "center",
          alignItems: "center",
          alignSelf: "center",
          marginTop: 80,
          ...appStyles.shadows.buttonShadow
      },
      refreshText: {
          fontSize: 20,
          fontWeight: "500",
          color: appStyles.colorSet[colorScheme].mainTextColor
      }
  });
};

export default dynamicStyles;
