import { decode, encode } from 'base-64';
import './timerConfig';
global.addEventListener = (x) => x;
if (!global.btoa) {
  global.btoa = encode;
}

if (!global.atob) {
  global.atob = decode;
}

import * as firebase from 'firebase';
import '@firebase/auth';
import '@firebase/firestore';

let firebaseConfig = {
    apiKey: "",
    authDomain: "",
    projectId: "",
    storageBucket: "",
    messagingSenderId: "",
    appId: "",
    measurementId: ""
 };
 // if(location.hostname === 'localhost') {
 // 	config = {
 // 		databaseUR: "http://localhost:4000?ns=uni-dating-main"
 // 	}
 // }
// config = {
// 	databaseUR: "http://localhost:4000?ns=uni-dating-main"
// };

if (!firebase.apps.length) firebase.initializeApp(firebaseConfig);

export { firebase };
