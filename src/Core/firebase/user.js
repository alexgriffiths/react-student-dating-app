import { firebase } from './config';

// export const usersRef = firebase.firestore().collection('test')
//   .doc('users')
//   .collection('users');


//firebase.firestore().collection('users');

export const getUserData = async (userId, cityId) => {
  try {
    const usersRef = firebase.firestore().collection('main_data')
      .doc(cityId)
      .collection('users')
      .doc(userID)

    const user = await usersRef.doc(userId).get();

    return { data: { ...user.data(), id: user.id }, success: true };
  } catch (error) {
    console.log(error);
    return {
      error: 'Oops! an error occurred. Please try again',
      success: false,
    };
  }
};

export const updateUserData = async (userId, cityId, userData) => {
  try {
    const userRef = firebase.firestore().collection('main_data')
      .doc(cityId)
      .collection('users')
      .doc(userId);

    await userRef.update({
      ...userData,
    });

    return { success: true };
  } catch (error) {
    return { error, success: false };
  }
};

export const subscribeUsers = (callback, cityId) => {
  const usersRef = firebase.firestore().collection('main_data')
    .doc(cityId)
    .collection('users'); 
  return usersRef.onSnapshot((querySnapshot) => {
    const users = [];
    querySnapshot.forEach((doc) => {
      users.push(doc.data());
    });
    return callback(users);
  });
};

export const subscribeCurrentUser = (userId, callback, cityId) => {
  const usersRef = firebase.firestore().collection('main_data')
    .doc(cityId)
    .collection('users');
  const ref = usersRef
    .where('id', '==', userId)
    .onSnapshot({ includeMetadataChanges: true }, (querySnapshot) => {
      const docs = querySnapshot.docs;
      if (docs.length > 0) {
        callback(docs[0].data());
      }
    });
  return ref;
};
