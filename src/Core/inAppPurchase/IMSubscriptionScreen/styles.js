import { StyleSheet, Dimensions } from 'react-native';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const tickContainerSize = 24;
const containerPaddingHorizontal = 20;

const dynamicStyles = (appStyles, colorScheme) => {
  return StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    carouselContainer: {
      flex: 1.4,
      paddingHorizontal: containerPaddingHorizontal,
    },
    carouselImageContainer: {
      flex: 1,
    },
    carouselImage: {
      height: '68%',
      width: '100%',
    },
    inactiveDot: {
      backgroundColor: 'rgba(0,0,0,.3)',
      width: 6,
      height: 6,
      borderRadius: 3,
      marginLeft: 3,
      marginRight: 3,
    },
    activeDot: {
      backgroundColor: "#ef8484",
      width: 6,
      height: 6,
      borderRadius: 3,
      marginLeft: 3,
      marginRight: 3,
    },
    subscriptionsContainer: {
      flex: 3,
      top: -20,
      alignItems: 'center',
      marginBottom: 20,
    },
    headerTitle: {
      color: appStyles.colorSet[colorScheme].mainTextColor,
      textAlign: 'center',
      fontSize: 26,
      paddingBottom: 3,
      // paddingHorizontal: containerPaddingHorizontal,
    },
    titleDescription: {
      color: appStyles.colorSet[colorScheme].mainTextColor,
      textAlign: 'center',
      fontSize: 13,
      lineHeight: 18,
      marginBottom: 10,
      // paddingHorizontal: containerPaddingHorizontal - 5,
    },
    titleDescriptionSmall: {
      color: appStyles.colorSet[colorScheme].mainTextColor,
      textAlign: 'center',
      fontSize: 8,
      lineHeight: 18,
      marginBottom: 10,
      paddingHorizontal: containerPaddingHorizontal - 5,
    },
    subscriptionPlansContainer: {
      flexDirection: "row",
      flex: 1,
      width: '100%',
      justifyContent: 'center',
      marginTop: 10,
      // marginHorizontal: 4,
      justifyContent: "space-evenly"
      // paddingHorizontal: containerPaddingHorizontal - 7,
    },
    subscriptionContainer: {
      minHeight: 180,
      flexDirection: 'column',
      alignItems: "center",
      flex: 0.3,
      // height: Math.floor(height * 0.11),
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
      borderRadius: Math.floor(height * 0.02),
      paddingVertical: 8,
      paddingHorizontal: 6,
      marginVertical: 10,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.23,
      shadowRadius: 2.62,

      elevation: 4,
    },
    subscriptionContainerBold: {
      minHeight: 180,
      flexDirection: 'column',
      alignItems: "center",
      flex: 0.3,
      // height: Math.floor(height * 0.11),
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
      borderRadius: Math.floor(height * 0.02),
      borderColor: "gold",
      borderWidth: 4,
      paddingVertical: 8,
      paddingHorizontal: 12,
      marginVertical: 10,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.65,
      shadowRadius: 2.62,

      elevation: 8,
    },
    selectContainer: {
      flex: 0.5,
      justifyContent: 'center',
      alignItems: 'center',
    },
    tickIconContainer: {
      width: tickContainerSize,
      height: tickContainerSize,
      borderRadius: Math.floor(tickContainerSize / 2),
      backgroundColor: '#f4f6fa',
      justifyContent: 'center',
      alignItems: 'center',
    },
    selectedSubscription: {
      backgroundColor: "#ef8484",
    },
    tick: {
      width: tickContainerSize - 10,
      height: tickContainerSize - 10,
      tintColor: '#fff',
    },
    labelView: {
      position: "absolute", 
      top: -12, 
      borderRadius: 8, 
      backgroundColor: "gold"
    },
    labelText: {
      textAlign: "center", 
      marginVertical: 4, 
      marginHorizontal: 6,
      color: "white"
    },
    rateContainer: {
      flex: 2,
      justifyContent: 'center',
    },
    rateText: {
      fontSize: 10,
      textAlign: "center",
      color: appStyles.colorSet[colorScheme].mainTextColor,
    },
    rateFullText: {
      fontSize: 20,
      fontWeight: "500",
      textAlign: "center",
      color: appStyles.colorSet[colorScheme].mainTextColor,
    },
    savings: {
      textAlign: "center",
      color: "gold"
    },
    monthText: {
      textAlign: "center",
      fontSize: 18,
      color: appStyles.colorSet[colorScheme].mainTextColor,
    },
    wtfText: {
      textAlign: "center",
      fontSize: 18,
      fontWeight: '400',
      color: appStyles.colorSet[colorScheme].mainTextColor,
    },
    bigNumber: {
      textAlign: "center",
      fontSize: 40,
      color: appStyles.colorSet[colorScheme].mainTextColor,
    },
    trialOptionContainer: {
      flex: 2,
      justifyContent: 'center',
    },
    trialContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: Math.floor(height * 0.92),
      marginHorizontal: 15,
      height: '65%',
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
    },
    trialText: {
      fontSize: 16,
      color: 'black',
    },
    bottomContainer: {
      flex: 2,
      justifyContent: 'center',
      alignItems: 'center',
    },
    bottomHeaderTitle: {
      color: appStyles.colorSet[colorScheme].mainTextColor,
      fontSize: 14,
      textAlign: 'center',
      fontWeight: '500',
    },
    bottomButtonContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: "#ef8484",
      width: '80%',
      height: '24%',
      borderRadius: Math.floor(height * 0.92),
      marginTop: 20,
      marginBottom: 40,
    },
    buttonTitle: {
      fontSize: 16,
      color: '#fff',
    },
    cancelTitle: {
      fontSize: 16,
      color: appStyles.colorSet[colorScheme].mainTextColor,
      padding: 15,
      marginBottom: 7,
    },
    indicator: {
      position: 'absolute',
      backgroundColor: "#F5FCFF88",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      alignItems: 'center',
      justifyContent: 'center'
    }
  });
};

export default dynamicStyles;
