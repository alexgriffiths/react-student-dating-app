import React, { useState, useEffect } from 'react';
import { View, Text, Image, TouchableOpacity, Platform, Linking, ActivityIndicator } from 'react-native';
// import Modal from 'react-native-modalbox';
import Modal from 'react-native-modal-patch';
import { useSelector, useDispatch } from 'react-redux';
import { useColorScheme } from 'react-native-appearance';
import Swiper from 'react-native-swiper';
import {
  initConnection,
  requestSubscription,
  getSubscriptions,
} from 'react-native-iap';
import { IMLocalized } from '../../localization/IMLocalization';
import { setPlans } from '../redux';
import dynamicStyles from './styles';
import { InAppBrowser } from 'react-native-inappbrowser-reborn';

export default function IMSubscriptionScreen(props) {
  const {
    visible,
    onClose,
    processing, 
    setProcessing,
    onSetSubscriptionPeriod,
    appStyles,
    appConfig,
  } = props;

  const dispatch = useDispatch();

  const currentUser = useSelector((state) => state.auth.user);
  const subscriptions = useSelector((state) => state.inAppPurchase.plans);

  const [selectedSubscriptionIndex, setSelectedSubscriptionIndex] = useState(1);
  const [selectedSubscriptionPlan, setSelectedSubscriptionPlan] = useState(subscriptions[1]);
  const [focusedSlide, setFocusedSlide] = useState(
    appConfig.subscriptionSlideContents[0],
  );

  const colorScheme = useColorScheme();
  const styles = dynamicStyles(appStyles, colorScheme);

  useEffect(() => {
    if (subscriptions.length === 0) {
      (async () => {
        await initConnection();
        getIAPProducts();
      })();
    }
  }, [subscriptions]);

  const onSubscriptioinPlanPress = (item, index) => {
    setSelectedSubscriptionIndex(index);
    setSelectedSubscriptionPlan(item);
  };

  const handleSubscription = async () => {
    const period =
      selectedSubscriptionPlan.subscriptionPeriodUnitIOS ||
      selectedSubscriptionPlan.subscriptionPeriodAndroid;

    try {
      setProcessing(true);
      onSetSubscriptionPeriod(period.toLowerCase());
      await requestSubscription(selectedSubscriptionPlan.productId);
      setProcessing(false);
    } catch (err) {
      setProcessing(false);
    }
  };

  const openTandCs = async () => {
    try {
      const url = 'https://www.uni-dating.com/privacy'
      if (await InAppBrowser.isAvailable()) {
        const result = await InAppBrowser.open(url)
      }
    } catch (error) {
      console.log(error);
    }
  }

  const getIAPProducts = async () => {
    try {
      const plans = await getSubscriptions(props.appConfig.IAP_SKUS);

      if (plans.length > 0) {
        setSelectedSubscriptionPlan(plans[1]);
      }

      dispatch(setPlans({ plans }));
    } catch (err) {
      console.log(err);
    }
  };

  const onSwipeIndexChange = (index) => {
    setFocusedSlide(appConfig.subscriptionSlideContents[index]);
  };

  const renderInactiveDot = () => <View style={styles.inactiveDot} />;

  const renderActiveDot = () => <View style={styles.activeDot} />;

  const pricePerMonth = (price, length) => {
    const intPrice = parseFloat(price);
    const intLen = parseFloat(length);

    const rounded = intPrice / intLen;
    return rounded.toFixed(2);
  };

  const savingsFromFirst = (first, price) => {
    const a = parseFloat(first) - price;
    const b = a / parseFloat(first);

    return (b * 100).toFixed(0);
  };

  const renderSubScriptionPlan = (item, index) => {
    const ppm = pricePerMonth(item.price, item.subscriptionPeriodNumberIOS);
    const savings = savingsFromFirst(subscriptions[0].price, ppm);

    return (
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => onSubscriptioinPlanPress(item, index)}
        style={index == 1 ? styles.subscriptionContainerBold : styles.subscriptionContainer}>

        {index == 1 && <View style={styles.labelView}>
          <Text style={styles.labelText}>Most Popular</Text>  
        </View>}

        <View style={styles.rateContainer}>
          <Text style={styles.bigNumber}>
            {item.subscriptionPeriodNumberIOS}
          </Text>
          <Text style={styles.monthText}>
            {item.subscriptionPeriodNumberIOS == "1" ? "month" : "months"}
          </Text>
          <Text style={styles.rateText}>
          {item?.localizedPrice.charAt(0) + ppm}/month
            {/* <Text style={styles.monthText}>
              {Platform.OS === 'ios'
                ? item?.subscriptionPeriodUnitIOS?.toLowerCase()
                : item.productId === 'annual_vip_subscription'
                ? IMLocalized('year')
                : IMLocalized('month')}
            </Text> */}
          </Text>
          <Text style={index > 0 ? styles.savings : {...styles.savings, color: "white"}}>
            {"Save " + savings + "%"}
          </Text>
          <Text style={styles.rateFullText}>
            £{item?.price}
          </Text>

          
          {/* <Text style={{marginLeft: 15}}>
            Popular
          </Text> */}
        </View>

        <View style={styles.selectContainer}>
          <View
            style={[
              styles.tickIconContainer,
              selectedSubscriptionIndex === index &&
                styles.selectedSubscription,
            ]}>
            {selectedSubscriptionIndex === index && (
              <Image
                style={styles.tick}
                source={require('../assets/tick.png')}
              />
            )}
            
          </View>
        </View>
        
      </TouchableOpacity>
    );
  };

  return (
    <Modal
      visible={visible}
      onDismiss={onClose}
      onRequestClose={onClose}
      animationType={'fade'}
      presentationStyle={'pageSheet'}>
      <View style={styles.container}>
        <View style={styles.carouselContainer}>
          <Swiper
            onIndexChanged={onSwipeIndexChange}
            removeClippedSubviews={false}
            containerStyle={{ flex: 1 }}
            dot={renderInactiveDot()}
            activeDot={renderActiveDot()}
            paginationStyle={{
              bottom: 20,
            }}
            loop={false}>
            {appConfig.subscriptionSlideContents.map((image, index) => (
              <View key={index + ''} style={styles.carouselImageContainer}>
                <Image style={styles.carouselImage} source={image.src} />
              </View>
            ))}
          </Swiper>
        </View>
        <View style={styles.subscriptionsContainer}>
          <Text style={styles.headerTitle}>WTF that's a bargain!</Text>
          <Text style={styles.titleDescription}>
            {focusedSlide.description}
          </Text>
          <View style={styles.subscriptionPlansContainer}>
            {subscriptions.map(renderSubScriptionPlan)}
          </View>
        </View>
        <View style={styles.bottomContainer}>
          <Text style={styles.bottomHeaderTitle}>
            {IMLocalized('Recurring billing, cancel anytime')}
          </Text>
          <Text style={styles.titleDescriptionSmall}>
              By tapping “Upgrade”, your payment will be charged to your iTunes account, and your subscription will automatically renew for the same package length and price until you cancel it in the iTunes store at least 24 hours prior to the end of the current period. By tapping “Upgrade”, you agree to our
              <Text onPress={() => openTandCs()} style={{...styles.titleDescriptionSmall, color: "blue", opacity: 0.5, textDecorationLine: "underline"}}> Terms & Conditions and Privacy Policy</Text>
              
          </Text>
          <TouchableOpacity
            // disabled={processing || subscriptions.length < 1}
            onPress={handleSubscription}
            style={styles.bottomButtonContainer}>
            <Text style={styles.buttonTitle}>{'Upgrade'}</Text>
          </TouchableOpacity>

          {/* {Platform.OS !== 'ios' && ( 
            <TouchableOpacity onPress={onClose}>
              <Text style={styles.cancelTitle}>{'Cancel'}</Text>
            </TouchableOpacity>
          )} */}
        </View>
      </View>
      {processing && <ActivityIndicator size={"large"} style={styles.indicator}/>}

    </Modal>
  );
}
