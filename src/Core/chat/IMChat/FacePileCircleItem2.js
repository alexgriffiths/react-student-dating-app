import React from 'react';
import { Modal, StyleSheet, Animated, TouchableOpacity, Text } from 'react-native';
import FastImage from 'react-native-fast-image';
import appStyles from '../IMChat/styles';

export default function FacePileCircleItem2(props) {
  const { name, image, modalVisible, setModalVisible } = props;

  // console.log(props.isModalVisible);
  
  // const innerCircleSize = circleSize * 2;
  // const marginRight = circleSize * offset;

  const showCardDetailsView = () => {

  };

  return (
    <Animated.View style={{ 10: -10, alignItems: "center"}}>
      <TouchableOpacity onPress={() => setModalVisible(!modalVisible)}>
        <Text style={{color: "#eb5a6d", fontSize: 20}}>
          {name}
        </Text>
      </TouchableOpacity>

      {/* <FastImage
        style={[
          styles.facePileCircleImage,
          {
            width: innerCircleSize,
            height: innerCircleSize,
            borderRadius: circleSize,
          },
        ]}
        source={{ uri: image }}
      /> */}
    </Animated.View>
  );
}
