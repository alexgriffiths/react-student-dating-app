import PropTypes from 'prop-types';
import React, {
  useEffect,
  useLayoutEffect,
  useContext,
  useState,
  useRef,
} from 'react';
import { useSelector, ReactReduxContext } from 'react-redux';
import IMConversationList from '../IMConversationList';
import ChannelsTracker from '../firebase/channelsTracker';

const IMConversationListView = (props) => {
  const appStyles =
    (props.navigation &&
      props.route &&
      props.route.params &&
      props.route.params.appStyles) ||
    props.appStyles;
  const currentUser = useSelector((state) => state.auth.user);
  const channels = useSelector((state) => state.chat.channels);
  const { store } = useContext(ReactReduxContext);
  const channelsTracker = useRef(null);
  const [conversationsFiltered, setConversationsFiltered] = useState([]);

  const mates = props.mates;

  useEffect(() => { 
    const userId = currentUser.id || currentUser.userID;
    if (!userId) {
      return;
    }
    channelsTracker.current = new ChannelsTracker(store, userId, currentUser.cityId);
    channelsTracker.current.subscribeIfNeeded();
  }, [currentUser?.id]);

  if (channels) {
    // console.log(channels);

    var newConversations = [];
    if (mates) {
      for (let convo of channels) { 

        if (convo.participants[0]?.settings?.traffic_light != "green") {
          newConversations.push(convo);
        }
      }
    } else {
      for (let convo of channels) {
        if (convo.participants[0]?.settings?.traffic_light == "green") {
          newConversations.push(convo);
        }
      }
    }
  }
  
  useEffect(() => {
    return () => {
      channelsTracker.current?.unsubscribe();
    };
  }, []);

  const onConversationPress = (channel) => {
    props.navigation.navigate('PersonalChat', {
      channel: { ...channel, name: channel.title },
      appStyles: appStyles,
    });
  };

  return (
    <IMConversationList
      loading={channels == null}
      conversations={newConversations}
      onConversationPress={onConversationPress}
      appStyles={appStyles}
      emptyStateConfig={props.emptyStateConfig}
      user={currentUser}
      mates={mates}
      emptyMatches={props.emptyMatches}
    />
  );
};

export default IMConversationListView;
