import React, { useEffect, useRef } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import deviceStorage from '../utils/AuthDeviceStorage';

import { logout } from '../redux/auth'
import { useDispatch } from 'react-redux';

const LoadScreen = (props) => {
  const dispatch = useDispatch();

  const { navigation, route } = props;

  const logoutBool = route.params.logout;

  const appStyles = route.params.appStyles;
  const appConfig = route.params.appConfig;
  const didFocusSubscription = useRef(
    props.navigation.addListener('focus', (payload) => {
      setAppState();
    }),
  );
  if (logoutBool) {
    console.log("dispatch log out");
    dispatch(logout());
  }

  useEffect(() => {

    setAppState();
    return () => {
      didFocusSubscription.current && didFocusSubscription.current();
    };
  }, []);

  const setAppState = async () => {
    // const shouldShowOnboardingFlow = await deviceStorage.getShouldShowOnboardingFlow();
    const shouldShowOnboardingFlow = false;
    if (!shouldShowOnboardingFlow) {
      if (appConfig.isDelayedLoginEnabled) {
        navigation.navigate('DelayedHome');  
        return;
      }
      navigation.navigate('LoginStack', {
        appStyles: appStyles,
        appConfig: appConfig,
      });
    } else {
      navigation.navigate('Walkthrough', {
        appStyles: appStyles,
        appConfig: appConfig,
      });
    }
  };

  return <View />;
};

LoadScreen.propTypes = {
  user: PropTypes.object,
  navigation: PropTypes.object,
};

LoadScreen.navigationOptions = {
  header: null,
};

export default LoadScreen;
