import React, { useState, useEffect } from 'react';
import {
  FlatList,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Linking,
  Alert
} from 'react-native';

import dynamicStyles from './styles';
import { useColorScheme } from 'react-native-appearance';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { firebase } from '../../firebase/config';
import TNActivityIndicator from '../../truly-native/TNActivityIndicator';
import { SearchBar } from 'react-native-elements';
import FastImage from 'react-native-fast-image';

const UniversityInfo = (props) => {
  const appStyles = props.appStyles;
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(appStyles, colorScheme);
  const uniRef = firebase.firestore().collection("universities");
  const [unis, setUnis] = useState([]);

  const [filteredUnis, setFilteredUnis] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");


  useEffect(() => {
    const newExtensionsMap = [];
    try {
      uniRef.get().then((uniSnapshot) => {
        uniSnapshot.forEach((docSnapshot) => {
            const name = docSnapshot.data().name;
            const extensions = docSnapshot.data().extension.values();
            const id = docSnapshot.data().id;
            const imageUrl = docSnapshot.data().imageUrl;
            newExtensionsMap.push({name: name, image: imageUrl, extension: Array.from(extensions), id: id})
          });
          setUnis(newExtensionsMap);
          setFilteredUnis(newExtensionsMap);
      });
    } catch (error) {
        console.log(error);
    }

  }, []);

  const renderItem = ({ item }) => {
    return (
      <View style={styles.uniItem}>


        <View style={{marginLeft: 10, marginVertical: 10, flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
          <View>
            <Text style={styles.uniName}>{item.name} </Text>
              
            <View style={{flexDirection: "row"}}>
              {item.extension.slice(0,3).map(ext => <Text>{ext} </Text>)}
            </View>
          </View>
          <FastImage 
              source={{uri: item.image}}
              style={styles.uniImage}
              resizeMode={"stretch"}
              on
            />
        </View>
      </View>
      
    );
  };

  const handleSearch = (text) => {
    setSearchTerm(text);
    const lowerText = text.toLowerCase();
    const filteredData = unis.filter(uni => uni.name.toLowerCase().includes(lowerText));
    
    if (text === "") {
      console.log("empty");
    }

    setFilteredUnis(filteredData);


  }

  const helpAlert = () => {
    Alert.alert(
      'Need Help?',
        "If you can't find your University or the wrong email extension is included, please let us know and we will get it fixed ASAP:)",
      [
        {
          text: "Contact Us",
          onPress: () => Linking.openURL('mailto:contact@uni-dating.com?subject=Eligible Universities Issue')
        },  
        { 
          text: 'Cancel',
        }
      ],
      {
        cancelable: false,
      },
    );
  }
 
  return (
    <View style={styles.uniInfoContainer}>

        <View style={styles.uniInfoTitleView}>
          <TouchableOpacity 
            style={{height: 25, marginLeft: 10}}
            onPress={props.closeModal}>
            <Icon name="arrow-left" size={25} color="black"/>
          </TouchableOpacity>
          <Text style={styles.uniInfoTitle}>Eligible Universities</Text>
          <TouchableOpacity 
            style={{height: 25, marginRight: 10}}
            onPress={() => helpAlert()}
          >
            <Icon name="help" size={25} color="black"/>
          </TouchableOpacity>
        </View>
 
        <SearchBar 
          placeholder="Search Universities"
          value={searchTerm}
          onChangeText={handleSearch}
          lightTheme={true}
        />

        {unis === [] ? 
        <TNActivityIndicator appStyles={appStyles} />
        :
        <FlatList
          data={filteredUnis}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
        />
        }
        <TouchableOpacity
          style={{alignItems: "center", marginBottom: 20}}
          onPress = {() => Linking.openURL("https://clearbit.com")}
        >
          <Text style={{fontSize: 12, color: "grey"}}>Logos provided by Clearbit</Text>
        </TouchableOpacity>

    </View>
  );
};

export default UniversityInfo;
