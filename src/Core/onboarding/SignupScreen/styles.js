import { StyleSheet, Dimensions, I18nManager } from 'react-native';
import { modedColor } from '../../helpers/colors';
import TNColor from '../../truly-native/TNColor';

const { height } = Dimensions.get('window');
const { width } = Dimensions.get('window').width;
const imageSize = height * 0.232;
const photoIconSize = imageSize * 0.27;

const dynamicStyles = (appStyles, colorScheme) => {
  return StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    imageBackground: {
      width: imageSize,
      height: imageSize,
      alignItems: "center",
      justifyContent: "center",
      alignSelf: "center",
      paddingTop: imageSize * 0.3,
      marginTop: -60,
    },
    title: {
      fontSize: 30,
      fontWeight: 'bold',
      color: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
      // marginTop:-10,
      marginBottom: 30,
      alignSelf: 'stretch',
      textAlign: 'center',
    },
    uniImage: {
      height: 40,
      width: 40,
      // borderWidth: 1,
      // borderColor: "black",
      marginRight: 30
    },


    content: {
      paddingLeft: 50,
      paddingRight: 50,
      textAlign: 'center',
      fontSize: appStyles.fontSet.middle,
      color: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
    },
    loginContainer: {
      width: appStyles.sizeSet.buttonWidth,
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
      borderRadius: appStyles.sizeSet.radius,
      padding: 10,
      marginTop: 30,
    },
    loginText: {
      color: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    placeholder: {
      color: 'red',
    },
    InputContainer: {
      height: 42,
      borderWidth: 1,
      borderColor: appStyles.colorSet[colorScheme].grey3,
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
      paddingLeft: 20,
      color: appStyles.colorSet[colorScheme].mainTextColor,
      width: '80%',
      alignSelf: 'center',
      marginTop: 20,
      alignItems: 'center',
      borderRadius: appStyles.roundness.textInputRadius,
      textAlign: I18nManager.isRTL ? 'right' : 'left',
      ...appStyles.shadows.buttonShadow
    },

    EmailInputContainerView: {
      height: 42,
      borderWidth: 1,
      borderColor: appStyles.colorSet[colorScheme].grey3,
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
      paddingLeft: 20,
      ...appStyles.shadows.buttonShadow,
      width: '80%',
      alignSelf: 'center',
      marginTop: 20,
      justifyContent: "space-between",
      alignItems: 'center',
      borderRadius: appStyles.roundness.textInputRadius,

      flexDirection: "row",
    },

    EmailInputContainer: {
      textAlign: I18nManager.isRTL ? 'right' : 'left',
      color: appStyles.colorSet[colorScheme].mainTextColor,
      width: "85%"
    },

    uniText: {
      color: appStyles.colorSet[colorScheme].mainTextColor, 
      textAlign: "center",
      fontSize: 16,
      letterSpacing: 1.2
    },

    signupContainer: {
      alignSelf: 'center',
      width: "100%", // appStyles.sizeSet.buttonWidth,
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
      padding: 10,
      marginTop: 40,
      ...appStyles.shadows.buttonShadow
    },

    signupText: {
      color: appStyles.colorSet[colorScheme].mainTextColor,
    },
    image: {
      width: '100%',
      height: '100%',
    },
    imageBlock: {
      flex: 2,
      flexDirection: 'row',
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
    imageContainer: {
      height: imageSize,
      width: imageSize,
      borderRadius: imageSize,
      shadowColor: '#006',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.1,
      overflow: 'hidden',
    },
    formContainer: {
      width: '100%',
      flex: 4,
      alignItems: 'center',
    },
    photo: {
      marginTop: imageSize * 0.77,
      marginLeft: -imageSize * 0.29,
      width: photoIconSize,
      height: photoIconSize,
      borderRadius: photoIconSize,
    },

    addButton: {
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#d9d9d9',
      opacity: 0.8,
      zIndex: 2,
    },
    orTextStyle: {
      color: 'black',
      marginTop: 20,
      marginBottom: 10,
      alignSelf: 'center',
      color: appStyles.colorSet[colorScheme].mainTextColor,
    },
    PhoneNumberContainer: {
      marginTop: 10,
      marginBottom: 10,
      alignSelf: 'center',
    },
    smsText: {
      color: '#4267b2',
    },
    tos: {
      marginTop: 40,
      alignItems: 'center',
      justifyContent: 'center',
      height: 30,
    },

    uniInfoTitle: {
      marginTop: 10,
      fontSize: 20,
      color: appStyles.colorSet[colorScheme].mainTextColor
      // width: "90%",
      // textAlign: "center"

    },
    
    uniName: {
      fontSize: 18, 
      fontWeight: "500",
    },

    uniInfoTitleView: {
      paddingTop: 50,
      paddingBottom: 20,
      // width: "100%",
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
      alignItems: "center",
      flexDirection: "row",
      justifyContent: "space-between"
    },
    uniInfoContainer: {
      flex: 1,
      // alignItems: 'center',
    },
    uniItem: {
      justifyContent: "flex-start", 
      opacity: 0.6,
      borderBottomColor: "black",
      borderBottomWidth: 1,
      justifyContent: "center"
    },
    searchContainer: {
      height: 42,
      borderWidth: 1,
      borderColor: appStyles.colorSet[colorScheme].grey3,
      paddingLeft: 20,
      width: '98%',
      alignSelf: 'center',
      marginVertical: 8,
      alignItems: 'center',
      borderRadius: 8,
    },



  });
};

export default dynamicStyles;
