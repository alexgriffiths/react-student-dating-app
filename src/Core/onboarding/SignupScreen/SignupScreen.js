import React, { useState, useEffect } from 'react';
import {
  Alert,
  Image,
  Keyboard,
  Text,
  TextInput,
  TouchableOpacity, 
  View,
  Checkbox,
  Modal,
  ImageBackground
} from 'react-native';
import Button from 'react-native-button';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import dynamicStyles from './styles';
import { useColorScheme } from 'react-native-appearance';
import TNActivityIndicator from '../../truly-native/TNActivityIndicator';
import TNProfilePictureSelector from '../../truly-native/TNProfilePictureSelector/TNProfilePictureSelector';
import { IMLocalized } from '../../localization/IMLocalization';
import { setUserData } from '../redux/auth';
import { connect } from 'react-redux';
import { localizedErrorMessage } from '../utils/ErrorCode';
import TermsOfUseView from '../components/TermsOfUseView';
import { firebase } from '../../firebase/config';
import auth from '@react-native-firebase/auth';
import FadeInOut from 'react-native-fade-in-out';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import UniversityInfo from './UniversityInfo';

const SignupScreen = (props) => {
  const appConfig = props.route.params.appConfig;
  const appStyles = props.route.params.appStyles;
  const authManager = props.route.params.authManager;

  const colorScheme = useColorScheme();
  const styles = dynamicStyles(appStyles, colorScheme);

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [profilePictureFile, setProfilePictureFile] = useState(null);
  const [loading, setLoading] = useState(false);

  const [university, setUniversity] = useState("");
  const [uniId, setUniId] = useState("");
  const [uniExtensionMap, setUniExtensionMap] = useState([]);

  const uniRef = firebase.firestore().collection("universities");

  const [showUniInfo, setShowUniInfo] = useState(false);
 
  // const assets = {
  //   gradient: require("../../../../assets/images/gradient.png"),
  // }

  useEffect(() => {
    const newExtensionsMap = [];
    try {
      uniRef.get().then((uniSnapshot) => {
        uniSnapshot.forEach((docSnapshot) => {
            const name = docSnapshot.data().name;
            const extensions = docSnapshot.data().extension.values();
            const id = docSnapshot.data().id;

            newExtensionsMap.push({name: name, extensions: extensions, id: id})
          });
          setUniExtensionMap(newExtensionsMap);
      });
    } catch (error) {
        console.log(error);
    }
  }, []);


  const validateEmail = (text) => {
    let reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return reg.test(String(text).toLowerCase()) ? true : false;
  };

  const isCorrectDomain = (email) => {
    var domain = email.split('@')[1];
    // return (domain == "brunel.ac.uk");
    return (domain == "gmail.com");
  }
  
  const validatePassword = (text) => {
    let reg = /^(?=.*[A-Z])(?=.*[a-z])/;
    return reg.test(String(text)) ? true : false;
  };

  const showAbout = () => {
    Alert.alert(
      'About',
      IMLocalized(
        "This app is exclusively for UK University students over the age of 18",
      ),
      [{ text: IMLocalized('OK') }],
      {
        cancelable: false,
      },
    );
  };

  const checkUniversity = (emailText) => {
    if(validateEmail(emailText?.trim())) {
      var extension = email?.trim().split('@')[1]
      var found = false;

      for (let uni of uniExtensionMap) {
        if (found) {
          break;
        }
        const name = uni.name;
        const extensions = uni.extensions;
        const id = uni.id;

        Array.from(extensions).forEach((e) => {
          console.log(e);
          if (extension === e) {
            setUniversity(name);
            setUniId(id);
            found = true;
          }
        })
      }
    }
  };
  
  const onRegister = () => {
    if(profilePictureFile == null) {
      Alert.alert(
        'Profile Picture Required',
        IMLocalized(
          'Please tap on the camera icon to upload a profile picture.',
        ),
        [{ text: IMLocalized('OK') }],
        {
          cancelable: false,
        },
      );
      return;
    };

    if(!validateEmail(email?.trim())) {
      Alert.alert(
        '',
        IMLocalized(
          'Please enter a valid email address.',
        ),
        [{ text: IMLocalized('OK') }],
        {
          cancelable: false,
        },
      );
      return;
    };

    if (uniId == '') {
      Alert.alert(
        '',
        IMLocalized(
          'Please use a University email address when registering',
        ),
        [{ text: IMLocalized('OK') }],
        {
          cancelable: false,
        },
      );
      return;
    }

    if (password?.trim() == '') {
      Alert.alert(
        '',
        IMLocalized(
          'Password cannot be empty.',
        ),
        [{ text: IMLocalized('OK') }],
        {
          cancelable: false,
        },
      );
      setPassword('');
      return;
    };

    if (password?.trim()?.length < 6) {
      Alert.alert(
        '',
        IMLocalized(
          'Password is too short. Please use at least 6 characters for security reasons.',
        ),
        [{ text: IMLocalized('OK') }],
        {
          cancelable: false,
        },
      );
      setPassword('');
      return;
    };

    setLoading(true);

    const userDetails = {
      firstName: firstName?.trim(),
      lastName: lastName?.trim(),
      email: email && email.trim(),
      password: password && password.trim(),
      photoFile: profilePictureFile,
      appIdentifier: appConfig.appIdentifier,
    };

    authManager
      .createAccountWithEmailAndPassword(userDetails, appConfig)
      .then((response) => {
        const user = response.user;
        if (user) {
          props.setUserData({
            user: response.user,
          });
          Keyboard.dismiss();
          
          setLoading(false);
          
          let userToVerify = firebase.auth().currentUser;

          userToVerify.sendEmailVerification().then(function() {
            // Email sent.
            props.navigation.navigate('LoginStack', {
              screen: 'VerifyEmail',
              params: {
                appStyles,
                appConfig,
                authManager,
                user: user
              },
            });
          }).catch(function(error) {
            console.log(error);
          });
         
        } else {
          setLoading(false);
          Alert.alert(
            '',
            localizedErrorMessage(response.error),
            [{ text: IMLocalized('OK') }],
            {
              cancelable: false,
            },
          );
        }
      });
  };

  const renderSignupWithEmail = () => {
    return (
      <>
        <Text style={styles.uniText}>
          {university ? university : ""}
        </Text>
        <TextInput
          style={styles.InputContainer}
          placeholder={IMLocalized('First Name')}
          placeholderTextColor="#aaaaaa"
          onChangeText={(text) => setFirstName(text)}
          value={firstName}
          underlineColorAndroid="transparent"
        />
        <TextInput
          style={styles.InputContainer}
          placeholder={IMLocalized('Last Name')}
          placeholderTextColor="#aaaaaa"
          onChangeText={(text) => setLastName(text)}
          value={lastName}
          underlineColorAndroid="transparent"
        />
        <View style={styles.EmailInputContainerView}>
          <TextInput
            style={styles.EmailInputContainer}
            placeholder={IMLocalized('University Email Address')}
            placeholderTextColor="#aaaaaa"
            onChangeText={(text) => setEmail(text)}
            onEndEditing={(e) => checkUniversity(e.nativeEvent.text)}
            value={email}
            underlineColorAndroid="transparent"
            autoCapitalize="none"
          />
          <TouchableOpacity 
            style={{height: 25, width: 25, marginRight: 10}}
            onPress={() => setShowUniInfo(true)}>
            <Icon name="information-outline" size={25} color="#7d7d7d"/>
          </TouchableOpacity>
        </View>

        <TextInput
          style={styles.InputContainer}
          placeholder={IMLocalized('Password')}
          placeholderTextColor="#aaaaaa"
          secureTextEntry
          onChangeText={(text) => setPassword(text)}
          value={password}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <FadeInOut visible={firstName.trim() && lastName.trim() && email.trim() && password.trim()}>
          <Button
            containerStyle={styles.signupContainer}
            style={styles.signupText}
            onPress={() => onRegister()}>
            {IMLocalized('Sign Up')}
          </Button>
        </FadeInOut>

      </>
    ); 
  };


  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView
        style={{ flex: 1, width: '100%' }}
        // keyboardShouldPersistTaps="always"
        >
        
        <Modal
            visible={showUniInfo}
            animationType={"slide"}>
              <UniversityInfo appStyles={appStyles} closeModal={() => setShowUniInfo(false)}/>
        </Modal>

        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <Image
            style={appStyles.styleSet.backArrowStyle }
            source={appStyles.iconSet.backArrow}
          /> 
        </TouchableOpacity>
        <Text style={styles.title}>Sign Up</Text>

        <ImageBackground
          style={styles.imageBackground}
          resizeMode={"contain"}
          source={require("../../../../assets/images/gradient.png")}>
          <TNProfilePictureSelector
            setProfilePictureFile={setProfilePictureFile}
            appStyles={appStyles}
          />
        </ImageBackground>

        {renderSignupWithEmail()}


        <TermsOfUseView tosLink={appConfig.tosLink} privacyPolicyLink={appConfig.privacyPolicyLink} style={styles.tos} />
        <Button onPress={() => showAbout()}>
          <Text style={{marginTop: 25, alignSelf: "center", color: appStyles.colorSet[colorScheme].mainTextColor}}>
            Who can register?
          </Text>
        </Button>
        {loading && <TNActivityIndicator appStyles={appStyles} />}
      </KeyboardAwareScrollView>
     
    </View>
  );
};

export default connect(null, {
  setUserData,
})(SignupScreen);
