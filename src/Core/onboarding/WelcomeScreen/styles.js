import { StyleSheet } from 'react-native';
import { Platform } from 'react-native';

const dynamicStyles = (appStyles, colorScheme) => {
  return StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    logo: {
      width: 150,
      height: 150,
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: 20,
      // marginTop: -100,
    },
    logoImage: {
      width: '100%',
      height: '100%',
      resizeMode: 'contain',
      // tintColor: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
    },
    title: {
      fontSize: 30,
      fontWeight: appStyles.boldness.bigTitle,
      color: appStyles.colorSet[colorScheme].mainTextColor,
      marginTop: 20,
      marginBottom: 20,
      textAlign: 'center',
    },
    contactUs: {
      fontSize: 16,
      color: appStyles.colorSet[colorScheme].mainTextColor,
      marginTop: 10,
      marginBottom: 30,
      top: "-80%",
      textAlign: 'center',
    },
    caption: {
      fontSize: 16,
      paddingHorizontal: 50,
      marginBottom: 20,
      textAlign: 'center',
      color: appStyles.colorSet[colorScheme].mainTextColor,
    },
    loginContainer: {
      width: '70%',
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
      borderRadius: 12,
      padding: 10,
      marginTop: 30,
      alignSelf: 'center',
      justifyContent: 'center',
      height: 48,
      ...appStyles.shadows.buttonShadow
    },
    loginText: {
      color: appStyles.colorSet[colorScheme].mainTextColor,
    },
    signupContainer: {
      justifyContent: 'center',
      width: '70%',
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
      borderRadius: 12,

      padding: 10,
      marginTop: 20,
      ...appStyles.shadows.buttonShadow

    },
    signupText: {
      color: appStyles.colorSet[colorScheme].mainTextColor,
    },
    dismissButton: {
      position: 'absolute',
      top: 15,
      right: 15,
    }
  });
};

export default dynamicStyles;
