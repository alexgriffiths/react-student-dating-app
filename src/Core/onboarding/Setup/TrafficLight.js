import React, { useEffect, useState } from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  Image,
  ScrollView
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import dynamicStyles from './styles';
import { useColorScheme } from 'react-native-appearance';
import FadeInOut from 'react-native-fade-in-out';
import { firebaseUser } from '../../firebase';
import { setUserData } from '../../../Core/onboarding/redux/auth';

const TrafficLight = (props) => {
  const appConfig = props.route.params.appConfig;
  const appStyles = props.route.params.appStyles;
  const authManager = props.route.params.authManager;
  const user = props.route.params.user;

  const [trafficLight, setTrafficLight] = useState("");

  const colorScheme = useColorScheme();
  const styles = dynamicStyles(appStyles, colorScheme);

  const dispatch = useDispatch();

  const onNextPressed = () => {
      var newSettings = user.settings;
      newSettings['traffic_light'] = trafficLight;

      let newUser = { ...user, settings: newSettings };
      firebaseUser.updateUserData(user.id, user.cityId, newUser);
      updateUserInfo(newUser);
      props.navigation.navigate('LoginStack', {
        screen: 'UserSetup2',
        params: {
          appStyles,
          appConfig,
          authManager,
          user: newUser
        },
    });
  }

  const updateUserInfo = (newUser) => {
    dispatch(setUserData({ user: newUser}));
  };

  const renderTrafficLightButtoms = () => {
    let redWidth = 0;
    let amberWidth = 0;
    let greenWidth = 0;

    if (trafficLight === "green") {
        greenWidth = 3;
    } else if (trafficLight === "red") {
        redWidth = 3;
    } else if (trafficLight === "amber") {
        amberWidth = 3;
    }; 

    return (
    <>
        <TouchableOpacity
            style={{...styles.trafficLight, borderColor: "#ef8484", shadowColor: "#ef8484", borderWidth: redWidth}}
            onPress={() => setTrafficLight("red")}
        >
        <Text style={styles.trafficLightText}>Red - In a relationship</Text>
        </TouchableOpacity>
        <TouchableOpacity
            style={{...styles.trafficLight, borderColor: "#ffb57f", shadowColor: "#ffb57f", borderWidth: amberWidth}}
            onPress={() => setTrafficLight("amber")}
        >
            <Text style={styles.trafficLightText}>Orange - It's complicated</Text>
        </TouchableOpacity>
        <TouchableOpacity
            style={{...styles.trafficLight, borderColor: "#b4e6a7", shadowColor: "#b4e6a7", borderWidth: greenWidth}}
            onPress={() => setTrafficLight("green")}
        >
            <Text style={styles.trafficLightText}>Green - Single</Text>
        </TouchableOpacity>
   </>
    );
  };

  return (
    <View style={styles.container}>
        <View style={{flex: 1, width: "100%"}}>
            <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <Image
                style={appStyles.styleSet.backArrowStyle}
                source={appStyles.iconSet.backArrow}
            />
            </TouchableOpacity>
            <Text style={styles.title}>Pick Your Light</Text>
            <Text style={styles.titleSmaller2}>Each profile on Uni Lights is clearly colour coded so everyone is on the same page! Select which light applies to you</Text>

            {/* <Image
                style={{height: 100, width: 100}}
                source={appStyles.iconSet.loveTraffic}
            /> */}

            {/* {renderTraffic Lights()} */}

            <View style={{width: "100%", alignItems: "center"}}>
                {renderTrafficLightButtoms()}
            </View>

            <FadeInOut visible={trafficLight !== ""} style={{marginTop: 40, alignItems: "center"}}>
                <TouchableOpacity 
                  style={{...styles.nextBtn, marginTop: 40}}
                  onPress={() => onNextPressed()}>
                  <Text style={styles.nextText}>Next</Text>
                </TouchableOpacity>
            </FadeInOut>

        </View>
    </View>
  );
};

export default TrafficLight;
