import React, { useState } from 'react';
import {
  Alert,
  Image,
  Keyboard,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Switch
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import Button from 'react-native-button';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import SwitchSelector from "react-native-switch-selector";

import dynamicStyles from './styles';
import { useColorScheme } from 'react-native-appearance';
import { IMLocalized } from '../../localization/IMLocalization';
import { connect } from 'react-redux';
import { firebaseUser } from '../../firebase';

import { setUserData } from '../../../Core/onboarding/redux/auth';


const UserSetup = (props) => {
  const appConfig = props.route.params.appConfig;
  const appStyles = props.route.params.appStyles;
  const authManager = props.route.params.authManager;
  const user = props.route.params.user;

  const colorScheme = useColorScheme();
  const styles = dynamicStyles(appStyles, colorScheme);

  const [gender, setGender] = useState('male');
  const [genderInterest, setGenderInterest] = useState('male');
  const [want, setWant] = useState('relationship');

  const [settings, setSettings] = useState(null);

  const dispatch = useDispatch();

  const onRegister = () => {
    var tempSettings = {};
    tempSettings['gender'] = gender;
    tempSettings['gender_preference'] = genderInterest;
    tempSettings['want'] = want;
    tempSettings['distance_radius'] = "unlimited";

    let newUser = { ...user, settings: tempSettings };
    firebaseUser.updateUserData(user.id, user.cityId, newUser);
    updateUserInfo(newUser);
    props.navigation.navigate('LoginStack', {
        screen: 'TrafficLight',
        params: {
          appStyles,
          appConfig,
          authManager,
          user: newUser
        },
    });
  };

  const updateUserInfo = (newUser) => {
    dispatch(setUserData({ user: newUser}));
  };

  const genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Non-Binary", value: "none" }
  ];

  const lookingFor = [
    { label: "Relationship", value: "relationship" },
    { label: "FWB", value: "fwb" },
    { label: "Friends", value: "friends" },
  ];

  const updateUserDetails = () => {
    return (
      <>
       
        <Text style={styles.subHeadingText}>Your Gender</Text>
        <SwitchSelector
            style={{marginTop:30, width: "80%", alignSelf: "center"}}
            options={genders}
            buttonColor={"#b4e6a7"}
            initial={0}
            onPress={value => setGender(value)}
        />

        <Text style={styles.subHeadingText}>Who Are You Interested In?</Text>
        <SwitchSelector
            style={{marginTop:30, width: "80%", alignSelf: "center"}}
            options={genders}
            buttonColor={"#b4e6a7"}
            initial={0} 
            onPress={value => setGenderInterest(value)}
        />
        {/* <Text style={styles.subHeadingText}>What Are You Looking For?</Text>
        <SwitchSelector
            style={{marginTop:30, width: "80%", alignSelf: "center"}}
            options={lookingFor}
            buttonColor={"#298367"}
            initial={0}
            onPress={value => setWant(value)}
        /> */}

        <Button
          containerStyle={styles.signupContainer}
          style={styles.signupText}
          onPress={() => onRegister()}>
          {IMLocalized('Next')}
        </Button>

      </>
    );
  };

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView
        style={{ flex: 1, width: '100%' }}
        keyboardShouldPersistTaps="always">
        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <Image
            style={appStyles.styleSet.backArrowStyle}
            source={appStyles.iconSet.backArrow}
          />
        </TouchableOpacity>
        <Text style={styles.title}>{IMLocalized('About You')}</Text>
        {updateUserDetails()}
        
      </KeyboardAwareScrollView>
      {/* {loading && <TNActivityIndicator appStyles={appStyles} />} */}
    </View>
  );
};

export default UserSetup;
