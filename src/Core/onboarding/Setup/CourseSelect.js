import React, { useState, useEffect } from 'react';
import {
  FlatList,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Linking,
  Alert
} from 'react-native';
 
import dynamicStyles from './styles';
import { useColorScheme } from 'react-native-appearance';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { firebase } from '../../firebase/config';
import TNActivityIndicator from '../../truly-native/TNActivityIndicator';
import { SearchBar } from 'react-native-elements';

const CourseSelect = (props) => {
  const appStyles = props.appStyles;
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(appStyles, colorScheme);
  const uniRef = firebase.firestore().collection("universities");

  const coursesRef = firebase.firestore().collection("courses");
  const [courses, setCourses] = useState([]);

  const [filteredCourses, setFilteredCourses] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");


  useEffect(() => {
    const newCourses = [];
    try {
        coursesRef.get().then((coursesSnapshot) => {
            coursesSnapshot.forEach((docSnapshot) => {
              const values = docSnapshot.data().courses.values();
              var index = 0;
              for (let courseVal of values) {
                newCourses.push({id: index, name: courseVal.toString(), checked: false});
                index++;
              }
            });
          setCourses(newCourses);
          setFilteredCourses(newCourses);
        });
    } catch (error) {
        console.log(error);
    }

  }, []);

  const handlePress = (course) => {
    props.setCourse(course)
    props.closeModal();
  }

  const renderItem = ({ item }) => {
    return (
      <View style={styles.uniItem}>
        <TouchableOpacity 
            style={{marginLeft: 10, marginVertical: 14}}
            onPress={() => handlePress(item.name)}
        >
          <Text style={styles.uniName}>{item.name} </Text>
        </TouchableOpacity>
      </View>
      
    );
  };

  const handleSearch = (text) => {
    setSearchTerm(text);
    const lowerText = text.toLowerCase();
    const filteredData = courses.filter(course => course.name.toLowerCase().includes(lowerText));
    
    if (text === "") {
      console.log("empty");
    }

    setFilteredCourses(filteredData);


  }

  const helpAlert = () => {
    Alert.alert(
      'Need Help?',
        "If you can't find your course, please let us know and we will get it added ASAP:)",
      [
        {
          text: "Contact Us",
          onPress: () => Linking.openURL('mailto:contact@uni-dating.com?subject=My Course Is Missing')
        },  
        { 
          text: 'Cancel',
        }
      ],
      {
        cancelable: false,
      },
    );
  }
 
  return (
    <View style={styles.uniInfoContainer}>

        <View style={styles.uniInfoTitleView}>
          <TouchableOpacity 
            style={{height: 25, marginLeft: 10}}
            onPress={props.closeModal}>
            <Icon name="arrow-left" size={25} color="black"/>
          </TouchableOpacity>
          <Text style={styles.uniInfoTitle}>Choose Your Course</Text>
          <TouchableOpacity 
            style={{height: 25, marginRight: 10}}
            onPress={() => helpAlert()}
          >
            <Icon name="help" size={25} color="black"/>
          </TouchableOpacity>
        </View>
 
        <SearchBar 
          placeholder="Search Courses"
          value={searchTerm}
          onChangeText={handleSearch}
          lightTheme={true}
        />

        {courses === [] ? 
        <TNActivityIndicator appStyles={appStyles} />
        :
        <FlatList
          data={filteredCourses}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
        />
        }

    </View>
  );
};

export default CourseSelect;
