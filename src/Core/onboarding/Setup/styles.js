import { StyleSheet, Dimensions, I18nManager } from 'react-native';
import { modedColor } from '../../helpers/colors';
import TNColor from '../../truly-native/TNColor';

const { height } = Dimensions.get('window');
const imageSize = height * 0.232;
const photoIconSize = imageSize * 0.27;

const dynamicStyles = (appStyles, colorScheme) => {
  return StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    // title: {
    //   fontSize: 30,
    //   fontWeight: 'bold',
    //   color: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
    //   marginTop: 25,
    //   marginBottom: 30,
    //   alignSelf: 'stretch',
    //   textAlign: 'left',
    //   marginLeft: 35,
    // },
    title: {
      fontSize: 30,
      fontWeight: '500',
      color: appStyles.colorSet[colorScheme].mainTextColor,
      // marginTop:-10,
      marginBottom: 30,
      alignSelf: 'stretch',
      textAlign: 'center',
    },

    content: {
      paddingLeft: 50,
      paddingRight: 50,
      textAlign: 'center',
      fontSize: appStyles.fontSet.middle,
      color: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
    },
    loginContainer: {
      width: appStyles.sizeSet.buttonWidth,
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
      borderRadius: appStyles.sizeSet.radius,
      padding: 10,
      marginTop: 30,
    },
    loginText: {
      color: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    placeholder: {
      color: 'red',
    },
    InputContainer: {
      height: 42,
      borderWidth: 1,
      borderColor: appStyles.colorSet[colorScheme].grey3,
      backgroundColor: modedColor(
        appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
        // TNColor('#e0e0e0'),
      ),
      paddingLeft: 20,
      color: appStyles.colorSet[colorScheme].mainTextColor,
      width: '80%',
      alignSelf: 'center',
      marginTop: 30,
      alignItems: 'center',
      borderRadius: 15,
      textAlign: I18nManager.isRTL ? 'right' : 'left',
    },
    InputContainer3: {
      height: 42,
      width: "90%",
      borderWidth: 1,
      borderColor: appStyles.colorSet[colorScheme].grey3,
      backgroundColor: modedColor(
        appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
        // TNColor('#e0e0e0'),
      ),
      paddingLeft: 20,
      color: appStyles.colorSet[colorScheme].mainTextColor,
      alignSelf: 'center',
      justifyContent: 'center',
      marginTop: 30,
      alignItems: 'center',
      borderRadius: 15,
      textAlign: I18nManager.isRTL ? 'right' : 'left',
    },

    InputContainer2: {
      height: 60,
      borderWidth: 1,
      borderColor: appStyles.colorSet[colorScheme].grey3,
      backgroundColor: modedColor(
        appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
        // TNColor('#e0e0e0'),
      ),
      padding: 20, 
      paddingVertical: 10,
      color: appStyles.colorSet[colorScheme].mainTextColor,
      width: '80%',
      alignSelf: 'center',
      marginTop: 20,
      alignItems: 'center',
      borderRadius: 15,
      textAlign: I18nManager.isRTL ? 'right' : 'left',
      textAlignVertical: "top"
    },

    signupContainer: {
      alignSelf: 'center',
      width: "100%",
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
      // borderRadius: appStyles.sizeSet.radius,
      padding: 10,
      marginTop: 80,
      ...appStyles.shadows.buttonShadow
    },
    signupText: {
      color: appStyles.colorSet[colorScheme].mainTextColor,
    },
    image: {
      width: '100%',
      height: '100%',
    },
    imageBlock: {
      flex: 2,
      flexDirection: 'row',
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
    imageContainer: {
      height: imageSize,
      width: imageSize,
      borderRadius: imageSize,
      shadowColor: '#006',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.1,
      overflow: 'hidden',
    },
    formContainer: {
      width: '100%',
      flex: 4,
      alignItems: 'center',
    },
    photo: {
      marginTop: imageSize * 0.77,
      marginLeft: -imageSize * 0.29,
      width: photoIconSize,
      height: photoIconSize,
      borderRadius: photoIconSize,
    },

    subHeadingText: {
      color: appStyles.colorSet[colorScheme].mainTextColor,
      fontSize: 18,
      marginTop: 30,
      alignSelf: "center"
    },

    addButton: {
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#d9d9d9',
      opacity: 0.8,
      zIndex: 2,
    },
    orTextStyle: {
      color: 'black',
      marginTop: 20,
      marginBottom: 10,
      alignSelf: 'center',
      color: appStyles.colorSet[colorScheme].mainTextColor,
    },
    PhoneNumberContainer: {
      marginTop: 10,
      marginBottom: 10,
      alignSelf: 'center',
    },
    smsText: {
      color: '#4267b2',
    },
    tos: {
      marginTop: 40,
      alignItems: 'center',
      justifyContent: 'center',
      height: 30,
    },
    interest: {
      backgroundColor: "white", 
      height: 35, 
      marginTop: 20, 
      marginHorizontal: 10,
      alignItems: "center",
      justifyContent: "center",
      borderRadius: 6,
      borderWidth: 1,
    },
    titleSmaller: {
      fontSize: 20,
      fontWeight: '500',
      color: appStyles.colorSet[colorScheme].mainTextColor,
      alignSelf: 'center',
      textAlign: 'center',
      marginBottom: 10,
    },
    titleSmaller2: {
      fontSize: 18,
      fontWeight: '400',
      color: appStyles.colorSet[colorScheme].mainTextColor,
      alignSelf: 'center',
      textAlign: 'center',
      marginTop: 30,
      marginBottom: 40,
      marginHorizontal: 10,
    },
    nextBtn: {
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
      height: 40,
      alignItems: "center",
      justifyContent: "center",
      // borderRadius: 8,
      marginTop: 20,
      width: "100%",
      ...appStyles.shadows.buttonShadow
    },
    nextText: {
      fontSize: 20,
      // fontWeight: "bold",
      color: appStyles.colorSet[colorScheme].mainTextColor,
      marginHorizontal: 10,
    },
    trafficLight: {
      width: "80%",
      height: 50,
      backgroundColor: "white",
      marginTop: 30,
      alignItems: "center",
      justifyContent: "center",
      borderRadius: 18,
      // borderColor: appStyles.colorSet[colorScheme].mainTextColor,
      // shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 0,
      },
      shadowOpacity: 1.0,
      shadowRadius: 12,

      elevation: 40,
    },
    trafficLightText: {
      fontSize: 16,
      color: appStyles.colorSet[colorScheme].mainTextColor,
      opacity: 0.8
      // fontWeight: "bold"
    },
    trafficLightBox: {
      marginTop: 20,
      marginBottom: 10,
      height: 200,
      width: 100,
      alignSelf: "center",
      borderWidth: 3,
      borderColor: appStyles.colorSet[colorScheme].mainTextColor,
      borderRadius: 4,
      alignItems: "center",
      justifyContent: "center",
    },
    circle: {
      width: 60,
      height: 60,
      borderRadius: 30,
      borderWidth: 3,
      marginTop: 2,
      borderColor: appStyles.colorSet[colorScheme].mainTextColor,
    },
    uniInfoTitle: {
      marginTop: 10,
      fontSize: 20,
      color: appStyles.colorSet[colorScheme].mainTextColor
      // width: "90%",
      // textAlign: "center"

    },
    
    uniName: {
      fontSize: 18, 
      fontWeight: "400",
    },

    uniInfoTitleView: {
      paddingTop: 50,
      paddingBottom: 20,
      // width: "100%",
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
      alignItems: "center",
      flexDirection: "row",
      justifyContent: "space-between"
    },
    uniInfoContainer: {
      flex: 1,
      // alignItems: 'center',
    },
    uniItem: {
      justifyContent: "flex-start", 
      opacity: 0.6,
      borderBottomColor: "black",
      borderBottomWidth: 0.2,
      justifyContent: "center"
    },
    searchContainer: {
      height: 42,
      borderWidth: 1,
      borderColor: appStyles.colorSet[colorScheme].grey3,
      paddingLeft: 20,
      width: '98%',
      alignSelf: 'center',
      marginVertical: 8,
      alignItems: 'center',
      borderRadius: 8,
    },
    
  });
};

export default dynamicStyles;
