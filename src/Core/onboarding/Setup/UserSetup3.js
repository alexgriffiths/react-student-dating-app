import React, { useEffect, useState } from 'react';
import {
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Image,
  ScrollView
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import dynamicStyles from './styles';
import { useColorScheme } from 'react-native-appearance';
import { firebase } from '../../firebase/config';
import TNActivityIndicator from '../../truly-native/TNActivityIndicator';
import FadeInOut from 'react-native-fade-in-out';
import { firebaseUser } from '../../firebase';
import { setUserData } from '../../../Core/onboarding/redux/auth';

const UserSetup3 = (props) => {
  const appConfig = props.route.params.appConfig;
  const appStyles = props.route.params.appStyles;
  const authManager = props.route.params.authManager;
  const user = props.route.params.user;

  const colorScheme = useColorScheme();
  const styles = dynamicStyles(appStyles, colorScheme);

  const dispatch = useDispatch();

  const [interests, setInterests] = useState([]);
  const [threeSelected, setThreeSelected] = useState([]);

  const interestRef = firebase.firestore().collection("interests");
  // const userRef = firebase.firestore().collection("users").doc(user.id);

  const [showNext, setShowNext] = useState(false);

  useEffect(() => {
    const newInterests = [];
    try {
      interestRef.get().then((interestsSnapshot) => {
            interestsSnapshot.forEach((docSnapshot) => {
              const values = docSnapshot.data().interests.values();
              var index = 0;
              for (let interestVal of values) {
                newInterests.push({id: index, title: interestVal.toString(), checked: false});
                index++;
              }
            });
          setInterests(newInterests);
        });
    } catch (error) {
        console.log(error);
    }
  }, []);

  const onNextPressed = () => {
    var interestNames = []
    for (var i = 0; i < interests.length; i++) {
      const interest = interests[i];
      if (interest.checked) {
        interestNames.push(interest.title);
      }
    }

    let newUser = { ...user, interests: interestNames};
    firebaseUser.updateUserData(user.id, user.cityId, newUser);
    updateUserInfo(newUser);

    props.navigation.reset({
      index: 0,
      routes: [{ name: 'MainStack', params: { user: user } }],
    });

    // userRef.update({
    //   interests: interestNames
    // }).then(() => {
    //   props.navigation.reset({
    //     index: 0,
    //     routes: [{ name: 'MainStack', params: { user: user } }],
    //   });
    // })
  }

  const updateUserInfo = (newUser) => {
    dispatch(setUserData({ user: newUser}));
  };

    const interestClicked = (index) => {
      var tempArray = [...threeSelected];
      var newInterests = [...interests];

      if (interests[index].checked) {
        tempArray.splice(tempArray.indexOf(index), 1)
      } else {
        if (tempArray.length >= 3) {
          newInterests[tempArray.pop()].checked = false;
        } 
        tempArray.push(index);
      }

      newInterests[index].checked = !interests[index].checked;

      setThreeSelected(tempArray);
      setInterests(newInterests);

      if (tempArray.length == 3) {
        setShowNext(true);
      } else {
        setShowNext(false);
      }
    }

    const renderInterests = () => {
        return interests.map((interest, index) => {
          const color = interest.checked ? "grey": "white";
          return (
            <View style={{...styles.interest, backgroundColor: color}}> 
              <TouchableOpacity 
                onPress={() => interestClicked(index)}
                style={{margin: 5}}>
                <Text>{interest.title}</Text>
              </TouchableOpacity>
            </View>
          );
        })
    }

  return (
    <View style={styles.container}>
      
        <View style={{ flex: 1, width: '100%' }}>
            <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <Image
                style={appStyles.styleSet.backArrowStyle}
                source={appStyles.iconSet.backArrow}
            />
            </TouchableOpacity>
            <Text style={styles.title}>{'Your Interests'}</Text>
            <ScrollView contentContainerStyle={{flexDirection: "row", alignItems: "flex-start", flexWrap: "wrap", width: "80%", marginLeft: 10}}>
                {renderInterests()}
            </ScrollView>
            <View style={{alignItems: "center", marginBottom: 50, marginTop: 10, width: "100%"}}>
              <Text style={styles.titleSmaller}>{threeSelected.length > 0 ? threeSelected.length + " / 3 selected" : "Please select 3"}</Text>
              <FadeInOut style={{width: "100%"}} visible={showNext}>
                <TouchableOpacity 
                  style={styles.nextBtn}
                  onPress={() => onNextPressed()}>
                  <Text style={styles.nextText}>Next</Text>
                </TouchableOpacity>
              </FadeInOut>
            </View>

        </View>
        {(interests === undefined || interests.length == 0) && <TNActivityIndicator appStyles={appStyles} />}

    </View>
  );
};

export default UserSetup3;
