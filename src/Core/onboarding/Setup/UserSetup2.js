import React, { useState } from 'react';
import {
  Alert,
  Image,
  Keyboard,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Switch,
  StyleSheet,
  Modal
} from 'react-native';
import Button from 'react-native-button';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import SwitchSelector from "react-native-switch-selector";
import { modedColor } from '../../helpers/colors';
import TNColor from '../../truly-native/TNColor';

import dynamicStyles from './styles';
import { useColorScheme } from 'react-native-appearance';
import { IMLocalized } from '../../localization/IMLocalization';
import { connect } from 'react-redux';
import { firebaseUser } from '../../firebase';
import { setUserData } from '../../../Core/onboarding/redux/auth';
import { useSelector, useDispatch } from 'react-redux';
import FadeInOut from 'react-native-fade-in-out';
import CourseSelect from './CourseSelect';
import RNPickerSelect from 'react-native-picker-select';


const UserSetup2 = (props) => {
  const appConfig = props.route.params.appConfig;
  const appStyles = props.route.params.appStyles;
  const authManager = props.route.params.authManager;
  const user = props.route.params.user;

  const colorScheme = useColorScheme();
  const styles = dynamicStyles(appStyles, colorScheme);

  const [aboutYou, setAboutYou] = useState('');
  const [studying, setStudying] = useState('');
  const [year, setYear] = useState('1st Year');
  const [onCampus, setOnCampus] = useState('yes');

  const [showCourseSelect, setShowCourseSelect] = useState(false);

  const dispatch = useDispatch();


  const onRegister = () => {

    if (aboutYou == "") {
      Alert.alert(
        'About You',
        IMLocalized(
          'Write something interesting about yourself to continue',
        ),
        [{ text: IMLocalized('OK') }],
        {
          cancelable: false,
        },
      );
      return;
    }

    if (studying == "") {
      Alert.alert(
        'Your course',
        IMLocalized(
          'Please fill out what course you study to continue',
        ),
        [{ text: IMLocalized('OK') }],
        {
          cancelable: false,
        },
      );
      return;
    }

    if (year == "What year are you in?") {
      lert.alert(
        'Your year',
        IMLocalized(
          "Please choose what year you're in to continue",
        ),
        [{ text: IMLocalized('OK') }],
        {
          cancelable: false,
        },
      );
      return;
    }
 
    var tempProfile = {};
    tempProfile['about'] = aboutYou;
    tempProfile['studying'] = studying;
    tempProfile['year'] = year;
    tempProfile['on_campus'] = onCampus;
    let newUser = { ...user, profile: tempProfile};
    firebaseUser.updateUserData(user.id, user.cityId, newUser);
    updateUserInfo(newUser);

    props.navigation.navigate('LoginStack', {
      screen: 'UserSetup3',
      params: {
        appStyles,
        appConfig,
        authManager,
        user: newUser
      },
  });

    // props.navigation.reset({
    //   index: 0,
    //   routes: [{ name: 'MainStack', params: { user: user } }],
    // });


  };

  const updateUserInfo = (newUser) => {
    dispatch(setUserData({ user: newUser}));
  };



  const yesno = [
    { label: "Yes", value: "yes" },
    { label: "No", value: "no" },
  ];
  const updateUserDetails = () => {
    return (
      <>
      
        <TextInput 
            style={styles.InputContainer2}
            placeholder={IMLocalized('Write something about yourself')}
            placeholderTextColor="#aaaaaa"
            onChangeText={(text) => setAboutYou(text)}
            value={aboutYou}
            multiline={true}
            maxLength={100}
            width={"90%"}
        />

        <TextInput 
          style={styles.InputContainer}
          placeholder={IMLocalized('What are you studying?')}
          placeholderTextColor="#aaaaaa"
          onChangeText={(text) => setStudying(text)}
          value={studying}
          multiline={false}
          maxLength={40}
          width={"90%"}
          onPress={() => setShowCourseSelect(true)}
          editable={false}
        />

        {/* <TextInput 
          style={styles.InputContainer}
          placeholder={IMLocalized('What year are you in?')}
          placeholderTextColor="#aaaaaa"
          onChangeText={(text) => setYear(text)}
          value={year}
          multiline={false}
          maxLength={40}
          width={"90%"}
        /> */}

      <View style={styles.InputContainer3}>
        <RNPickerSelect
          color={"white"}
          onValueChange={(value) => setYear(value)}
          placeholder={{
            label: 'What year are you in?',
            // value: year,
          }}
          itemStyle={{
            color: modedColor(
              appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
              TNColor('#e0e0e0'),
            )
          }}
          items={[
            { label: "1st Year", value: "1st Year" },
            { label: "2nd Year", value: "2nd Year" },
            { label: "3rd Year", value: "3rd Year" },
            { label: "4th Year", value: "4th Year" },
            { label: "5th Year", value: "5th Year" },
            { label: "6th Year", value: "6th Year" },
            { label: "Masters Student", value: "Masters Student" },
            { label: "PhD", value: "PhD" },
        
          ]}
        />
      </View>

        {/* <Text style={styles.subHeadingText}>Are You On Campus?</Text>
        <SwitchSelector
            style={{marginTop:30, width: "80%", alignSelf: "center"}}
            options={yesno}
            buttonColor={"#66D3F0"} 
            initial={0}
            onPress={value => setOnCampus(value)}
        /> */}

        <FadeInOut visible={year !== null && studying.trim() && aboutYou.trim()}>
          <Button
            containerStyle={styles.signupContainer}
            style={styles.signupText}
            onPress={() => onRegister()}>
            {IMLocalized('Next')}
          </Button>
        </FadeInOut>
      </>
    );
  };

  return (
    <View style={styles.container}>

        <Modal
          visible={showCourseSelect}
          animationType={"slide"}>
            <CourseSelect appStyles={appStyles} setCourse={setStudying} closeModal={() => setShowCourseSelect(false)}/>
        </Modal>

      <KeyboardAwareScrollView
        style={{ flex: 1, width: '100%' }}>
        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <Image
            style={appStyles.styleSet.backArrowStyle}
            source={appStyles.iconSet.backArrow}
          />
        </TouchableOpacity>
        <Text style={styles.title}>Your Profile</Text>
        {updateUserDetails()}
        
      </KeyboardAwareScrollView>
    </View>
  );
};

export default UserSetup2;
