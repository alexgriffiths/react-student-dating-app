import React, { useState } from 'react';
import {
  Alert,
  Linking,
  Image,
  Keyboard,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Button from 'react-native-button';
import appleAuth, {
  AppleButton,
} from '@invertase/react-native-apple-authentication';
import { connect } from 'react-redux';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import TNActivityIndicator from '../../truly-native/TNActivityIndicator';
import { IMLocalized } from '../../localization/IMLocalization';
import dynamicStyles from './styles';
import { useColorScheme } from 'react-native-appearance';
import { setUserData } from '../redux/auth';
import { localizedErrorMessage } from '../utils/ErrorCode';
import { firebase } from '../../firebase/config';
import FadeInOut from 'react-native-fade-in-out';

const LoginScreen = (props) => {
  const appConfig = props.route.params.appConfig;
  const authManager = props.route.params.authManager;

  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const appStyles = props.route.params.appStyles;
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(appStyles, colorScheme);
 
  const onPressLogin = () => {
    setLoading(true);
    authManager
      .loginWithEmailAndPassword(
        email && email.trim(),
        password && password.trim(),
        appConfig,
      )
      .then((response) => {
        setLoading(false);
        if (response?.user) {
          const user = response.user;
          let verified = firebase.auth().currentUser.emailVerified;
          const userRef =  firebase.firestore().collection('main_data')
            .doc(user.cityId)
            .collection('users')
            .doc(user.userID);

          userRef.get().then((user) => {
            const userManuallyVerified = user.data().manuallyVerified;
            
            if (verified || userManuallyVerified) {
            
              props.setUserData({
                user: response.user,
              });
              Keyboard.dismiss();

              const incompleteSettings = user.data().settings == null || user.data().settings == undefined;
              const incompleteProfile = user.data().profile == null || user.data().profile == undefined;
              const incompleteInterests = user.data().interests == null || user.data().interests == undefined;
              const incompleteTrafficLights = user.data().settings.traffic_light == null || user.data().settings.traffic_light == undefined;
              
              let nextScreenName = "";
              let signUpOk = true;

              // Check if user has completed sign up.
              if (incompleteInterests) {
                nextScreenName = "UserSetup3";
                signUpOk = false;
              } 
              if (incompleteProfile) {
                nextScreenName = "UserSetup2";
                signUpOk = false;
              } 
              if (incompleteTrafficLights) {
                nextScreenName = "TrafficLight";
                signUpOk = false;
              }
              if (incompleteSettings) {
                nextScreenName = "UserSetup";
                signUpOk = false;
              }
              
              if (signUpOk) {
                props.navigation.reset({
                  index: 0,
                  routes: [{ name: 'MainStack', params: { user: user } }],
                });
              } else {
                props.navigation.navigate('LoginStack', {
                  screen: nextScreenName,
                  params: {
                    appStyles,
                    appConfig,
                    authManager,
                    user: response.user
                  },
                });
              }

            } else {
              Alert.alert(
                'Account not verified',
                IMLocalized(
                  'Please click the link in the email we sent you to verify your account!',
                ),
                [
                  {
                    text: "Need Help?",
                    onPress: () => Linking.openURL('mailto:contact@uni-dating.com')
                  },  
                  { 
                    text: IMLocalized('OK'),
                  //   onPress: () =>  props.navigation.navigate('LoginStack', {
                  //     screen: 'Welcome',
                  // })
                  }
                ],
                {
                  cancelable: false,
                },
              );
            }

          }).catch((error) => {
            console.log(error);
          });





        } else {
          setLoading(false);
          Alert.alert(
            '',
            localizedErrorMessage(response.error),
            [{ text: IMLocalized('OK') }],
            {
              cancelable: false,
            },
          );
        }
      });
  };

  const onForgotPassword = async () => {
    props.navigation.push('ResetPassword', {
      isResetPassword: true,
      appStyles,
      appConfig,
    });
  };

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView
        style={{ flex: 1, width: '100%' }}>
        <TouchableOpacity
          style={{ alignSelf: 'flex-start' }}
          onPress={() => props.navigation.goBack()}>
          <Image
            style={appStyles.styleSet.backArrowStyle}
            source={appStyles.iconSet.backArrow}
          />
        </TouchableOpacity>
        <Text style={styles.title}>{IMLocalized('Sign In')}</Text>
        <TextInput
          style={styles.InputContainer}
          placeholder={IMLocalized('E-mail')}
          placeholderTextColor="#aaaaaa"
          onChangeText={(text) => setEmail(text)}
          value={email}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <TextInput
          style={styles.InputContainer}
          placeholderTextColor="#aaaaaa"
          secureTextEntry
          placeholder={IMLocalized('Password')}
          onChangeText={(text) => setPassword(text)}
          value={password}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <View style={styles.forgotPasswordContainer}>
          <Button
            style={styles.forgotPasswordText}
            onPress={() => onForgotPassword()}>
            {IMLocalized('Forgot password?')}
          </Button>
        </View>
        <FadeInOut visible={email.trim() && password.trim()}>
          <Button
            containerStyle={styles.loginContainer}
            style={styles.loginText}
            onPress={() => onPressLogin()}>
            {IMLocalized('Log In')}
          </Button>
        </FadeInOut>

        {loading && <TNActivityIndicator appStyles={appStyles} />}
      </KeyboardAwareScrollView>
    </View>
  );
};

export default connect(null, {
  setUserData,
})(LoginScreen);
