import { I18nManager } from 'react-native';
import { StyleSheet } from 'react-native';
import { modedColor } from '../../helpers/colors';
import TNColor from '../../truly-native/TNColor';

const dynamicStyles = (appStyles, colorScheme) => {
  return StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    orTextStyle: {
      color: appStyles.colorSet[colorScheme].mainTextColor,
      marginTop: 40,
      marginBottom: 10,
      alignSelf: 'center',
    },
    title: {
      fontSize: 30,
      fontWeight: appStyles.boldness.bigTitle,
      color: appStyles.colorSet[colorScheme].mainTextColor,
      marginVertical: 30,
      marginTop: 100,
      // marginBottom: 20,
      alignSelf: 'stretch',
      textAlign: 'left',
      // marginLeft: 30,
      alignSelf: "center",
    },
    loginContainer: {
      width: '100%',
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
      // borderRadius: 25,
      padding: 10,
      marginTop: 40,
      alignSelf: 'center',
      ...appStyles.shadows.buttonShadow
    },
    loginText: {
      color: appStyles.colorSet[colorScheme].mainTextColor,
    },
    placeholder: {
      color: 'red',
    },
    InputContainer: {
      height: 42,
      borderWidth: 1,
      borderColor: appStyles.colorSet[colorScheme].grey3,
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
      paddingLeft: 20,
      color: appStyles.colorSet[colorScheme].mainTextColor,
      width: '80%',
      alignSelf: 'center',
      marginTop: 20,
      alignItems: 'center',
      borderRadius: appStyles.roundness.textInputRadius,
      textAlign: I18nManager.isRTL ? 'right' : 'left',
      ...appStyles.shadows.buttonShadow
    },

    facebookContainer: {
      width: '70%',
      backgroundColor: '#4267B2',
      borderRadius: 25,
      padding: 10,
      marginTop: 30,
      alignSelf: 'center',
    },
    appleButtonContainer: {
      width: '70%',
      height: 40,
      marginTop: 16,
      alignSelf: 'center',
    },
    facebookText: {
      color: '#ffffff',
      fontSize: 14,
    },
    phoneNumberContainer: {
      marginTop: 20,
    },
    forgotPasswordContainer: {
      // width: '80%',
      marginTop: 10,
      alignSelf: 'center',
      alignItems: 'flex-end',
    },
    forgotPasswordText: {
      fontSize: 14,
      padding: 4,
    },
  });
};

export default dynamicStyles;
