import { StyleSheet, Dimensions, I18nManager } from 'react-native';

const dynamicStyles = (appStyles, colorScheme) => {
  return StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
      },
      title: {
        marginTop: 80,
        fontSize: 30,
        fontWeight: '500',
        color: appStyles.colorSet[colorScheme].mainTextColor,
        // marginTop:-10,
        marginBottom: 20,
        alignSelf: 'stretch',
        textAlign: 'center',
      },
      titleSmaller2: {
        fontSize: 18,
        fontWeight: '400',
        color: appStyles.colorSet[colorScheme].mainTextColor,
        alignSelf: 'center',
        textAlign: 'center',
        marginTop: 20,
        marginBottom: 40,
        marginHorizontal: 10,
      },
  
      content: {
        paddingLeft: 50,
        paddingRight: 50,
        textAlign: 'center',
        fontSize: appStyles.fontSet.middle,
        color: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
      },
    nextBtn: {
        backgroundColor: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        // borderRadius: 8,
        marginTop: 20,
        width: "100%",
        ...appStyles.shadows.buttonShadow
      },
      nextText: {
        fontSize: 20,
        // fontWeight: "bold",
        color: appStyles.colorSet[colorScheme].mainTextColor,
        marginHorizontal: 10,
      },
      trafficLight: {
        width: "80%",
        height: 50,
        backgroundColor: "white",
        marginTop: 30,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 18,
        // borderColor: appStyles.colorSet[colorScheme].mainTextColor,
        // shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 0,
        },
        shadowOpacity: 1.0,
        shadowRadius: 12,
  
        elevation: 40,
      },
      trafficLightText: {
        fontSize: 16,
        color: appStyles.colorSet[colorScheme].mainTextColor,
        opacity: 0.8
        // fontWeight: "bold"
      },
      trafficLightBox: {
        marginTop: 20,
        marginBottom: 10,
        height: 200,
        width: 100,
        alignSelf: "center",
        borderWidth: 3,
        borderColor: appStyles.colorSet[colorScheme].mainTextColor,
        borderRadius: 4,
        alignItems: "center",
        justifyContent: "center",
      }
  });
};

export default dynamicStyles;
