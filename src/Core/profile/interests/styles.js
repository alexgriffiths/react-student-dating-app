import { StyleSheet, Dimensions, I18nManager } from 'react-native';

const dynamicStyles = (appStyles, colorScheme) => {
  return StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    title: {
      fontSize: 30,
      fontWeight: 'bold',
      color: appStyles.colorSet[colorScheme].mainTextColor,
      marginTop: 25,
      marginBottom: 10,
      alignSelf: 'stretch',
      textAlign: 'left',
      marginLeft: 35,
    },

    content: {
      paddingLeft: 50,
      paddingRight: 50,
      textAlign: 'center',
      fontSize: appStyles.fontSet.middle,
      color: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
    },
    interest: {
      backgroundColor: "white", 
      height: 35, 
      marginTop: 20, 
      marginHorizontal: 10,
      alignItems: "center",
      justifyContent: "center",
      borderRadius: 6,
      borderWidth: 1,
    },
    titleSmaller: {
      fontSize: 20,
      fontWeight: 'bold',
      color: appStyles.colorSet[colorScheme].mainTextColor,
      alignSelf: 'center',
      textAlign: 'center',
      marginBottom: 10,
    },
    nextBtn: {
      backgroundColor: appStyles.colorSet[colorScheme].mainThemeForegroundColor,
      height: 40,
      alignItems: "center",
      justifyContent: "center",
      marginTop: 20,
      width: "100%",
      ...appStyles.shadows.buttonShadow

    },
    nextText: {
      fontSize: 18,
      fontWeight: "bold",
      color: appStyles.colorSet[colorScheme].mainTextColor,
      marginHorizontal: 10,
    }
  });
};

export default dynamicStyles;
