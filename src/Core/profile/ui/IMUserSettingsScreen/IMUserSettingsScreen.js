import React, { Component } from 'react';
import { BackHandler, View, TouchableOpacity, Text } from 'react-native';
import { connect } from 'react-redux';
import { IMLocalized } from '../../../localization/IMLocalization';
import { setUserData, settingsChanged } from '../../../onboarding/redux/auth';
import { firebaseUser } from '../../../firebase';
import IMFormComponent from '../IMFormComponent/IMFormComponent';
import { Appearance } from 'react-native-appearance';

class IMUserSettingsScreen extends Component {
  constructor(props) {
    super(props);
    this.appStyles = props.appStyles;
    let screenTitle = props.screenTitle || IMLocalized('Settings');
    let COLOR_SCHEME = Appearance.getColorScheme();
    let currentTheme = this.appStyles.navThemeConstants[COLOR_SCHEME];

    this.form = props.form;
    this.initialValuesDict = props.user.settings || {};

    this.state = {
      form: props.form,
      alteredFormDict: {},
    };
  }


  componentWillUnmount() {
    this.didFocusSubscription && this.didFocusSubscription();
    this.willBlurSubscription && this.willBlurSubscription();
  }


  onFormSubmit = () => {
    const user = this.props.user;
    var newSettings = user.settings || {};
    const form = this.form;
    const alteredFormDict = this.state.alteredFormDict;

    form.sections.forEach((section) => {
      section.fields.forEach((field) => {
        const newValue = alteredFormDict[field.key];
        if (newValue != null) {
          newSettings[field.key] = alteredFormDict[field.key];
        }
      });
    });

    let newUser = { ...user, settings: newSettings };
    firebaseUser.updateUserData(user.id, user.cityId, newUser);
    this.props.setUserData({ user: newUser });
    this.props.settingsChanged(true);
    // this.props.navigation.goBack();
  };

  onFormChange = (alteredFormDict) => {
    this.setState({ alteredFormDict });
  };

  onFormButtonPress = (buttonField) => {
    this.onFormSubmit();
  };

  render() {
    return (

      <View style={{flex: 1, width: "100%", height: "100%", alignItems: "center"}}>
        <View style={{alignItems: "center", marginTop: 10}}>
          <Text style={{color: "#464646", fontSize: 26, fontWeight: "bold"}}>
            Settings
          </Text>
        </View>
        
        <View style={{marginTop: 10, height: "60%", width: "100%", marginBottom: 250}}>
          <IMFormComponent
            form={this.form}
            initialValuesDict={this.initialValuesDict}
            onFormChange={this.onFormChange}
            navigation={this.props.navigation}
            appStyles={this.appStyles}
          />
        </View>
        <TouchableOpacity onPress={() => { this.onFormSubmit(); this.props.hideModal()} }  style={{width: "60%", height: 40, alignItems: "center", borderRadius: 4, justifyContent: "center", backgroundColor: "white", marginTop: 10}}>
          <Text style={{color:"#464646", fontSize: 18, fontWeight: "bold"}}>
            Save
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  return {
    user: auth.user,
  };
};

export default connect(mapStateToProps, { setUserData, settingsChanged })(IMUserSettingsScreen);
