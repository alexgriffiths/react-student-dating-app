import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { BackHandler, View, Button, Text, Appearance, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import TextButton from 'react-native-button';
import { firebaseUser } from '../../../firebase';
import IMFormComponent from '../IMFormComponent/IMFormComponent';
import { setUserData } from '../../../onboarding/redux/auth';
import { IMLocalized } from '../../../localization/IMLocalization';
// import { Appearance } from 'react-native-appearance';
import { useColorScheme } from 'react-native-appearance';
import dynamicStyles from './styles';

class IMEditProfileScreen extends Component {
  constructor(props) {
    super(props);
    // console.log(this.appStyles);
    // let screenTitle = props.screenTitle;
    // let COLOR_SCHEME = Appearance.getColorScheme();
    // let currentTheme = this.appStyles.navThemeConstants[COLOR_SCHEME];
    
    this.form = props.form;
    this.onComplete = props.onComplete;
    this.state = {
      form: props.form,
      alteredFormDict: {},
    };
    // this.didFocusSubscription = props.navigation.addListener(
    //   'focus',
    //   (payload) =>
    //     BackHandler.addEventListener(
    //       'hardwareBackPress',
    //       this.onBackButtonPressAndroid,
    //     ),
    // );
  }

  // componentDidMount() {
  //   this.willBlurSubscription = this.props.navigation.addListener(
  //     'beforeRemove',
  //     (payload) =>
  //       BackHandler.removeEventListener(
  //         'hardwareBackPress',
  //         this.onBackButtonPressAndroid,
  //       ),
  //   );
  // }

  componentWillUnmount() {
    this.didFocusSubscription && this.didFocusSubscription();
    this.willBlurSubscription && this.willBlurSubscription();
  }

  // onBackButtonPressAndroid = () => {
  //   this.props.navigation.goBack();
  //   return true;
  // };

  isInvalid = (value, regex) => {
    const regexResult = regex.test(value);

    if (value.length > 0 && !regexResult) {
      return true;
    }
    if (value.length > 0 && regexResult) {
      return false;
    }
  };

  onFormSubmit = () => {
    var newUser = this.props.user;
    const form = this.form;
    const alteredFormDict = this.state.alteredFormDict;
    var allFieldsAreValid = true;

    form.sections.forEach((section) => {
      section.fields.forEach((field) => {
        const newValue = alteredFormDict[field.key]?.trim();

        if (newValue != null) {
          if (field.regex && this.isInvalid(newValue, field.regex)) {
            allFieldsAreValid = false;
          } else {
            newUser["profile"][field.key] = alteredFormDict[field.key]?.trim();
          }
        }
      });
    });

    if (allFieldsAreValid) {
      firebaseUser.updateUserData(this.props.user.id, this.props.user.cityId, newUser);
      this.props.setUserData({ user: newUser });
      // this.props.navigation.goBack();
      if (this.onComplete) {        
        this.onComplete();
      }
    } else {
      alert(
        IMLocalized(
          'An error occurred while trying to update your account. Please make sure all fields are valid.',
        ),
      );
    }
  };

  onFormChange = (alteredFormDict) => {
    this.setState({ alteredFormDict });
  };

  render() {
    return (
      <View style={{flex: 1, width: "100%", height: "100%", alignItems: "center"}}>
        <View style={{alignItems: "center", marginTop: 10}}>
          <Text style={{color: "#464646", fontSize: 26, fontWeight: "bold"}}>
            Account Details
          </Text>
        </View>
        
        <View style={{marginTop: 10, height: "60%", width: "100%", marginBottom: 200}}>
          <IMFormComponent
            form={this.form}
            initialValuesDict={Object.assign({}, this.props.user, this.props.user.profile) }
            onFormChange={this.onFormChange}
            navigation={this.props.navigation}
            appStyles={this.props.appStyles}
          />
        </View>
        <TouchableOpacity onPress={() => { this.onFormSubmit(); this.props.hideModal()} }  style={{width: "60%", height: 40, alignItems: "center", borderRadius: 4, justifyContent: "center", backgroundColor: "white", marginTop: 10}}>
          <Text style={{color:"#464646", fontSize: 18, fontWeight: "bold"}}>
            Save
          </Text>
        </TouchableOpacity>
      </View>

    );
  }
}

IMEditProfileScreen.propTypes = {
  user: PropTypes.object,
  setUserData: PropTypes.func,
};

const mapStateToProps = ({ auth }) => {
  return {
    user: auth.user,
  };
};

export default connect(mapStateToProps, { setUserData })(IMEditProfileScreen);
