import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import PropTypes from 'prop-types';
import { size } from '../../helpers/devices';
import * as Statics from '../../helpers/statics';
import FastImage from 'react-native-fast-image';
import { BlurView } from "@react-native-community/blur";
import LinearGradient from 'react-native-linear-gradient';
import AppStyles from '../../AppStyles';
import { DEVICE_HEIGHT } from '../../helpers/statics';

const TinderCard = (props) => {
  const { url, name, age, school, course, year, interests, trafficLight, uni } = props;

  const renderInterests = () => {
    return interests.map((interest, index) => {
      const color = "white";
      return (
        <View style={{backgroundColor: "rgba(153, 153, 153, 0.4)", height: 30, marginRight: 5,alignItems: "center",justifyContent: "center",borderRadius: 6,borderWidth: 1, marginTop: 5}}> 
            <Text style={{margin: 5, color: "white"}}>{interest}</Text>
        </View>
      );
    })
  }

  const renderTrafficLights = () => {
    let redColor = "#ffffff00";
    let amberColor = "#ffffff00";
    let greenColor = "#ffffff00";

    if (trafficLight === "green") {
        greenColor = "#b4e6a7";
    } else if (trafficLight === "red") {
        redColor = "#ef8484";
    } else if (trafficLight === "amber") {
        amberColor = "#ffb57f";
    };

    return (
        <View style={styles.trafficLightBox}>
            <View style={{...styles.circle, backgroundColor: redColor}}>

            </View>
            <View style={{...styles.circle, backgroundColor: amberColor}}>

            </View>
            <View style={{...styles.circle, backgroundColor: greenColor}}>

            </View>
        </View>
    );
  };

  let shadowLight = "#ffffff00";

  if (trafficLight === "green") {
    shadowLight = "#b4e6a7";
  } else if (trafficLight === "red") {
    shadowLight = "#ef8484";
  } else if (trafficLight === "amber") {
    shadowLight = "#ffb57f";
  };

  return (
    <View style={{...styles.container, ...styles.cardStyle, shadowColor: shadowLight}}>
      <FastImage source={{ uri: url }} style={styles.news_image_style}>
        {/* <BlurView blurType={"light"} blurAmount={6} style={styles.name_info_container}> */}
        <LinearGradient
            colors={['transparent', 'white']}
            start = {{x: 0, y: 0}}
            end = {{x: 0, y: 1}}
            locations={[0.1, 0.7]}
            style={styles.name_info_container}
          >
          <View style={styles.userDetailContainer}> 
            <Text style={styles.name_style}>
              {name ? name : ' '}  
            </Text>

            {/* <View style={{flexDirection: "row", alignItems: "flex-start", flexWrap: "wrap", width: "100%"}}>
              {renderInterests()}
            </View> */}

            <View style={styles.txtBox}>
              <Text style={styles.label}>{course ? course : ' '}</Text>
            </View>
            <View style={styles.txtBox}>
              <Text style={styles.label}>{year ? year : ' '}</Text>
            </View>
            <View style={styles.txtBox}>
              <Text style={styles.label}>{uni ? uni : ' '}</Text>
            </View>
          </View>
          
          {/* {trafficLight && renderTrafficLights()} */}

          <View style={{...styles.backView, backgroundColor: shadowLight}}>
            <Image
              style={styles.backIcon}
              source={AppStyles.iconSet.arrowupIcon}
            />
          </View>



          {/* <View style={styles.undoIconContainer}>
            <TouchableOpacity
              onPress={props.undoSwipe}
              style={styles.roundUndoIconContainer}>
              <Image style={styles.icon} source={AppStyles.iconSet.undo} />
            </TouchableOpacity>
          </View> */}

        </LinearGradient>
      </FastImage>
    </View>
  );
};

TinderCard.propTypes = {
  school: PropTypes.string,
  url: PropTypes.string,
  name: PropTypes.string,
  age: PropTypes.string,
  distance: PropTypes.number,
  setShowMode: PropTypes.func,
};

const undoIconSize = size(20);
const undoIconContainerSize = undoIconSize + 8;
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  cardStyle: {
    position: 'absolute',
    top: 0,
    ...ifIphoneX(
      {
        bottom: 0,
      },
      {
        bottom: 65,
      },
    ),
    left: 0,
    right: 0,
    width: Statics.DEVICE_WIDTH,

    // shadowColor: "#b4e6a7",
    shadowOffset: {
      width: 0,
      height: -10,
    },
    shadowOpacity: 1,
    shadowRadius: 4,

    elevation: 10,
    zIndex: 0,
  },
  news_image_style: {
    width: Statics.DEVICE_WIDTH - size(4),
    height: Statics.DEVICE_HEIGHT * 0.66, // FIX ME ITS BAD
    flexDirection: 'column',
    justifyContent: 'flex-end',
    marginHorizontal: size(10),
    ...ifIphoneX(
      {
        marginTop: size(6),
      },
      {
        marginTop: 0,
      },
    ),
    borderRadius: 6,
    backgroundColor: 'white',

  },
  name_info_container: {
    padding: 100,
    flexDirection: 'row',
  },
  userDetailContainer: {
    flex: 4,
    top: 80,
    alignItems: "center",
  },
  undoIconContainer: {
    flex: 1, 
    position: "absolute",
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingBottom: 7,
  },
  roundUndoIconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: undoIconContainerSize + 7,
    width: undoIconContainerSize + 7,
    borderRadius: Math.floor(undoIconContainerSize + 7 / 2),
    backgroundColor: '#e95c6f',
    zIndex: 2,
  },
  name_style: {
    fontSize: 24,
    fontWeight: '500',
    color: "black",
    marginBottom: 0,
    backgroundColor: 'transparent',
  },
  txtBox: {
    marginTop: size(3),
    flexDirection: 'row',
  },
  icon: {
    width: size(20),
    height: size(20),
    tintColor: 'black',
  },
  label: {
    // paddingLeft: size(10),
    fontSize: 16,
    fontWeight: '400',
    color: 'black',
    backgroundColor: 'transparent',
  },
  detailBtn: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    // zIndex: 3000
  },
  backView: {
    position: 'absolute',
    top: DEVICE_HEIGHT * 0.21,
    right: 20,
    width: 55,
    height: 55,
    borderRadius: 27.5,
    // backgroundColor: '#ef8484',
    justifyContent: 'center',
    alignItems: 'center',
  },
  backIcon: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
    tintColor: 'white',
  },
  trafficLightBox: {
    height: 100,
    width: 50,
    alignSelf: "center",
    borderWidth: 3,
    borderColor: "black",
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(153, 153, 153, 0.4)"
  },
  circle: {
    width: 29,
    height: 29,
    borderRadius: 15,
    borderWidth: 3,
    marginTop: 2,
    borderColor: "black",
  }
});

export default TinderCard;
