import { Platform } from 'react-native';
import { IMLocalized, setI18nConfig } from './Core/localization/IMLocalization';

setI18nConfig();

const regexForNames = /^[a-zA-Z]{2,25}$/;
const regexForPhoneNumber = /\d{9}$/;
const regexForAge = /[0-9]/g;

const DatingConfig = {
  isSMSAuthEnabled: false,
  // appIdentifier: 'rn-dating-android',
  appIdentifier: 'uni-dating-main',
  onboardingConfig: {
    welcomeTitle: IMLocalized('Student exclusive social app'),
    welcomeCaption: IMLocalized(
      'Match and chat with verified students from your University.',
    ),
    uniName: IMLocalized('Brunel'),
    walkthroughScreens: [
      {
        icon: require('../assets/images/fire-icon.png'),
        title: 'Get a Date',
        description: IMLocalized(
          'Swipe right to get a match with people you like from your area.',
        ),
      },
      {
        icon: require('../assets/images/chat.png'),
        title: 'Private Messages',
        description: IMLocalized('Chat privately with people you match.'),
      },
      {
        icon: require('../assets/images/instagram.png'),
        title: 'Send Photos & Videos',
        description: IMLocalized(
          'Have fun with your matches by sending photos and videos to each other.',
        ),
      },
      {
        icon: require('../assets/images/notification.png'),
        title: 'Get Notified',
        description: IMLocalized(
          'Receive notifications when you get new messages and matches.',
        ),
      },
    ],
  },
  tosLink: 'https://uni-dating.com/privacy/',
  privacyPolicyLink: 'https://uni-dating.com/privacy/',
  editProfileFields: {
    sections: [
      {
        title: IMLocalized('PUBLIC PROFILE'),
        fields: [
          {
            displayName: IMLocalized('First Name'),
            type: 'text',
            editable: true,
            regex: regexForNames,
            key: 'firstName',
            placeholder: 'Your first name',
          },
          {
            displayName: IMLocalized('Last Name'),
            type: 'text',
            editable: true,
            regex: regexForNames,
            key: 'lastName',
            placeholder: 'Your last name',
          },
          // {
          //   displayName: IMLocalized('Age'),
          //   type: 'text',
          //   editable: true,
          //   regex: regexForAge,
          //   key: 'age',
          //   placeholder: 'Your age',
          // },
          {
            displayName: IMLocalized('About you'),
            type: 'text',
            editable: true,
            key: 'about',
            placeholder: 'About you',
          },
          // {
          //   displayName: IMLocalized('Course'),
          //   type: 'other',
          //   editable: true,
          //   key: 'studying',
          //   placeholder: 'Your course',
          // },
          {
            displayName: IMLocalized('Year'),
            type: 'select',
            options: ['1st Year', '2nd Year', '3rd Year', '4th Year', '5th Year', '6th Year', 'Masters Student', 'PhD'],
            displayOptions: [
              '1st Year', 
              '2nd Year', 
              '3rd Year', 
              '4th Year', 
              '5th Year', 
              '6th Year', 
              'Masters Student', 
              'PhD'
            ],
            editable: true,
            key: 'year',
            placeholder: 'Your year',
          },
          {
            displayName: IMLocalized('Your Location'),
            type: 'select',
            options: ['yes', 'no'],
            displayOptions: [
              'On Campus', 
              'Off Campus',
            ],
            editable: true,
            key: 'on_campus',
            placeholder: 'Your Location',
          },
          // {
          //   displayName: IMLocalized('School'),
          //   type: 'text',
          //   editable: true,
          //   key: 'school',
          //   placeholder: 'Your bio',
          // },
        ],
      },
      {
        title: IMLocalized('PRIVATE DETAILS'),
        fields: [
          {
            displayName: IMLocalized('E-mail Address'),
            type: 'text',
            editable: false,
            key: 'email',
            placeholder: 'Your email address',
          },
          // {
          //   displayName: IMLocalized('Phone Number'),
          //   type: 'text',
          //   editable: true,
          //   regex: regexForPhoneNumber,
          //   key: 'phone',
          //   placeholder: 'Your phone number',
          // },
        ],
      },
    ],
  },
  userSettingsFields: {
    sections: [
      {
        title: IMLocalized('DISCOVERY'),
        fields: [
          {
            displayName: IMLocalized('Show Me on Uni Lights'),
            type: 'switch',
            editable: true,
            key: 'show_me',
            value: true,
          },
          // {
          //   displayName: IMLocalized('Distance Radius'),
          //   type: 'select',
          //   options: ['5', '10', '15', '25', '50', '100', 'unlimited'],
          //   displayOptions: [
          //     '5 miles',
          //     '10 miles',
          //     '15 miles',
          //     '25 miles',
          //     '50 miles',
          //     '100 miles',
          //     'Unlimited',
          //   ],
          //   editable: true,
          //   key: 'distance_radius',
          //   value: 'Unlimited',
          // },
          {
            displayName: IMLocalized('Your Gender'),
            type: 'select',
            options: ['female', 'male', 'none'],
            displayOptions: ['Female', 'Male', 'None'],
            editable: false,
            key: 'gender',
            value: 'None',
          },
          {
            displayName: IMLocalized('Gender Preference'),
            type: 'select',
            options: ['female', 'male', 'all'],
            displayOptions: ['Female', 'Male', 'All'],
            editable: true,
            key: 'gender_preference',
            value: 'All',
          },
          // {
          //   displayName: IMLocalized('Looking For'),
          //   type: 'select',
          //   options: ['friends', 'fwb', 'relationship'],
          //   displayOptions: [
          //     'Friends', 'FWB', 'A Relationship'
          //   ],
          //   editable: true,
          //   key: 'want',
          //   placeholder: "What you're looking for",
          // },
        ],
      },
      {
        title: IMLocalized('PUSH NOTIFICATIONS'),
        fields: [
          {
            displayName: IMLocalized('New matches'),
            type: 'switch',
            editable: true,
            key: 'push_new_matches_enabled',
            value: true,
          },
          {
            displayName: IMLocalized('Messages'),
            type: 'switch',
            editable: true,
            key: 'push_new_messages_enabled',
            value: true,
          },
          // {
          //   displayName: IMLocalized('Super Likes'),
          //   type: 'switch',
          //   editable: true,
          //   key: 'push_super_likes_enabled',
          //   value: true,
          // },
          // {
          //   displayName: IMLocalized('Top Picks'),
          //   type: 'switch',
          //   editable: true,
          //   key: 'push_top_picks_enabled',
          //   value: true,
          // },
        ],
      },
      // {
      //   title: '',
      //   fields: [
      //     {
      //       displayName: IMLocalized('Save'),
      //       type: 'button',
      //       key: 'savebutton',
      //     },
      //   ],
      // },
    ],
  },
  contactUsFields: {
    sections: [
      {
        title: IMLocalized('CONTACT'),
        fields: [
          {
            displayName: IMLocalized('Address'),
            type: 'text',
            editable: false,
            key: 'push_notifications_enabled',
            value: '142 Steiner Street, San Francisco, CA, 94115',
          },
          {
            displayName: IMLocalized('E-mail us'),
            value: 'contact@uni-dating.com',
            type: 'text',
            editable: false,
            key: 'email',
            placeholder: 'Your email address',
          },
        ],
      },
      {
        title: '',
        fields: [
          {
            displayName: IMLocalized('Call Us'),
            type: 'button',
            key: 'savebutton',
          },
        ],
      },
    ],
  },
  dailySwipeLimit: 30,
  subscriptionSlideContents: [
    {
      title: IMLocalized("Uni Lights Pro"),
      description: IMLocalized(
        'Want unlimated swipes and to see who already likes you? Upgrade to Pro to unlock all features!',
      ),
      src: require('../assets/images/fencing.png'),
    },
    // {
    //   title: IMLocalized('Undo Actions'),
    //   description: IMLocalized('Get undo swipe actions when you subscribe.'),
    //   src: require('../assets/images/vip_1.png'),
    // },
    // {
    //   title: IMLocalized('Vip Badge'),
    //   description: IMLocalized(
    //     'Stand out with vip badge amongst other swipes when you subscribe',
    //   ),
    //   src: require('../assets/images/vip_2.png'),
    // },
    // {
    //   title: IMLocalized('Enjoy Unlimited Access'),
    //   description: IMLocalized(
    //     'Get unlimited app access and more features to come.',
    //   ),
    //   src: require('../assets/images/vip-pass.png'),
    // },
  ],
  contactUsPhoneNumber: '+16504859694',
  IAP_SHARED_SECRET: '',
  IAP_SKUS: Platform.select({
    ios: [
      '1MONTH',
      '3MONTH',
      '6MONTH',
    ],
    android: ['annual_vip_subscription', 'monthly_vip_subscription'],
  }),
  facebookIdentifier: '',
};

export default DatingConfig;
