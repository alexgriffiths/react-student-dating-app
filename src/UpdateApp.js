import React from 'react';
import { useState } from 'react';
import { 
    Text, 
    View, 
    TouchableOpacity, 
    Linking
} from 'react-native';

const UpdateApp = (props) => {
  return (
    <View style={{alignSelf: 'center', marginTop: 300, alignItems: "center", width: "90%"}}>
        <Text style={{textAlign: "center", fontSize: 22, fontWeight: 'bold', color: "#5c5c5c"}}>A New Update Is Available</Text>
        <Text style={{textAlign: "center", marginTop: 10, fontSize: 18, color: '#5c5c5c'}}>Please download the new version of Uni Lights to continue using the app.</Text>

        <TouchableOpacity 
            style={{width: 140, height: 50, backgroundColor: "#ef8484", alignItems: "center", justifyContent: "center", borderRadius: 8, marginTop: 30}}
            onPress={() => Linking.openURL('https://apps.apple.com/gb/app/id1576112611')}
        >
            <Text style={{color: "white", fontSize: 16}}>Update</Text>
        </TouchableOpacity>
    
    </View>
  );
};


export default UpdateApp;
