import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import React from 'react';
import {useState} from "react";
import {
  IMEditProfileScreen,
  IMUserSettingsScreen,
  IMContactUsScreen,
} from '../Core/profile';
import {
  View,
} from 'react-native';
import { IMChatScreen } from '../Core/chat';
import ConversationsScreen from '../screens/ConversationsScreen/ConversationsScreen';
import LoadScreen from '../Core/onboarding/LoadScreen/LoadScreen';
import SwipeScreen from '../screens/SwipeScreen/SwipeScreen';
import MyProfileScreen from '../screens/MyProfileScreen/MyProfileScreen';
import AddProfilePictureScreen from '../screens/AddProfilePictureScreen';
import LoginScreen from '../Core/onboarding/LoginScreen/LoginScreen';
import SignupScreen from '../Core/onboarding/SignupScreen/SignupScreen';
import WelcomeScreen from '../Core/onboarding/WelcomeScreen/WelcomeScreen';
import WalkthroughScreen from '../Core/onboarding/WalkthroughScreen/WalkthroughScreen';
import ResetPasswordScreen from '../Core/onboarding/ResetPasswordScreen/ResetPasswordScreen';
import SmsAuthenticationScreen from '../Core/onboarding/SmsAuthenticationScreen/SmsAuthenticationScreen';
import DynamicAppStyles from '../DynamicAppStyles';
import DatingConfig from '../DatingConfig';
import { NavigationContainer } from '@react-navigation/native';
import { TNTouchableIcon } from '../Core/truly-native';
import { useColorScheme } from 'react-native-appearance';
import authManager from '../Core/onboarding/utils/authManager';
import UserSetup from '../Core/onboarding/Setup/UserSetup';
import UserSetup2 from '../Core/onboarding/Setup/UserSetup2';
import UserSetup3 from '../Core/onboarding/Setup/UserSetup3';
import TrafficLight from '../Core/onboarding/Setup/TrafficLight';
import FeedsScreen from '../screens/FeedsScreen/FeedsScreen';
import DealsScreen from '../screens/DealsScreen/DealsScreen';
import VerifyEmailScreen from '../screens/VerifyEmailScreen/VerifyEmailScreen';
import IMCreateGroupScreen from '../Core/chat/ui/IMCreateGroupScreen/IMCreateGroupScreen';
import LikesScreen from '../screens/LikesScreen/LikesScreen';
import GroupChatScreen from '../screens/GroupChatScreen/GroupChatScreen';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const LoginStack = () => {
  let colorScheme = useColorScheme();
  let currentTheme = DynamicAppStyles.navThemeConstants[colorScheme];

  return (
    <Stack.Navigator
      initialRouteName="Welcome"
      screenOptions={{ headerShown: false, animationEnabled: true }}>
      <Stack.Screen
        initialParams={{
          appStyles: DynamicAppStyles,
          appConfig: DatingConfig,
          authManager: authManager,
        }}
        name="Welcome"
        component={WelcomeScreen}
      />
      <Stack.Screen name="Login" component={LoginScreen} />
      <Stack.Screen name="VerifyEmail" component={VerifyEmailScreen}/>
      <Stack.Screen name="Signup" component={SignupScreen} />
      <Stack.Screen name="Sms" component={SmsAuthenticationScreen} />
      <Stack.Screen name="ResetPassword" component={ResetPasswordScreen} />
      <Stack.Screen name="UserSetup" component={UserSetup} />
      <Stack.Screen name="UserSetup2" component={UserSetup2} />
      <Stack.Screen name="UserSetup3" component={UserSetup3} />
      <Stack.Screen name="TrafficLight" component={TrafficLight} />
    </Stack.Navigator>
  );
};

const MyProfileStack = () => {
  let colorScheme = useColorScheme();
  let currentTheme = DynamicAppStyles.navThemeConstants[colorScheme];

  return (
    <Stack.Navigator
      headerLayoutPreset="center"
      initialRouteName="MyProfile"
      screenOptions={{ headerShown: false, animationEnabled: false }}>
      <Stack.Screen
        initialParams={{ appStyles: DynamicAppStyles }}
        name="MyProfile"
        component={MyProfileScreen}
      />
      
    </Stack.Navigator>
  );
};

const ConversationsStack = () => {
  return (
    <Stack.Navigator
      headerLayoutPreset="center"
      screenOptions={{ headerShown: false, animationEnabled: false }}
      initialRouteName="Conversations">
      <Stack.Screen
        initialParams={{
          appStyles: DynamicAppStyles,
        }}
        name="Conversations"
        component={ConversationsScreen}
      />
    </Stack.Navigator>
  );
};

const doNotShowHeaderOption = {
  headerShown: false,
};



const Tab = createMaterialTopTabNavigator();

function DrawerTabStack() {
  let colorScheme = useColorScheme();
  let currentTheme = DynamicAppStyles.navThemeConstants[colorScheme];

  const [swipeEnabled, setSwipeEnabled] = useState(true);

  return (
    <Tab.Navigator 
      initialRouteName="Feeds"
      // swipeEnabled={swipeEnabled}
      swipeEnabled={false}
      lazy={true} // Potenially change this
      screenOptions={({ navigation, route }) => {
        if (route.name === 'Swipe' && navigation.isFocused()) {
          setSwipeEnabled(false);
        } else if (route.name !== 'Swipe' && navigation.isFocused()) {
          setSwipeEnabled(true);
        }
      }}

      tabBarOptions={{
        indicatorStyle: {backgroundColor: DynamicAppStyles.colorSet[colorScheme].mainThemeForegroundColor},
        labelStyle: { fontColor: currentTheme.fontColor },
        style: { backgroundColor: currentTheme.backgroundColor, paddingTop: 40},
        indicatorStyle: {backgroundColor: "#ef8484"},
        showLabel: false,
        showIcon: true,
      }}
      >
      <Tab.Screen  
        options={{
          tabBarIcon: ({focused}) => (
            focused ?
            <Icon style={{height: 25, width: 25}} name="account" size={25} color="#ef8484"/> 
            :
            <Icon style={{height: 25, width: 25}} name="account" size={25} color={DynamicAppStyles.colorSet[colorScheme].mainSubtextColor}/> 
            )
        }}
        name="Settings" 
        component={MyProfileStack} 
      />
      
      <Tab.Screen 
        initialParams={{ appStyles: DynamicAppStyles, appConfig: DatingConfig }}
        options={{
          
          tabBarIcon: ({focused}) => (
          focused ?
            <Icon style={{height: 25, width: 25}} name="animation" size={25} color="#ef8484"/> 
            :
            <Icon style={{height: 25, width: 25}} name="animation" size={25} color={DynamicAppStyles.colorSet[colorScheme].mainSubtextColor}/> 
          )
        }}
        name="Feeds" 
        component={FeedsScreen} 
      />
      <Tab.Screen 
        
        options={{
          tabBarIcon: ({focused}) => (
            focused ?
            <Icon style={{height: 25, width: 25}} name="calendar" size={25} color="#ef8484"/> 
            :
            <Icon style={{height: 25, width: 25}} name="calendar" size={25} color={DynamicAppStyles.colorSet[colorScheme].mainSubtextColor}/> 
          )
        }}
        name="Events" 
        component={DealsScreen} 
      />
      <Tab.Screen 
        
        options={{
          tabBarIcon: ({focused}) => (
            focused ?
            <Icon style={{height: 25, width: 25}} name="account-heart" size={25} color="#ef8484"/> 
            :
            <Icon style={{height: 25, width: 25}} name="account-heart" size={25} color={DynamicAppStyles.colorSet[colorScheme].mainSubtextColor}/> 
          )
        }}
        name="Swipe" 
        component={SwipeScreen} 
      />
      <Tab.Screen 
        initialParams={{ appStyles: DynamicAppStyles }}
        options={{
          tabBarIcon: ({focused}) => (
            focused ?
            <Icon style={{height: 25, width: 25}} name="heart-multiple" size={25} color="#ef8484"/> 
            :
            <Icon style={{height: 25, width: 25}} name="heart-multiple" size={25} color={DynamicAppStyles.colorSet[colorScheme].mainSubtextColor}/> 
          )
        }}
        name="Likes" 
        component={LikesScreen} 
      />
      <Tab.Screen 
        options={{
          tabBarIcon: ({focused}) => (
          focused ?
            <Icon style={{height: 25, width: 25}} name="chat" size={25} color="#ef8484"/> 
            :
            <Icon style={{height: 25, width: 25}} name="chat" size={25} color={DynamicAppStyles.colorSet[colorScheme].mainSubtextColor}/> 
          )
        }}
        name="Chat"
        component={ConversationsStack} 
      />
    </Tab.Navigator>
  );
}


const DrawerStack = () => {
  let colorScheme = useColorScheme();
  let currentTheme = DynamicAppStyles.navThemeConstants[colorScheme];
  return (
    <Stack.Navigator
      screenOptions={{ headerShown: false, headerTitleAlign: 'center', animationEnabled: false}}
      headerMode="float"
      initialRouteName="Swipe">
      <Stack.Screen
        name="Swipe"
        component={DrawerTabStack}
      />
      <Stack.Screen
        initialParams={{
          appStyles: DynamicAppStyles,
        }}
        name="Conversations"
        component={ConversationsStack}
      />

      <Stack.Screen 
        initialParams={{
          appStyles: DynamicAppStyles,
        }}
        name="Profile"
        component={MyProfileStack}
      />

      <Stack.Screen
        name="AddProfilePicture"
        component={AddProfilePictureScreen}
      />
      <Stack.Screen name="AccountDetails" component={IMEditProfileScreen} />
    </Stack.Navigator>
  );
};

const MainStackNavigator = () => {
  return (
    <Stack.Navigator headerMode="float" initialRouteName="NavStack">
      <Stack.Screen
        initialParams={{
          appStyles: DynamicAppStyles,
        }}
        options={doNotShowHeaderOption}
        name="NavStack"
        component={DrawerStack}
      />
      <Stack.Screen
        options={{ headerBackTitle: 'Back' }}
        name="PersonalChat"
        component={IMChatScreen}
      />
      <Stack.Screen
        name="AccountDetails"
        component={IMEditProfileScreen}
      />
      <Stack.Screen
        name="Settings"
        component={IMUserSettingsScreen}
      />
      <Stack.Screen
        name="ContactUs"
        component={IMContactUsScreen}
      />
      <Stack.Screen
        options={{ headerBackTitle: 'Back' }}
        name="GroupChat"
        component={GroupChatScreen}
      />
    </Stack.Navigator>
  );
};

// Manifest of possible screens
const RootNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{ animationEnabled: false, headerShown: false }}
      initialRouteName="LoadScreen">
      <Stack.Screen
        initialParams={{
          appStyles: DynamicAppStyles,
          appConfig: DatingConfig,
        }}
        name="LoadScreen"
        component={LoadScreen}
      />
      <Stack.Screen
        options={{ headerShown: false }}
        name="Walkthrough"
        component={WalkthroughScreen}
      />
      <Stack.Screen
        options={{ headerShown: false }}
        name="LoginStack"
        component={LoginStack}
      />
      <Stack.Screen
        options={{ headerShown: false }}
        name="MainStack"
        component={MainStackNavigator}
      />
    </Stack.Navigator>
  );
};

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <RootNavigator />
    </NavigationContainer>
  );
};

export { RootNavigator, AppNavigator };
