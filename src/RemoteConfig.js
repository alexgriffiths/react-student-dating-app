import { Platform } from 'react-native';
import React, { useEffect } from 'react';

import { IMLocalized, setI18nConfig } from './Core/localization/IMLocalization';
import remoteConfig from '@react-native-firebase/remote-config';


const regexForNames = /^[a-zA-Z]{2,25}$/;
const regexForPhoneNumber = /\d{9}$/;
const regexForAge = /[0-9]/g;

function RemoteConfig() {

  // remoteConfig().fetch(0)
  // .then(() => remoteConfig().activate())
  // .then(fetched => {
  //   const remoteVal = remoteConfig().getValue('remoteValue');
  //   console.log(remoteVal);
  // });

  remoteConfig().setDefaults({
    onboardingInterests: [
      {"interest":"Dog lover"},
      {"interest":"Gym-goer"},
      {"interest":"Netflix"},
      {"interest":"Clubbing"},
      {"interest":"Vegan"},
      {"interest":"Coffe lover"},
      {"interest":"Video games"},
      {"interest":"Football"},
      {"interest":"Walks"}
    ],
    remoteValue: 1,
    // onboardingInterests: 3,

  }).then(() => remoteConfig().fetchAndActivate())      
  .then(fetchedRemotely => {
    if (fetchedRemotely) {
      return remoteConfig().getAll();
    } else {
      console.log(
        'No configs were fetched from the backend, and the local configs were already activated',
      );
    }
  });
}

export default RemoteConfig;
