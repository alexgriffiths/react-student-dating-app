
# React Native Student App

## Installation
(Currently only running production on iOS with no support for Android)

Building:
- npm install
- npm start

Running on iOS:
- cd iOS
- post install
- open the .xcworkspace file in Xcode
- Run on iOS simulator or device (issues occur running the app locally on certain iOS's and Xcode versions).

Why can't I just run "react-native run-ios"?
Sadly, because we are using Firebase packages the app has to be built with Xcode.

## Repository stucture
The structure of the repo should be quite self-explanatory, `src/screens` contains the majority of pages featured with in the app. These are accessed via `src/navigations/AppNavigation.js`.

## Backend
This project is built using a Firebase serverless backend, specifically using a Firestore database and Firebase functions.
