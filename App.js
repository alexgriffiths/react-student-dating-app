// import './wdyr'; // <--- first import
import React, { useEffect, useState } from 'react';

import { 
  AppRegistry, 
  StatusBar,
  View,
  Modal,
  Text,
  TouchableOpacity
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import * as RNLocalize from 'react-native-localize';
import { Appearance, AppearanceProvider } from 'react-native-appearance';
import { setI18nConfig } from './src/Core/localization/IMLocalization';
import { AppNavigator } from './src/navigations/AppNavigation';
import IAPManagerWrapped from './src/Core/inAppPurchase/IAPManagerWrapped';
import AppReducer from './src/redux';
import { enableScreens } from 'react-native-screens';
import DynamicAppStyles from './src/DynamicAppStyles';
import DatingConfig from './src/DatingConfig';
import AnimatedSplash from "react-native-animated-splash-screen";
import VersionInfo from 'react-native-version-info';
import compareVersions from 'compare-versions';
import UpdateApp from './src/UpdateApp';
import remoteConfig from '@react-native-firebase/remote-config';

import {
  setCustomText,
} from 'react-native-global-props'; 

const MainNavigator = AppNavigator;

const store = createStore(AppReducer, applyMiddleware(thunk));
// const useForceUpdate = () => useState()[1];

const App = (props) => {
  const [colorScheme, setColorScheme] = useState(Appearance.getColorScheme());
  const [showForceUpdate, setShowForceUpdate] = useState(false);

  enableScreens();

const customTextProps = {
  style: {
    fontFamily: Platform.OS === 'ios' ? 'kanit-regular' : 'Roboto',
  }
};
setCustomText(customTextProps);

const [isLoaded, setIsLoaded] = useState(0);

  useEffect(() => {
    SplashScreen.hide();
    remoteConfig().setConfigSettings({
      minimumFetchIntervalMillis: 10,
    });
    remoteConfig()
    .setDefaults({
      minVersion: '1.0.21',
    })
    .then(() => remoteConfig().fetchAndActivate())
    .then(fetchedRemotely => {
      if (fetchedRemotely) {
        setShowForceUpdate(compareVersions(VersionInfo.appVersion, remoteConfig().getValue('minVersion').asString()))
        console.log(remoteConfig().getValue('minVersion').asString());
        console.log('Configs were retrieved from the backend and activated.');
      } else {
        console.log(
          'No configs were fetched from the backend, and the local configs were already activated',
        );
      }
    });

    setIsLoaded(true);
    setI18nConfig();
    RNLocalize.addEventListener('change', handleLocalizationChange);
    Appearance.addChangeListener(({ colorScheme }) => {
      setColorScheme(colorScheme);
    });
    return () => {
      RNLocalize.removeEventListener('change', handleLocalizationChange);
    };
  }, []);

  const handleLocalizationChange = () => {
    setI18nConfig();
    // useForceUpdate();
  };

  return (
    <AnimatedSplash
    translucent={true}
    isLoaded={isLoaded}
    logoImage={require("./assets/images/fire-icon.png")}
    backgroundColor={"#262626"}
    logoHeight={150}
    logoWidth={150}
  >
    {showForceUpdate > -1 ? 
      <Provider store={store}>
        <AppearanceProvider>
          <IAPManagerWrapped
            appStyles={DynamicAppStyles}
            appConfig={DatingConfig}>
            <StatusBar />
            <MainNavigator screenProps={{ theme: colorScheme }} />
          </IAPManagerWrapped>
        </AppearanceProvider>
      </Provider>
      :
      <UpdateApp/>
    }
    </AnimatedSplash>

  );
};

App.propTypes = {};

App.defaultProps = {};

AppRegistry.registerComponent('App', () => App);

export default App;
